﻿using CTCLogging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Xml.Linq;

namespace XMLLocker.CTC
{
    public static class NodeFunctions
    {
        #region members

        #endregion

        #region properties

        #endregion

        #region constructor

        #endregion

        #region methods

        public static bool IsCommonXml(string xmlFile)
        {
            XDocument xml = XDocument.Load(xmlFile);
            var root = xml.Root;
            if (root.Name.ToString() == "NodeFile")
            {
                var matrix = root.Element("IdentityMatrix");
                if (matrix != null)
                {
                    return true;
                }
                matrix = root.Element("IdendtityMatrix");
                if (matrix != null)
                {
                    return true;
                }
            }
            return false;
        }
        public static List<NodeFileTrackingOrder> AddtoOrderList(List<NodeFileTrackingOrder> o, List<NodeFileTrackingOrder> orders)
        {
            foreach (NodeFileTrackingOrder order in orders)
            {
                o.Add(order);
            }
            return o;
        }

        public static NodeFile ImportProducts(DataTable dt, string senderid, string recipientid, List<MapOperation> mapping, string logPath)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            string lo = logPath;
            string cp = string.Empty;
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            string conversionResult = string.Empty;
            NodeFile nodeFile = new NodeFile();
            nodeFile.IdendtityMatrix = CreateIdentityMatrix(recipientid, senderid, "ProductImport");


            if (dt.Rows.Count > 0)
            {

                try
                {
                    int iRec = 0;
                    List<ProductElement> products = new List<ProductElement>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        ProductElement product = new ProductElement();
                        iRec++;
                        ProductElementParent parentItem = new ProductElementParent();
                        bool containsParent = false;
                        foreach (var map in mapping)
                        {
                            PropertyInfo prop = product.GetType().GetProperty(map.MD_FromField.Trim(), BindingFlags.Public | BindingFlags.Instance);

                            if (null != prop && prop.CanWrite)
                            {

                                switch (map.MD_DataType)
                                {
                                    case "BOO":
                                        bool valBool;
                                        if (bool.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valBool))
                                        {
                                            prop.SetValue(product, valBool, null);
                                        }
                                        break;
                                    case "DAT":
                                        DateTime valDate = new DateTime();
                                        if (DateTime.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valDate))
                                        {
                                            prop.SetValue(product, valDate, null);
                                        }
                                        break;
                                    case "NUM":
                                        Decimal valDec = 0;
                                        if (Decimal.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valDec))
                                        {
                                            prop.SetValue(product, valDec, null);
                                        }
                                        break;
                                    case "INT":
                                        int valInt = 0;
                                        if (int.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valInt))
                                        {
                                            prop.SetValue(product, valInt, null);
                                        }
                                        break;
                                    case "STR":
                                        prop.SetValue(product, dr[map.MD_ToField.ToString().Trim()].ToString(), null);
                                        break;
                                    case "OVR":
                                        prop.SetValue(product, map.MD_ToField, null);
                                        break;
                                }
                                if (map.MD_ToField.Contains("Parent"))
                                {
                                    containsParent = true;
                                    switch (map.MD_DataType)
                                    {
                                        case "NUM":
                                            Decimal valDec = 0;
                                            if (Decimal.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valDec))
                                            {
                                                prop.SetValue(parentItem, valDec, null);
                                            }
                                            break;
                                        case "INT":
                                            int valInt = 0;
                                            if (int.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valInt))
                                            {
                                                prop.SetValue(parentItem, valInt, null);
                                            }
                                            break;
                                        case "STR":
                                            prop.SetValue(parentItem, dr[map.MD_ToField.ToString().Trim()].ToString(), null);
                                            break;
                                    }
                                }

                            }

                        }
                        if (containsParent)
                        {
                            List<ProductElementParent> parentcoll = new List<ProductElementParent>
                            {
                             new ProductElementParent{ ParentBarcode= parentItem.ParentBarcode, ParentUOM = parentItem.ParentUOM, QtyInParent = parentItem.QtyInParent}
                            };
                            product.Parent = parentcoll.ToArray();
                        }

                        products.Add(product);
                    }
                    nodeFile.ProductImport = products.ToArray();
                    //nodeFile.Operation.ProductImport = products.ToArray();
                }
                catch (Exception ex)
                {
                    using (AppLogger log = new AppLogger(lo, cp, ex.Message, m.Name, ti, string.Empty, di))
                    {
                        log.AddLog();
                    }
                    return null;
                }
                //}
                //    catch (Exception ex)
                //{
                //    //NodeResources.GenieMessage(NodeResources.GenerateArticleXML("ERP.ArticleEx", "replyFromExport", "articleId", articleID.Trim(), false, "Error Converting XML: " + ex.Message));
                //    string strEx = ex.GetType().Name;

                //}
            }
            return nodeFile;


        }

        private static NodeFileIdendtityMatrix CreateIdentityMatrix(string recipientid, string senderid, string operation)
        {
            NodeFileIdendtityMatrix id = new NodeFileIdendtityMatrix
            {
                CustomerId = recipientid,
                SenderId = senderid,
                DocumentType = operation
            };
            return id;
        }
        #endregion

        public static NodeFile CreateWhOrder(DataTable dt, string senderid, string recipientid, List<MapOperation> mapping, string logPath)
        {
            NodeFile nodeFile = new NodeFile();
            nodeFile.IdendtityMatrix = CreateIdentityMatrix(recipientid, senderid, "TrackingOrders");

            return nodeFile;

        }
        #region helpers

        #endregion









    }
}

﻿using CTCLogging;
using NodeResources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using XMLLocker.Cargowise.NDM.Organization;
using XMLLocker.Cargowise.XUS;

namespace XMLLocker.CTC
{
    public class ToCargowise
    {
        #region members
        private string _appLogPath { get; set; }
        public Ivw_CustomerProfile Profile { get; set; }
        public ToCargowise(string logPath)
        {
            _appLogPath = logPath;
        }
        private string _cwType;
        #endregion

        #region properties

        #endregion

        #region constructor

        #endregion

        #region methods
        public Native CreateOrgFromCompany(NodeFile nodeFile)
        {
            Native native = new Native();
            foreach (var company in nodeFile.CompanyImport)
            {
                NativeOrganization org = new NativeOrganization();
                org.Action = Cargowise.NDM.Organization.Action.MERGE;
                org.ActionSpecified = true;
                org.Code = company.CompanyCode;
                org.OrgAddressCollection = GetAddressesFromCommon(company.AddressCollection).ToArray();
                NativeHeader orgHeader = new NativeHeader();
                NativeBody body = new NativeBody();
                orgHeader.OwnerCode = nodeFile.IdendtityMatrix.SenderId;
                orgHeader.EnableCodeMapping = true;
                orgHeader.EnableCodeMappingSpecified = true;
                OrganizationData organizationData = new OrganizationData();
                organizationData.OrgHeader = org;
                List<NativeOrganization> orgs = new List<NativeOrganization>();
                var cwNSNative = new XmlSerializerNamespaces();
                cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
                body.Any = new[] { org.AsXmlElement(cwNSNative) };
                native.Header = orgHeader;
                native.Body = body;
            }
            return native;
        }
        private List<NativeOrganizationOrgAddress> GetAddressesFromCommon(CompanyElement[] Addresses)
        {
            List<NativeOrganizationOrgAddress> orgAddressCollection = new List<NativeOrganizationOrgAddress>();
            foreach (var address in Addresses)
            {
                NativeOrganizationOrgAddress orgAddress = new NativeOrganizationOrgAddress
                {
                    CompanyNameOverride = address.CompanyName,
                    Address1 = address.Address1,
                    Address2 = address.Address2,
                    City = address.City,
                    State = address.State,
                    PostCode = address.PostCode,
                    Email = address.EmailAddress,
                    Phone = address.PhoneNo,
                    OrgAddressCapabilityCollection = GetAddressTypeFromCommon(address.CompanyType).ToArray(),
                };

                orgAddressCollection.Add(orgAddress);
            }
            return orgAddressCollection;
        }

        private List<NativeOrganizationOrgAddressOrgAddressCapability> GetAddressTypeFromCommon(CompanyElementCompanyType companyType)
        {
            List<NativeOrganizationOrgAddressOrgAddressCapability> orgCapabilityCollection = new List<NativeOrganizationOrgAddressOrgAddressCapability>();
            NativeOrganizationOrgAddressOrgAddressCapability orgCapability = new NativeOrganizationOrgAddressOrgAddressCapability();
            orgCapability.Action = Cargowise.NDM.Organization.Action.MERGE;
            orgCapability.ActionSpecified = true;
            switch (companyType)
            {
                case CompanyElementCompanyType.DeliveryAddress: orgCapability.AddressType = "DLV"; break;
                case CompanyElementCompanyType.Consignee: orgCapability.AddressType = "MAI"; break;
                case CompanyElementCompanyType.Consignor: orgCapability.AddressType = "MAI"; break;
                case CompanyElementCompanyType.OrderDeliveryAddress: orgCapability.AddressType = "DLV"; break;
                case CompanyElementCompanyType.PickupAddress: orgCapability.AddressType = "PAD"; break;
                case CompanyElementCompanyType.ReceivingAgent: orgCapability.AddressType = "MAI"; break;
            }
            orgCapabilityCollection.Add(orgCapability);
            return orgCapabilityCollection;
        }
        public void CreateProductFile(NodeFile common, Cargowise.NDM.Action partOperation, string outputPath)
        {
            if (common.ProductImport != null)
            {
                string lo = this._appLogPath;
                string cp = string.Empty;
                string de = "Processing Folder Queue";
                string fu = "Process Pickup";
                DateTime ti = DateTime.Now;
                string er = string.Empty;
                string di = string.Empty;
                foreach (ProductElement product in common.ProductImport)
                {
                    int iRec = 0;
                    try
                    {

                        Cargowise.NDM.ProductData productData = new Cargowise.NDM.ProductData();

                        Cargowise.NDM.NativeHeader nativeHeader = new Cargowise.NDM.NativeHeader();
                        Cargowise.NDM.DataContext dataContext = new Cargowise.NDM.DataContext();
                        Cargowise.NDM.NativeBody nativeBody = new Cargowise.NDM.NativeBody();
                        Cargowise.NDM.NativeProduct nativeProduct = new Cargowise.NDM.NativeProduct();
                        List<Cargowise.NDM.NativeProductOrgPartRelation> nativeProductOrgRelationColl = new List<Cargowise.NDM.NativeProductOrgPartRelation>();
                        Cargowise.NDM.NativeProductOrgPartRelation nativeProductOrgPartRelation = new Cargowise.NDM.NativeProductOrgPartRelation();
                        Cargowise.NDM.NativeProductOrgPartRelationOrgHeader orgPartRelationOrgHeader = new Cargowise.NDM.NativeProductOrgPartRelationOrgHeader();
                        List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode> nativeProductBarcodeColl = new List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode>();
                        Cargowise.NDM.NativeProductOrgSupplierPartBarcode nativeProductBarcode = new Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                        List<Cargowise.NDM.NativeProductOrgPartUnit> nativeProductOrgPartUnitColl = new List<Cargowise.NDM.NativeProductOrgPartUnit>();
                        iRec++;

                        nativeProduct = new Cargowise.NDM.NativeProduct();
                        nativeProduct.PartNum = product.Code;
                        nativeProduct.StockKeepingUnit = product.StockKeepingUnit;
                        nativeProduct.Weight = product.Weight > 0 ? product.Weight : 0;
                        nativeProduct.WeightSpecified = product.Weight > 0 ? true : false;
                        nativeProduct.WeightUQ = product.WeightUnit != null ? product.WeightUnit : null;
                        nativeProduct.WeightSpecified = product.WeightUnit != null ? true : false;
                        nativeProduct.Height = product.Height > 0 ? product.Height : 0;
                        nativeProduct.HeightSpecified = product.Height > 0 ? true : false;
                        nativeProduct.Width = product.Width > 0 ? product.Width : 0;
                        nativeProduct.WidthSpecified = product.Width > 0 ? true : false;
                        nativeProduct.Depth = product.Depth > 0 ? product.Depth : 0;
                        nativeProduct.DepthSpecified = product.Depth > 0 ? true : false;
                        int volFactor = 1;
                        switch (product.DimsUnit)
                        {
                            case "CM":
                                volFactor = 100;
                                break;
                            case "IN":
                                volFactor = 1;
                                break;
                            case "M":
                                volFactor = 1;
                                break;

                            default:
                                volFactor = 100;
                                break;
                        }
                        decimal volume = 0;
                        if (nativeProduct.Height > 0 && nativeProduct.Width > 0 && nativeProduct.Depth > 0)
                        {
                            volume = ((product.Height / volFactor) * (product.Width / volFactor) * (product.Depth / volFactor));
                        }
                        nativeProduct.Cubic = volume > 0 ? volume : 0;
                        nativeProduct.CubicSpecified = volume > 0 ? true : false;
                        nativeProduct.CubicUQ = volume > 0 ? "M3" : null;
                        nativeProduct.MeasureUQ = product.DimsUnit != null ? product.DimsUnit : null;
                        nativeProduct.Desc = product.Description;
                        nativeProduct.ActionSpecified = true;
                        nativeProduct.Action = partOperation;
                        nativeProduct.IsActive = true;
                        nativeProduct.IsWarehouseProduct = true;
                        nativeProduct.IsWarehouseProductSpecified = true;
                        nativeProduct.IsBarcoded = true;
                        nativeProduct.IsBarcodedSpecified = true;
                        orgPartRelationOrgHeader = new Cargowise.NDM.NativeProductOrgPartRelationOrgHeader();
                        nativeProductOrgRelationColl = new List<Cargowise.NDM.NativeProductOrgPartRelation>();
                        orgPartRelationOrgHeader.Code = common.IdendtityMatrix.SenderId;
                        nativeProductOrgPartRelation.OrgHeader = orgPartRelationOrgHeader;
                        nativeProductOrgPartRelation.Relationship = "OWN";
                        nativeProductOrgPartRelation.Action = partOperation;
                        nativeProductOrgPartRelation.ActionSpecified = true;
                        nativeProductOrgRelationColl.Add(nativeProductOrgPartRelation);

                        nativeProduct.OrgPartRelationCollection = nativeProductOrgRelationColl.ToArray();
                        if (product.Barcode != null)
                        {
                            nativeProductBarcodeColl = new List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode>();
                            nativeProduct.IsBarcoded = true;
                            nativeProductBarcode = new Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                            nativeProductBarcode.Action = Cargowise.NDM.Action.MERGE;
                            nativeProductBarcode.ActionSpecified = true;
                            nativeProductBarcode.Barcode = product.Barcode;
                            //nativeProductBarcode.PackType.TableName = "RefPackType";
                            Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType productPackType = new Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType();
                            productPackType.TableName = "RefPackType";
                            productPackType.Code = "UNT";
                            nativeProductBarcode.PackType = productPackType;
                            nativeProductBarcodeColl.Add(nativeProductBarcode);
                            nativeProduct.OrgSupplierPartBarcodeCollection = nativeProductBarcodeColl.ToArray();
                        }
                        else
                        {
                            nativeProduct.IsBarcoded = false;
                        }
                        var cwNSUniversal = new XmlSerializerNamespaces();
                        cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                        var cwNSNative = new XmlSerializerNamespaces();
                        cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
                        productData.OrgSupplierPart = nativeProduct;
                        productData.version = "2.0";
                        nativeProduct.Action = partOperation;
                        //nativeBody.Any = new[] { productData.AsXmlElement(cwNSNative) };
                        //nativeHeader.OwnerCode = common.IdendtityMatrix.SenderId;
                        //nativeHeader.EnableCodeMapping = true;
                        //nativeHeader.EnableCodeMappingSpecified = true;
                        //dataContext.ActionPurpose = new Cargowise.NDM.CodeDescriptionPair { Code = "IMP", Description = "Product Import" };
                        //dataContext.EventType = new Cargowise.NDM.CodeDescriptionPair { Code = "ADD", Description = "Record Added" };
                        Cargowise.NDM.Native native = new Cargowise.NDM.Native
                        {
                            Header = new Cargowise.NDM.NativeHeader
                            {
                                DataContext = new Cargowise.NDM.DataContext
                                {
                                    ActionPurpose = new Cargowise.NDM.CodeDescriptionPair { Code = "IMP", Description = "Product Import" },
                                    EventType = new Cargowise.NDM.CodeDescriptionPair { Code = "ADD", Description = "Record Added" }
                                },
                                EnableCodeMapping = true,
                                EnableCodeMappingSpecified = true,
                                OwnerCode = common.IdendtityMatrix.SenderId
                            },
                            Body = new Cargowise.NDM.NativeBody
                            {
                                Any = new[] { productData.AsXmlElement(cwNSNative) }
                            },
                            version = "2.0"
                        };
                        //native.Header.DataContext = dataContext;
                        //native.version = "2.0";
                        //native.Header = nativeHeader;
                        //native.Body = nativeBody;
                        Cargowise.NDM.UniversalInterchange universalInterchange = new Cargowise.NDM.UniversalInterchange
                        {
                            Header = new Cargowise.NDM.UniversalInterchangeHeader
                            {
                                SenderID = common.IdendtityMatrix.SenderId,
                                RecipientID = common.IdendtityMatrix.CustomerId
                            },
                            Body = new Cargowise.NDM.UniversalInterchangeBody
                            {
                                Any = new[] { native.AsXmlElement(cwNSUniversal) }
                            },
                            version = "1.1"
                        };
                        //Cargowise.NDM.UniversalInterchangeHeader universalInterchangeHeader = new Cargowise.NDM.UniversalInterchangeHeader();
                        //Cargowise.NDM.UniversalInterchangeBody universalInterchangeBody = new Cargowise.NDM.UniversalInterchangeBody();
                        //universalInterchange.version = "1.1";
                        //universalInterchangeHeader.SenderID = common.IdendtityMatrix.SenderId;
                        //universalInterchangeHeader.RecipientID = common.IdendtityMatrix.CustomerId;
                        //universalInterchangeBody.Any = new[] { native.AsXmlElement(cwNSUniversal) };
                        //universalInterchange.Body = universalInterchangeBody;
                        //universalInterchange.Header = universalInterchangeHeader;
                        String cwXML = Path.Combine(outputPath, common.IdendtityMatrix.SenderId + "PRODUCT" + "-" + product.Code + partOperation.ToString() + ".xml");
                        int iFileCount = 0;
                        while (File.Exists(cwXML))
                        {
                            iFileCount++;
                            cwXML = Path.Combine(outputPath, common.IdendtityMatrix.SenderId + "PRODUCT" + "-" + product.Code + partOperation + iFileCount + ".xml");
                        }
                        using (Stream outputCW = File.Open(cwXML, FileMode.Create))
                        {
                            StringWriter writer = new StringWriter();
                            XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.NDM.UniversalInterchange));
                            xSer.Serialize(outputCW, universalInterchange, cwNSUniversal);
                            outputCW.Flush();
                            outputCW.Close();
                        }
                    }


                    catch (Exception ex)
                    {
                        cp = "Error Creating Product File";
                        de = "Error Processing " + product.Code;
                        ti = DateTime.Now;
                        er = "Error:" + ex.GetType().Name + " :" + ex.Message;
                        di = outputPath;
                        using (AppLogger log = new AppLogger(lo, cp, de, fu, ti, er, di))
                        {
                            log.AddLog();
                        }

                    }
                }
            }
        }




        public XMLLocker.Cargowise.XUS.UniversalInterchange[] CWFromCommonOrders(NodeFile nodeFile)
        {
            XMLLocker.Cargowise.XUS.UniversalInterchange cw = new XMLLocker.Cargowise.XUS.UniversalInterchange();
            List<XMLLocker.Cargowise.XUS.UniversalInterchange> cws = new List<Cargowise.XUS.UniversalInterchange>();

            if (nodeFile.TrackingOrders != null)
            {
                if (nodeFile.TrackingOrders.Length > 0)
                {
                    foreach (NodeFileTrackingOrder _ord in nodeFile.TrackingOrders)
                    {
                        _cwType = "OrderManagerOrder";
                        cw = CreateFramework("OrderManagerOrder");
                        cw = CreateTrackingOrder(_ord, CreateFramework("OrderManagerOrder"));
                        cws.Add(cw);
                    }

                }
            }

            return cws.ToArray();
        }

        public XMLLocker.Cargowise.XUS.UniversalInterchange CWFromCommon(NodeFile nodeFile)
        {
            XMLLocker.Cargowise.XUS.UniversalInterchange cw = new XMLLocker.Cargowise.XUS.UniversalInterchange();
            List<XMLLocker.Cargowise.XUS.UniversalInterchange> cws = new List<Cargowise.XUS.UniversalInterchange>();

            if (nodeFile.TrackingOrders != null)
            {
                if (nodeFile.TrackingOrders.Length > 0)
                {
                    foreach (NodeFileTrackingOrder _ord in nodeFile.TrackingOrders)
                    {
                        _cwType = "OrderManagerOrder";
                        cw = CreateFramework("OrderManagerOrder");
                        cw = CreateTrackingOrder(_ord, CreateFramework("OrderManagerOrder"));
                        cws.Add(cw);
                    }

                }
            }
            if (nodeFile.WarehouseOrders != null)
            {
                if (nodeFile.WarehouseOrders.Length > 0)
                {
                    _cwType = "WarehouseOrder";
                    cw = CreateFramework("WarehouseOrder");
                    cw = CreateWarehouseOrder(nodeFile, CreateFramework("WarehouseOrder"));
                }
            }
            if (nodeFile.WarehouseReceipt != null)
            {
                _cwType = "WarehouseReceive";
                cw = CreateFramework("WarehouseReceive");
                cw = CreateWarehouseReceipt(nodeFile, CreateFramework("WarehouseReceive"));
            }

            if (cw != null)
            {
                if (cw.Header == null)
                {
                    cw.Header = new Cargowise.XUS.UniversalInterchangeHeader();
                }
                cw.Header.SenderID = nodeFile.IdendtityMatrix.SenderId;
                cw.Header.RecipientID = nodeFile.IdendtityMatrix.CustomerId;
                return cw;
            }
            return null;
        }


        public XMLLocker.Cargowise.XUS.UniversalInterchange CWFromCommonWarehouse(NodeFileWarehouseOrder nodeFile)
        {
            XMLLocker.Cargowise.XUS.UniversalInterchange cw = new XMLLocker.Cargowise.XUS.UniversalInterchange();
            List<XMLLocker.Cargowise.XUS.UniversalInterchange> cws = new List<Cargowise.XUS.UniversalInterchange>();
                    _cwType = "WarehouseOrder";
                    cw = CreateFramework("WarehouseOrder");
                    cw = CreateWarehouseOrder(nodeFile, CreateFramework("WarehouseOrder"));
                
            
            return cw;
        }

        private Cargowise.XUS.UniversalInterchange CreateWarehouseReceipt(NodeFile nodeFile, Cargowise.XUS.UniversalInterchange emptyInterchange)
        {
            Cargowise.XUS.UniversalInterchange cw = emptyInterchange;
            cw.Header.RecipientID = nodeFile.IdendtityMatrix.CustomerId;
            cw.Header.SenderID = nodeFile.IdendtityMatrix.SenderId;

            Shipment shipment = new Shipment();
            Cargowise.XUS.DataContext dc = new Cargowise.XUS.DataContext();
            List<DataTarget> dataTargetColl = new List<DataTarget>();
            DataTarget dataTarget = new DataTarget();
            dataTarget.Type = "WarehouseReceive";
            dataTargetColl.Add(dataTarget);
            dc.DataTargetCollection = dataTargetColl.ToArray();
            shipment.DataContext = dc;
            ShipmentOrder pOrder = new ShipmentOrder();
            pOrder.OrderNumber = nodeFile.WarehouseReceipt.ReceiveReference;
            pOrder.ClientReference = nodeFile.WarehouseReceipt.CustomerReferences;
            pOrder.Warehouse = new ShipmentOrderWarehouse { Code = nodeFile.WarehouseReceipt.WarehouseCode };


            //TODO 
            //Create Variable to allow for Return Authority
            CodeDescriptionPair codePair = new CodeDescriptionPair();
            codePair.Code = "REC";
            codePair.Description = "GOODS RECEIPT";
            pOrder.Type = codePair;
            pOrder.DateCollection = GetDatesFromCommon(nodeFile.WarehouseReceipt.Dates);
            pOrder.OrderLineCollection = GetOrderLinesFromCommon(nodeFile.WarehouseReceipt.OrderLines);
            pOrder.OrderLineCollection.Content = CollectionContent.Complete;
            pOrder.OrderLineCollection.ContentSpecified = true;
            shipment.Order = pOrder;
            if (nodeFile.WarehouseReceipt.Containers != null)
            {
                if (nodeFile.WarehouseReceipt.Containers.Length > 0)
                {
                    shipment.ContainerCollection = new ShipmentContainerCollection
                    {
                        Content = CollectionContent.Partial,
                        ContentSpecified = true,
                        Container = GetContainersFromCommon(nodeFile.WarehouseReceipt.Containers).ToArray()

                    };
                    shipment.ContainerCount = shipment.ContainerCollection.Container.Length;
                    shipment.ContainerCountSpecified = true;
                }
            }


            shipment.OrganizationAddressCollection = GetCompaniesFromCommon(nodeFile.WarehouseReceipt.Companies);
            cw.Body.BodyField.Shipment = shipment;
            return cw;
        }

        private List<Container> GetContainersFromCommon(ContainerElement[] containers)
        {
            return (from container in containers
                    select new Container
                    {
                        ContainerNumber = container.ContainerNo,
                        ContainerType = new ContainerContainerType { Code = container.ContainerCode }
                    }).ToList();

        }

        private XMLLocker.Cargowise.XUS.UniversalInterchange CreateWarehouseOrder(NodeFile nodeFile, XMLLocker.Cargowise.XUS.UniversalInterchange emptyXUS)
        {
            XMLLocker.Cargowise.XUS.UniversalInterchange cw = new XMLLocker.Cargowise.XUS.UniversalInterchange();
            cw = emptyXUS;
            Shipment shipment = cw.Body.BodyField.Shipment;
            foreach (NodeFileWarehouseOrder ord in nodeFile.WarehouseOrders)
            {
                if (ord.ShipVia != null)
                {
                    List<AdditionalReference> additionalReferences = new List<AdditionalReference>();
                    AdditionalReference adRef = new AdditionalReference
                    {
                        Type = new EntryType { Code = "VHN" },
                        ReferenceNumber = ord.ShipVia
                    };
                    additionalReferences.Add(adRef);
                    var addReferenceCollection = new ShipmentAdditionalReferenceCollection();
                    if (shipment.AdditionalReferenceCollection == null)
                    {
                        addReferenceCollection.Content = CollectionContent.Partial;
                        addReferenceCollection.ContentSpecified = true;
                    }
                    else
                    {
                        addReferenceCollection = shipment.AdditionalReferenceCollection;
                    }
                    addReferenceCollection.AdditionalReference = additionalReferences.ToArray();
                    shipment.AdditionalReferenceCollection = addReferenceCollection;
                }

                if (ord.TransportService != null)
                {
                    shipment.CarrierServiceLevel = new ServiceLevel { Code = ord.TransportService };
                }


                var dates = GetDatesFromCommon(ord.Dates);
                if (dates != null)
                {
                    shipment.DateCollection = dates;
                    if (shipment.DateCollection == null)
                    {
                        return null;
                    }
                }
                var orgs = GetCompaniesFromCommon(ord.Companies);
                if (ord.TransportProvider != null)
                {
                    OrganizationAddress oaTransport = new OrganizationAddress
                    {
                        AddressType = "TransportCompanyDocumentaryAddress",
                        CompanyName = ord.TransportProvider
                    };
                    if (orgs != null)
                    {

                        var neworgs = orgs.ToList();
                        neworgs.Add(oaTransport);
                        orgs = neworgs.ToArray();
                    }

                }
                if (orgs != null)
                {
                    shipment.OrganizationAddressCollection = orgs;
                    if (shipment.OrganizationAddressCollection == null)
                    {
                        return null;
                    }
                }

                ShipmentOrder xmlOrder = new ShipmentOrder();
                xmlOrder.OrderNumber = ord.OrderNumber;
                xmlOrder.ClientReference = !string.IsNullOrEmpty(ord.OrderReference) ? ord.OrderReference : null;
                xmlOrder.FulfillmentRule = new CodeDescriptionPair
                {
                    Code = "NON",
                    Description = "None"
                };
                CodeDescriptionPair pickRule = new CodeDescriptionPair();

                xmlOrder.PickOption = new CodeDescriptionPair
                {
                    Code = "MAN",
                    Description = "Manual Pick"
                };
                xmlOrder.Type = new CodeDescriptionPair
                {
                    Code = "ORD",
                    Description = "ORDER"
                };
                if (!string.IsNullOrEmpty(ord.WarehouseCode))
                {
                    xmlOrder.Warehouse = new ShipmentOrderWarehouse
                    {
                        Code = ord.WarehouseCode
                    };
                }

                CodeDescriptionPair orderStatus = new CodeDescriptionPair();
                List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
                ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
                orderLine.Content = CollectionContent.Complete;
                var lines = GetOrderLinesFromCommon(ord.OrderLines);
                if (lines != null)
                {
                    xmlOrder.OrderLineCollection = lines;
                    if (xmlOrder.OrderLineCollection == null)
                    {
                        return null;
                    }
                }
                ShipmentNoteCollection notecollection = new ShipmentNoteCollection
                {
                    Content = CollectionContent.Partial,
                    ContentSpecified = true
                };
                shipment.Order = xmlOrder;
                var notes = new List<Note>();
                var note = GetNotesFromCommon(ord.SpecialInstructions, "Special Instructions");
                if (note != null)
                {
                    notes.Add(note);
                }
                note = GetNotesFromCommon(ord.DeliveryInstructions, "Delivery Instructions");
                if (note != null)
                {
                    notes.Add(note);
                }

                if (notes.Count > 0)
                {
                    notecollection.Note = notes.ToArray();
                    if (notecollection.Note == null)
                    {
                        return null;
                    }
                    notecollection.ContentSpecified = true;
                    shipment.NoteCollection = notecollection;
                }
            }
            cw.Body.BodyField.Shipment = shipment;
            cw.Header.SenderID = nodeFile.IdendtityMatrix.SenderId;
            cw.Header.RecipientID = nodeFile.IdendtityMatrix.CustomerId;
            return cw;
        }


        private XMLLocker.Cargowise.XUS.UniversalInterchange CreateWarehouseOrder(NodeFileWarehouseOrder ord, XMLLocker.Cargowise.XUS.UniversalInterchange emptyXUS)
        {
            XMLLocker.Cargowise.XUS.UniversalInterchange cw = new XMLLocker.Cargowise.XUS.UniversalInterchange();
            cw = emptyXUS;
            Shipment shipment = cw.Body.BodyField.Shipment;
            
                if (ord.ShipVia != null)
                {
                    List<AdditionalReference> additionalReferences = new List<AdditionalReference>();
                    AdditionalReference adRef = new AdditionalReference
                    {
                        Type = new EntryType { Code = "VHN" },
                        ReferenceNumber = ord.ShipVia
                    };
                    additionalReferences.Add(adRef);
                    var addReferenceCollection = new ShipmentAdditionalReferenceCollection();
                    if (shipment.AdditionalReferenceCollection == null)
                    {
                        addReferenceCollection.Content = CollectionContent.Partial;
                        addReferenceCollection.ContentSpecified = true;
                    }
                    else
                    {
                        addReferenceCollection = shipment.AdditionalReferenceCollection;
                    }
                    addReferenceCollection.AdditionalReference = additionalReferences.ToArray();
                    shipment.AdditionalReferenceCollection = addReferenceCollection;
                }

                if (ord.TransportService != null)
                {
                    shipment.CarrierServiceLevel = new ServiceLevel { Code = ord.TransportService };
                }


                var dates = GetDatesFromCommon(ord.Dates);
                if (dates != null)
                {
                    shipment.DateCollection = dates;
                    if (shipment.DateCollection == null)
                    {
                        return null;
                    }
                }
                var orgs = GetCompaniesFromCommon(ord.Companies);
                if (ord.TransportProvider != null)
                {
                    OrganizationAddress oaTransport = new OrganizationAddress
                    {
                        AddressType = "TransportCompanyDocumentaryAddress",
                        CompanyName = ord.TransportProvider
                    };
                    if (orgs != null)
                    {

                        var neworgs = orgs.ToList();
                        neworgs.Add(oaTransport);
                        orgs = neworgs.ToArray();
                    }

                }
                if (orgs != null)
                {
                    shipment.OrganizationAddressCollection = orgs;
                    if (shipment.OrganizationAddressCollection == null)
                    {
                        return null;
                    }
                }

                ShipmentOrder xmlOrder = new ShipmentOrder();
                xmlOrder.OrderNumber = ord.OrderNumber;
                xmlOrder.ClientReference = !string.IsNullOrEmpty(ord.OrderReference) ? ord.OrderReference : null;
                xmlOrder.FulfillmentRule = new CodeDescriptionPair
                {
                    Code = "NON",
                    Description = "None"
                };
                CodeDescriptionPair pickRule = new CodeDescriptionPair();

                xmlOrder.PickOption = new CodeDescriptionPair
                {
                    Code = "MAN",
                    Description = "Manual Pick"
                };
                xmlOrder.Type = new CodeDescriptionPair
                {
                    Code = "ORD",
                    Description = "ORDER"
                };
                if (!string.IsNullOrEmpty(ord.WarehouseCode))
                {
                    xmlOrder.Warehouse = new ShipmentOrderWarehouse
                    {
                        Code = ord.WarehouseCode
                    };
                }

                CodeDescriptionPair orderStatus = new CodeDescriptionPair();
                List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
                ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
                orderLine.Content = CollectionContent.Complete;
                var lines = GetOrderLinesFromCommon(ord.OrderLines);
                if (lines != null)
                {
                    
                    xmlOrder.OrderLineCollection = lines;
                    if (xmlOrder.OrderLineCollection == null)
                    {
                        return null;
                    }
                }
                ShipmentNoteCollection notecollection = new ShipmentNoteCollection
                {
                    Content = CollectionContent.Partial,
                    ContentSpecified = true
                };
                shipment.Order = xmlOrder;
                var notes = new List<Note>();
                var note = GetNotesFromCommon(ord.SpecialInstructions, "Special Instructions");
                if (note != null)
                {
                    notes.Add(note);
                }
                note = GetNotesFromCommon(ord.DeliveryInstructions, "Delivery Instructions");
                if (note != null)
                {
                    notes.Add(note);
                }

                if (notes.Count > 0)
                {
                    notecollection.Note = notes.ToArray();
                    if (notecollection.Note == null)
                    {
                        return null;
                    }
                    notecollection.ContentSpecified = true;
                    shipment.NoteCollection = notecollection;
                }
            
            cw.Body.BodyField.Shipment = shipment;
            
            return cw;
        }

        private void LogError(string profile, string details, string process, DateTime time, string error, string path)
        {
            var appLogger = typeof(AppLogger);
            var strArgs = new Type[] { typeof(string) };
            ConstructorInfo constructorAppLogger = appLogger.GetConstructor(strArgs);
            var objLog = constructorAppLogger.Invoke(new object[] { this._appLogPath, profile, details, process, time, error, path });
            var logMethod = appLogger.GetMethod("AddLog");
            var addLog = logMethod.Invoke(objLog, new object[] { });
        }

        private Note GetNotesFromCommon(string specialInstructions, string instructionType)
        {

            Note orderNote = new Note();

            if (!string.IsNullOrEmpty(specialInstructions))
            {
                
                orderNote.Visibility = new CodeDescriptionPair
                {
                    Code = "PUB",
                    Description = "CLIENT-VISIBLE"
                };
                orderNote.Description = instructionType;
                orderNote.IsCustomDescription = true;
                orderNote.NoteText = specialInstructions;

                NoteNoteContext noteContext = new NoteNoteContext();
                noteContext.Code = "WAA";
                noteContext.Description = "Module: W - Warehouse, Direction: A - All, Freight: A - All";
                orderNote.NoteContext = noteContext;
                return orderNote;
            }

            return null;
        }

        private XMLLocker.Cargowise.XUS.UniversalInterchange CreateTrackingOrder(NodeFileTrackingOrder ord, XMLLocker.Cargowise.XUS.UniversalInterchange emptyXUS)
        {
            XMLLocker.Cargowise.XUS.UniversalInterchange cw = new XMLLocker.Cargowise.XUS.UniversalInterchange();
            cw = emptyXUS;
            Shipment shipment = cw.Body.BodyField.Shipment;

            if (ord.IncoTerms != null)
            { shipment.ShipmentIncoTerm = new IncoTerm { Code = ord.IncoTerms }; }
            if (!string.IsNullOrEmpty(ord.TransportMode))
            {
                shipment.TransportMode = new CodeDescriptionPair { Code = ord.TransportMode };
            }
            if (!string.IsNullOrEmpty(ord.ContainerMode))
            {
                shipment.ContainerMode = new ContainerMode { Code = ord.ContainerMode };
            }
            if (!string.IsNullOrEmpty(ord.Currency))
            {
                shipment.FreightRateCurrency = new Currency { Code = ord.Currency };
            }
            shipment.DateCollection = GetDatesFromCommon(ord.Dates);
            var lp = GetLocalProcessing(ord.Dates);
            if (lp != null)
            {
                shipment.LocalProcessing = lp;
            }
            shipment.OrganizationAddressCollection = GetCompaniesFromCommon(ord.Companies);
            foreach(var item in ord.Companies)
            {
                if(item.CompanyType == CompanyElementCompanyType.DeliveryAddress)
                {
                    shipment.PortOfDestination = new UNLOCO
                    {
                        Code = item.State
                    };

                    shipment.PortOfDischarge = new UNLOCO
                    {
                        Code = item.State
                    }; 
                }

                if (item.CompanyType == CompanyElementCompanyType.PickupAddress)
                {
                    shipment.PortOfLoading = new UNLOCO
                    {
                        Code = item.City
                    };

                    shipment.PortOfOrigin = new UNLOCO
                    {
                        Code = item.City
                    };
                }
            }


            ShipmentOrder order = new ShipmentOrder();
            order.OrderNumber = ord.OrderNo;
            order.Status = new CodeDescriptionPair { Code = ord.OrderStatus };
            order.OrderLineCollection = GetOrderLinesFromCommon(ord.OrderLines);
            shipment.Order = order;

            cw.Body.BodyField.Shipment = shipment;
            cw.Header.SenderID = ord.IdentityMatrix.SenderId;
            cw.Header.RecipientID = ord.IdentityMatrix.CustomerId;
            return cw;
        }

        private ShipmentOrderOrderLineCollection GetOrderLinesFromCommon(OrderLineElement[] orderLines)
        {
            ShipmentOrderOrderLineCollection linesColl = new ShipmentOrderOrderLineCollection();
            linesColl.Content = CollectionContent.Complete;
            List<ShipmentOrderOrderLineCollectionOrderLine> linesOrderLines = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            try
            {
                foreach (OrderLineElement line in orderLines)
                {

                    ShipmentOrderOrderLineCollectionOrderLine oline = new ShipmentOrderOrderLineCollectionOrderLine
                    {
                        LineNumber = line.LineNo,
                        LineNumberSpecified = line.LineNo > 0 ? true : false,
                        SubLineNumber = string.IsNullOrEmpty(line.subLineNo.ToString()) ? 0 : line.subLineNo,
                        SubLineNumberSpecified = string.IsNullOrWhiteSpace(line.subLineNo.ToString()) ? false : true,
                        UnitPriceRecommended = line.UnitPrice,
                        UnitPriceRecommendedSpecified = line.UnitPrice > 0 ? true : false,
                        ExpectedQuantity = line.ExpectedQuantity,
                        ExpectedQuantitySpecified = line.ExpectedQuantity > 0 ? true : false,
                        Product = new Product
                        {
                            Code = line.Product.Code,
                            Description = line.Product.Description
                        },
                        OrderedQty = line.OrderQty,
                        PackageQty = line.PackageQty,
                        OrderedQtyUnit = new CodeDescriptionPair
                        {
                            Code = line.OrderQtyUnitCode,
                            Description = line.OrderQtyUnit
                        },
                        PartAttribute1 = line.Batch,

                        PackageQtySpecified = true,
                        OrderedQtySpecified = true,
                        Status = new CodeDescriptionPair
                        {
                            Code = line.StatusCode,
                            Description = line.Status
                        }
                    };
                    oline.PackageQtyUnit = new PackageType
                    {
                        Code = line.PackageUnit
                    };
                    if (!string.IsNullOrEmpty(line.Product.Barcode))
                    {
                        oline.ConfirmationNumber = line.Product.Barcode; //test field
                    }

                    if (line.ContainerField != null)
                    {
                        if (line.ContainerField.Dates != null)
                        {
                            if (line.ContainerField.Dates.Count() > 0)
                            {
                                foreach (var item in line.ContainerField.Dates)
                                {
                                    if (!string.IsNullOrEmpty(item.ActualDate))
                                    {
                                        oline.RequiredInStore = item.ActualDate;
                                    }
                                }

                            }
                        }
                    }

                    if(line.DestinationState != "" && line.DestinationState != null)
                    {
                        List<CustomizedField> custColl = new List<CustomizedField>();

                        CustomizedField cust = new CustomizedField();

                        cust.DataType = CustomizedFieldDataType.String;
                        cust.Key = "Destination";
                        cust.Value = line.DestinationState;

                        custColl.Add(cust);
                        oline.CustomizedFieldCollection = custColl.ToArray();
                    }
                    linesOrderLines.Add(oline);
                }
                linesColl.OrderLine = linesOrderLines.ToArray();
                return linesColl;
            }
            catch (Exception)
            {

                return null;
            }

        }

        private OrganizationAddress[] GetCompaniesFromCommon(CompanyElement[] companies)
        {
            List<OrganizationAddress> addColl = new List<OrganizationAddress>();
            foreach (CompanyElement company in companies)
            {
                OrganizationAddress org = new OrganizationAddress
                {
                    CompanyName = company.CompanyName,
                    Address1 = company.Address1,
                    Address2 = company.Address2,
                    City = company.City,
                    State = company.State,
                    Postcode = company.PostCode,
                    Phone = company.PhoneNo,
                    OrganizationCode = string.IsNullOrEmpty(company.CompanyOrgCode) ? company.CompanyCode : company.CompanyOrgCode,
                    AddressOverride = true,
                    AddressOverrideSpecified = true
                };
                org.Country = new Country
                {
                    Code = company.Country == null ? null : company.Country,
                    Name = company.CountryCode == null ? null : company.CountryCode
                };
                switch (company.CompanyType)
                {

                    case CompanyElementCompanyType.Consignee:

                        org.AddressType = "ConsigneeDocumentaryAddress";

                        break;
                    case CompanyElementCompanyType.Consignor:
                        org.AddressType = "ConsignorDocumentaryAddress";
                        break;
                    case CompanyElementCompanyType.Supplier:
                        org.AddressType = "ConsignorDocumentaryAddress";
                        break;

                    case CompanyElementCompanyType.DeliveryAddress:
                        if (_cwType == "WarehouseOrder")
                        {
                            org.AddressType = "ConsigneeAddress";
                        }
                        else
                        {
                            org.AddressType = "ConsigneePickupDeliveryAddress";
                        }
                        break;
                    case CompanyElementCompanyType.PickupAddress:
                        org.AddressType = "ConsignorPickupDeliveryAddress";
                        break;
                    case CompanyElementCompanyType.ForwardingAgent:
                        org.AddressType = "SendingForwarderAddress";
                        break;
                    case CompanyElementCompanyType.ImportBroker:
                        org.AddressType = "ImportBroker";
                        break;
                    case CompanyElementCompanyType.ShippingLine:
                        org.AddressType = "ShippingLineAddress";
                        break;
                    case CompanyElementCompanyType.BillToAddress:
                        org.AddressType = "SendersLocalClient";
                        break;
                    case CompanyElementCompanyType.OrderDeliveryAddress:
                        org.AddressType = "ConsigneeAddress";
                        break;
                }
                addColl.Add(org);
            }

            return addColl.ToArray();
        }

        private Date[] GetDatesFromCommon(DateElement[] commonDates)
        {
            if (commonDates != null)
            {
                List<Date> cwDates = new List<Date>();
                foreach (DateElement date in commonDates)
                {
                    Date cwDate = new Date();
                    switch (date.DateType)
                    {
                        case DateElementDateType.Required:
                            cwDate.Type = DateType.DeliveryRequiredBy;
                            break;
                        case DateElementDateType.Arrival:
                            cwDate.Type = DateType.Arrival;
                            break;
                        case DateElementDateType.Booked:
                            cwDate.Type = DateType.BookingConfirmed;
                            break;
                        case DateElementDateType.Closing:
                            cwDate.Type = DateType.CutOffDate;
                            break;
                        case DateElementDateType.CustomsCleared:
                            cwDate.Type = DateType.EntryDate;
                            break;
                        case DateElementDateType.DeliverBy:
                            cwDate.Type = DateType.ClientRequestedETA;
                            break;
                        case DateElementDateType.Delivered:
                            cwDate.Type = DateType.Delivery;
                            break;
                        case DateElementDateType.Departure:
                            cwDate.Type = DateType.Departure;
                            break;
                        case DateElementDateType.ExFactory:
                            cwDate.Type = DateType.AvailableExFactory;
                            break;
                        case DateElementDateType.InStoreDateReq:
                            cwDate.Type = DateType.DeliveryRequiredBy;
                            break;
                        case DateElementDateType.Loading:
                            cwDate.Type = DateType.LoadingDate;
                            break;
                        case DateElementDateType.Raised:
                            cwDate.Type = DateType.OrderDate;
                            break;
                        case DateElementDateType.Ordered:
                            cwDate.Type = DateType.OrderDate;
                            break;
                        case DateElementDateType.Unloading:
                            cwDate.Type = DateType.Unpack;
                            break;

                    }
                    if (!string.IsNullOrEmpty(date.ActualDate))
                    {
                        cwDate.Value = DateTime.Parse(date.ActualDate).ToString("yyyy-MM-ddThh:mm:ss");
                        cwDate.IsEstimate = false;
                        cwDate.IsEstimateSpecified = true;
                        cwDates.Add(cwDate);
                    }
                    if (!string.IsNullOrEmpty(date.EstimateDate))
                    {
                        cwDate.Value = DateTime.Parse(date.EstimateDate).ToString("yyyy-MM-ddThh:mm:ss");
                        cwDate.IsEstimate = true;
                        cwDate.IsEstimateSpecified = true;
                        cwDates.Add(cwDate);
                    }
                }
                return cwDates.ToArray();
            }
            return null;
        }
        private ShipmentLocalProcessing GetLocalProcessing(DateElement[] commonDates)
        {
            var localProcessing = new ShipmentLocalProcessing();
            bool hasValue = false;
            if (commonDates != null)
            {
                List<Date> cwDates = new List<Date>();
                foreach (DateElement date in commonDates)
                {
                    Date cwDate = new Date();
                    switch (date.DateType)
                    {
                        case DateElementDateType.Required:
                            cwDate.Type = DateType.DeliveryRequiredBy;
                            break;
                        case DateElementDateType.Arrival:
                            cwDate.Type = DateType.Arrival;
                            break;
                        case DateElementDateType.Booked:
                            cwDate.Type = DateType.BookingConfirmed;
                            break;
                        case DateElementDateType.Closing:
                            cwDate.Type = DateType.CutOffDate;
                            break;
                        case DateElementDateType.CustomsCleared:
                            cwDate.Type = DateType.EntryDate;
                            break;
                        case DateElementDateType.DeliverBy:
                            cwDate.Type = DateType.ClientRequestedETA;
                            break;
                        case DateElementDateType.Delivered:
                            cwDate.Type = DateType.Delivery;
                            break;
                        case DateElementDateType.Departure:
                            cwDate.Type = DateType.Departure;
                            break;
                        case DateElementDateType.ExFactory:
                            cwDate.Type = DateType.AvailableExFactory;
                            break;
                        case DateElementDateType.InStoreDateReq:
                            cwDate.Type = DateType.DeliveryRequiredBy;
                            break;
                        case DateElementDateType.Loading:
                            cwDate.Type = DateType.LoadingDate;
                            break;
                        case DateElementDateType.Raised:
                            cwDate.Type = DateType.OrderDate;
                            break;
                        case DateElementDateType.Ordered:
                            cwDate.Type = DateType.OrderDate;
                            break;
                        case DateElementDateType.Unloading:
                            cwDate.Type = DateType.Unpack;
                            break;

                    }
                    if (!string.IsNullOrEmpty(date.ActualDate))
                    {
                        cwDate.Value = DateTime.Parse(date.ActualDate).ToString("yyyy-MM-ddThh:mm:ss");
                        cwDate.IsEstimate = false;
                        cwDate.IsEstimateSpecified = true;
                        cwDates.Add(cwDate);
                        hasValue = true;
                        localProcessing.DeliveryRequiredBy = DateTime.Parse(date.ActualDate).ToString("yyyy-MM-ddThh:mm:ss");
                        break;
                    }
                }

                if (hasValue)
                    return localProcessing;
                else
                    return null;
            }
            return null;
        }
        private XMLLocker.Cargowise.XUS.UniversalInterchange CreateFramework(string dtType)
        {
            XMLLocker.Cargowise.XUS.UniversalInterchange frame = new XMLLocker.Cargowise.XUS.UniversalInterchange();
            Shipment shipment = new Shipment();
            DataTarget dt = new DataTarget
            {
                Type = dtType
            };
            List<DataTarget> dtColl = new List<DataTarget> { dt };
            XMLLocker.Cargowise.XUS.DataContext dc = new XMLLocker.Cargowise.XUS.DataContext
            {
                DataTargetCollection = dtColl.ToArray()
            };
            dc.ActionPurpose = new CodeDescriptionPair
            {
                Code = "CTC",
                Description = "Imported by CTC Node"
            };

            dc.EventType = new CodeDescriptionPair
            {
                Code = "DIM",
                Description = "Data Imported"
            };
            shipment.DataContext = dc;
            XMLLocker.Cargowise.XUS.UniversalInterchangeBody body = new XMLLocker.Cargowise.XUS.UniversalInterchangeBody();
            UniversalShipmentData bodyData = new UniversalShipmentData();
            bodyData.version = "1.1";
            bodyData.Shipment = shipment;
            body.BodyField = bodyData;
            frame.Body = body;
            frame.Header = new XMLLocker.Cargowise.XUS.UniversalInterchangeHeader();
            return frame;
        }
        public string ToFile(XUS.UniversalInterchange cw, string fileName, string path)
        {
            String cwXML = Path.Combine(path, fileName + ".xml");
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(path, fileName + iFileCount + ".xml");
            }
            using (Stream outputCW = File.Open(cwXML, FileMode.Create))
            {
                var cwNSUniversal = new XmlSerializerNamespaces();
                cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(XUS.UniversalInterchange));
                xSer.Serialize(outputCW, cw, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();
            }
            return cwXML;
        }

        public string ToFile(XMLLocker.Cargowise.XUS.UniversalInterchange cw, string fileName, string path)
        {
            String cwXML = Path.Combine(path, fileName + ".xml");
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(path, fileName + iFileCount + ".xml");
            }
            using (Stream outputCW = File.Open(cwXML, FileMode.Create))
            {
                var cwNSUniversal = new XmlSerializerNamespaces();
                cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.XUS.UniversalInterchange));
                xSer.Serialize(outputCW, cw, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();
            }
            return cwXML;
        }
        #endregion











    }
}

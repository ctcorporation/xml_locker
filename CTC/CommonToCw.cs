﻿using CTCLogging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using MoreLinq;
using XUS;

namespace XMLLocker.CTC

{
    public class CommonToCw
    {

        public string LogPath { get; set; }

        public CommonToCw()
        {

        }

        public CommonToCw(string logPath)
        {
            LogPath = logPath;
        }

        public XUE.UniversalInterchange CreateEvent(NodeFile common, string outtutpath)
        {
            XUE.UniversalInterchange cwEvent = CreateEventHeader(common.IdendtityMatrix);
            //cwEvent.Body = CreateEventBody()
            return cwEvent;

        }

        private XUE.UniversalInterchange CreateEventHeader(NodeFileIdendtityMatrix idendtityMatrix)
        {
            XUE.UniversalInterchange eventHeader = new XUE.UniversalInterchange();
            eventHeader.Header = new XUE.UniversalInterchangeHeader
            {
                RecipientID = idendtityMatrix.CustomerId,
                SenderID = idendtityMatrix.SenderId
            };
            return eventHeader;
        }

        public UniversalInterchange CreateOrderFile(NodeFile common)
        {
            //XUS.UniversalInterchange uic = CreateInterchange(common.IdendtityMatrix);
            //ShipmentOrder order = new ShipmentOrder();
            //List<UniversalShipmentData> orderColl = new List<UniversalShipmentData>();
            //UniversalShipmentData shipmentData = new UniversalShipmentData();
            //List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            //Shipment shipment = new Shipment();
            //DataContext dc = new DataContext();
            //List<DataTarget> dtColl = new List<DataTarget>();
            //DataTarget dt = new DataTarget();
            //List<OrganizationAddress> orgAddressColl = new List<OrganizationAddress>();
            //OrganizationAddress orgAdd = new OrganizationAddress();
            foreach (NodeFileTrackingOrder ord in common.TrackingOrders)
            {
                XUS.UniversalInterchange uic = CreateInterchange(common.IdendtityMatrix);
                ShipmentOrder order = new ShipmentOrder();
                List<UniversalShipmentData> orderColl = new List<UniversalShipmentData>();
                UniversalShipmentData shipmentData = new UniversalShipmentData();
                List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                Shipment shipment = new Shipment();
                DataContext dc = new DataContext();
                List<DataTarget> dtColl = new List<DataTarget>();
                DataTarget dt = new DataTarget();
                List<OrganizationAddress> orgAddressColl = new List<OrganizationAddress>();
                OrganizationAddress orgAdd = new OrganizationAddress();
                shipment.OrganizationAddressCollection = GetCompaniesFromCompanies(ord.Companies).ToArray();
                shipment.DateCollection = GetDatesFromCompanies(ord.Dates).ToArray();
                dt.Type = "OrderManagerOrder"; // Need to revisit this - Not to hardocode                       
                dt.Key = "";
                CodeDescriptionPair eventType = new CodeDescriptionPair
                {
                    Code = common.IdendtityMatrix.EventCode
                };

                dtColl.Add(dt);
                dc.DataTargetCollection = dtColl.ToArray();
                dc.DataProvider = common.IdendtityMatrix.SenderId;
                dc.EventType = new CodeDescriptionPair
                {
                    Code = string.IsNullOrEmpty(common.IdendtityMatrix.EventCode) ? "ADD" : common.IdendtityMatrix.EventCode
                };
                shipment.DataContext = dc;
                if (!string.IsNullOrEmpty(ord.IncoTerms))
                {
                    shipment.ShipmentIncoTerm = new IncoTerm { Code = ord.IncoTerms };
                }
                if (!string.IsNullOrEmpty(ord.TransportMode))
                {
                    shipment.TransportMode = new CodeDescriptionPair { Code = ord.TransportMode };
                }
                if (!string.IsNullOrEmpty(ord.ContainerMode))
                {
                    shipment.ContainerMode = new ContainerMode { Code = ord.ContainerMode };
                }
                var custreferences = GetCustomerReferencesFromOrder(ord);
                if (custreferences != null)
                {
                    shipment.CustomizedFieldCollection = custreferences.ToArray();

                }
                shipment.ContainerCollection = GetContainersFromOrder(ord);


                shipment.Order = GetOrderFromOrder(ord);
                shipment.GoodsDescription = ord.GoodsDescription;
                uic.Body = new UniversalInterchangeBody
                {
                    BodyField = new UniversalShipmentData
                    {
                        Shipment = shipment,
                        version = "1.1"
                    }
                };

                return uic;
            }
            return null;

        }

        public UniversalInterchange CreateTransportFile(NodeFile common)
        {
            foreach (NodeFileTrackingOrder ord in common.TrackingOrders)
            {
                XUS.UniversalInterchange uic = CreateInterchange(common.IdendtityMatrix);
                ShipmentOrder order = new ShipmentOrder();
                List<UniversalShipmentData> orderColl = new List<UniversalShipmentData>();
                UniversalShipmentData shipmentData = new UniversalShipmentData();
                List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                Shipment shipment = new Shipment();
                DataContext dc = new DataContext();
                List<DataSource> dtsColl = new List<DataSource>();
                DataSource dts = new DataSource();
                dts.Type = "ForwardingConsol";
                dts.Key = "C00453592";
                dtsColl.Add(dts);
                DataSource dts1 = new DataSource();
                dts1.Type = "ForwardingShipment";
                dts1.Key = "S00679826"; //Excel file SO
                dtsColl.Add(dts1);
                DataSource dts2 = new DataSource();
                dts2.Type = "CustomsDeclaration";
                dts2.Key = "S00679826"; //Excel file SO
                dtsColl.Add(dts2);
                dc.DataSourceCollection = dtsColl.ToArray();
                dc.ActionPurpose = new CodeDescriptionPair()
                {
                    Code = "EVT",
                    Description = "Event"
                };
                dc.Company = new Company()
                {
                    Code = "",
                    Country = new Country()
                    {
                        Code = "AU",
                        Name = "Australia"
                    },
                    Name = "" //Excel company
                };
                dc.DataProvider = common.IdendtityMatrix.SenderId;
                dc.EventType = new CodeDescriptionPair
                {
                    Code = string.IsNullOrEmpty(common.IdendtityMatrix.EventCode) ? "ADD" : common.IdendtityMatrix.EventCode
                };
                shipment.DataContext = dc;
                //shipment.AWBServiceLevel

                List<OrganizationAddress> orgAddressColl = new List<OrganizationAddress>();
                OrganizationAddress orgAdd = new OrganizationAddress();
                shipment.OrganizationAddressCollection = GetCompaniesFromCompanies(ord.Companies).ToArray();
                shipment.DateCollection = GetDatesFromCompanies(ord.Dates).ToArray();
                if (!string.IsNullOrEmpty(ord.IncoTerms))
                {
                    shipment.ShipmentIncoTerm = new IncoTerm { Code = ord.IncoTerms };
                }
                if (!string.IsNullOrEmpty(ord.TransportMode))
                {
                    shipment.TransportMode = new CodeDescriptionPair { Code = ord.TransportMode };
                }
                if (!string.IsNullOrEmpty(ord.ContainerMode))
                {
                    shipment.ContainerMode = new ContainerMode { Code = ord.ContainerMode };
                }
                var custreferences = GetCustomerReferencesFromOrder(ord);
                if (custreferences != null)
                {
                    shipment.CustomizedFieldCollection = custreferences.ToArray();

                }
                shipment.ContainerCollection = GetContainersFromOrder(ord);


                shipment.Order = GetOrderFromOrder(ord);
                shipment.GoodsDescription = ord.GoodsDescription;
                uic.Body = new UniversalInterchangeBody
                {
                    BodyField = new UniversalShipmentData
                    {
                        Shipment = shipment,
                        version = "1.1"
                    }
                };

                return uic;
            }
            return null;

        }

        private ShipmentContainerCollection GetContainersFromOrder(NodeFileTrackingOrder ord)
        {
            ShipmentContainerCollection contCollection = new ShipmentContainerCollection
            {
                Content = CollectionContent.Partial,
                ContentSpecified = true
            };
            if (ord.Containers != null)
            {
                if (ord.Containers.Length > 0)
                {
                    List<Container> containers = new List<Container>();
                    foreach (var container in ord.Containers)
                    {
                        var newCont = new Container
                        {
                            ContainerCount = ord.Containers.Length,
                            ContainerCountSpecified = true,
                            ContainerNumber = container.ContainerNo,
                            ContainerType = new ContainerContainerType { Code = container.ContainerCode },
                            GoodsWeight = container.GoodsWeight,
                            GoodsWeightSpecified = true

                        };
                        containers.Add(newCont);
                    }
                    contCollection.Container = containers.ToArray();
                }
            }
            return contCollection;
        }

        public UniversalInterchange CreateOrderFile(NodeFile common, string outputpath)
        {
            //XUS.UniversalInterchange uic = CreateInterchange(common.IdendtityMatrix);
            //ShipmentOrder order = new ShipmentOrder();
            //List<UniversalShipmentData> orderColl = new List<UniversalShipmentData>();
            //UniversalShipmentData shipmentData = new UniversalShipmentData();
            //List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            //Shipment shipment = new Shipment();
            //DataContext dc = new DataContext();
            //List<DataTarget> dtColl = new List<DataTarget>();
            //DataTarget dt = new DataTarget();
            //List<OrganizationAddress> orgAddressColl = new List<OrganizationAddress>();
            //OrganizationAddress orgAdd = new OrganizationAddress();
            foreach (NodeFileTrackingOrder ord in common.TrackingOrders)
            {
                XUS.UniversalInterchange uic = CreateInterchange(common.IdendtityMatrix);
                ShipmentOrder order = new ShipmentOrder();
                List<UniversalShipmentData> orderColl = new List<UniversalShipmentData>();
                UniversalShipmentData shipmentData = new UniversalShipmentData();
                List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                Shipment shipment = new Shipment();
                DataContext dc = new DataContext();
                List<DataTarget> dtColl = new List<DataTarget>();
                DataTarget dt = new DataTarget();
                List<OrganizationAddress> orgAddressColl = new List<OrganizationAddress>();
                OrganizationAddress orgAdd = new OrganizationAddress();
                dtColl = new List<DataTarget>();
                shipment.OrganizationAddressCollection = GetCompaniesFromCompanies(ord.Companies).ToArray();
                shipment.DateCollection = GetDatesFromCompanies(ord.Dates).ToArray();
                dt.Type = "OrderManagerOrder"; // Need to revisit this - Not to hardocode                       
                dt.Key = "";
                CodeDescriptionPair eventType = new CodeDescriptionPair
                {
                    Code = common.IdendtityMatrix.EventCode
                };
                dtColl.Add(dt);
                dc.DataProvider = common.IdendtityMatrix.SenderId;
                dc.EventType = new CodeDescriptionPair
                {
                    Code = string.IsNullOrEmpty(common.IdendtityMatrix.EventCode) ? "ADD" : common.IdendtityMatrix.EventCode
                };
                dc.DataTargetCollection = dtColl.ToArray();
                shipment.DataContext = dc;
                if (!string.IsNullOrEmpty(ord.IncoTerms))
                {
                    shipment.ShipmentIncoTerm = new IncoTerm { Code = ord.IncoTerms };
                }
                if (!string.IsNullOrEmpty(ord.TransportMode))
                {
                    shipment.TransportMode = new CodeDescriptionPair { Code = ord.TransportMode };
                }
                if (!string.IsNullOrEmpty(ord.ContainerMode))
                {
                    shipment.ContainerMode = new ContainerMode { Code = ord.ContainerMode };
                }
                var custreferences = GetCustomerReferencesFromOrder(ord);
                if (custreferences != null)
                {
                    shipment.CustomizedFieldCollection = custreferences.ToArray();

                }
                shipment.ContainerCollection = GetContainersFromOrder(ord);

                shipment.Order = GetOrderFromOrder(ord);
                shipment.GoodsDescription = ord.GoodsDescription;
                uic.Body = new UniversalInterchangeBody
                {
                    BodyField = new UniversalShipmentData
                    {
                        Shipment = shipment,
                        version = "1.1"
                    }
                };
                ToCargowise toCargowise = new ToCargowise(LogPath);
                char[] chars = new char[] { '/', '#', '&', '\\', '?', '\'', ',', '\"' };

                toCargowise.ToFile(uic, common.IdendtityMatrix.SenderId + "-ORD-" + ord.OrderNo.RemoveChars(chars) + ".XML", outputpath);

            }
            return null;

        }


        public UniversalInterchange CreateTransportFile(NodeFile common, string outputpath)
        {
            string orderNo = common.TransportJob[0].OrderNo;
            
                XUS.UniversalInterchange uic = CreateInterchange(common.IdendtityMatrix);
                ShipmentOrder order = new ShipmentOrder();
                List<UniversalShipmentData> orderColl = new List<UniversalShipmentData>();
                UniversalShipmentData shipmentData = new UniversalShipmentData();
                List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                Shipment shipment = new Shipment();

                DataContext dc = new DataContext();
                List<DataTarget> dtsColl = new List<DataTarget>();
                DataTarget dts = new DataTarget();
                dc.ActionPurpose = new CodeDescriptionPair
                {
                    Code = "CTC",
                    Description = "Imported by CTC Node"
                };

                dc.EventType = new CodeDescriptionPair
                {
                    Code = "DIM",
                    Description = "Data Imported"
                };

                dts.Type = "LocalTransport";
                dtsColl.Add(dts);
                dc.DataTargetCollection = dtsColl.ToArray();
                dc.DataProvider = common.IdendtityMatrix.SenderId;
                shipment.DataContext = dc;

                shipment.Branch = new Branch
                {
                    Code = "SYD"
                };

                shipment.ContainerMode = new ContainerMode
                {
                    Code = "LSE"
                };
                
                //shipment.GoodsDescription = common.TransportJob[0].FactorySort;
                shipment.LocalTransportEquipmentNeeded = new CodeDescriptionPair
                {
                    Code = "PSL"
                };


                shipment.LocalTransportJobType = new CodeDescriptionPair4Char
                {
                    Code = common.TransportJob[0].JobTypeCode,
                    Description = common.TransportJob[0].JobType
                };
            shipment.Order = new ShipmentOrder
            {
                OrderNumber = common.TransportJob[0].OrderNo
                };


                var companies = (from x in common.TransportJob
                                 select x.Company).ToList();

                List<ShipmentInstruction> instColl = new List<ShipmentInstruction>();


                ShipmentInstruction inst = new ShipmentInstruction();


                inst = new ShipmentInstruction();

                inst.Address = new OrganizationAddress
                {
                    AddressType = "LocalCartageCFS",
                    Country = new Country
                    {
                        Code = "AU",
                        Name = "Australia"
                    },
                    OrganizationCode = "PAUCUSSYD",
                    Port = new UNLOCO
                    {
                        Code = "AUSYD",
                        Name = "SYD"
                    },
                    CompanyName = "PAULS CUSTOMS AND FORWARDING SOLUTIONS PTY LTD"
                };

                inst.SequenceSpecified = true;

                inst.Sequence = common.TransportJob[0].StartLeg;

                inst.Type = new CodeDescriptionPair
                {
                    Code = "PIC",
                    Description = "Pickup"
                };
            List<ShipmentInstructionInstructionPackingLineLink> picLegColl = new List<ShipmentInstructionInstructionPackingLineLink>();
            
                
            List<ShipmentInstructionInstructionPackingLineLink> plColl = new List<ShipmentInstructionInstructionPackingLineLink>();

                ShipmentInstructionInstructionPackingLineLink pl = new ShipmentInstructionInstructionPackingLineLink();


            instColl.Add(inst);
                int sequence = common.TransportJob[0].StartLeg;
                int link = common.TransportJob[0].StartLeg;
                List<PackingLine> plines = new List<PackingLine>();
                List<TransportLeg> tlColl = new List<TransportLeg>();
            List<Confirmation> cfmColl = new List<Confirmation>();
            
            foreach(NodeFileTransportJob job in common.TransportJob)
            {
                sequence += 1;

                inst = new ShipmentInstruction();


                inst.Address = new OrganizationAddress
                {
                    AddressType = "LocalCartageImporter",
                    Address1 = job.Company.Address1,
                    AddressOverrideSpecified = true,
                    AddressOverride = true,
                    AddressShortCode = job.Company.Address1,
                    City = job.Company.City,
                    CompanyName = job.Company.CompanyName,
                    Country = new Country
                    {
                        Code = "AU",
                        Name = "Australia"
                    },
                    //Email = comp.EmailAddress,
                    OrganizationCode = "NORAUSBNE",
                    Phone = job.Company.PhoneNo,
                    Port = new UNLOCO
                    {
                        Code = "AUSYD",
                        Name = job.Company.Port
                    },
                    Postcode = job.Company.PostCode,
                    State = job.Company.State,
                };


                inst.SequenceSpecified = true;

                inst.Sequence = sequence;

                inst.Type = new CodeDescriptionPair
                {
                    Code = "DLV",
                    Description = "Delivery"
                };

                

                plColl = new List<ShipmentInstructionInstructionPackingLineLink>();


                PackingLine pline = new PackingLine();
                TransportLeg tl = new TransportLeg();
                Confirmation cfm = new Confirmation();
                
                    link += 1;
                    pl = new ShipmentInstructionInstructionPackingLineLink();


                    pl.PackingLineLink = link;
                    pl.PackingLineLinkSpecified = true;
                    pl.Quantity = int.Parse(job.OrderLine.OrderQty.ToString());
                    pl.QuantitySpecified = true;

                    cfmColl = new List<Confirmation>();

                    cfm = new Confirmation();
                    cfm.DateDescription = "DLV";
                    cfm.Distance = decimal.Parse("0.000");
                    cfm.DistanceUnit = new UnitOfLength
                    {
                        Code = "KM",
                        Description = "Kilometres"
                    };
                    cfm.IsEmptyContainer = false;
                    cfm.LegLink = link;
                    cfm.LegLinkSpecified = true;
                    cfm.Quantity = int.Parse(job.OrderLine.OrderQty.ToString());
                    cfm.QuantitySpecified = true;

                    cfmColl.Add(cfm);
                    pl.ConfirmationCollection = cfmColl.ToArray();
                    plColl.Add(pl);

                //add to pickup leg

                pl = new ShipmentInstructionInstructionPackingLineLink();


                pl.PackingLineLink = link;
                pl.PackingLineLinkSpecified = true;
                pl.Quantity = int.Parse(job.OrderLine.OrderQty.ToString());
                pl.QuantitySpecified = true;

                cfmColl = new List<Confirmation>();

                cfm = new Confirmation();
                cfm.DateDescription = "PIC";
                cfm.Distance = decimal.Parse("0.000");
                cfm.DistanceUnit = new UnitOfLength
                {
                    Code = "KM",
                    Description = "Kilometres"
                };
                cfm.IsEmptyContainer = false;
                cfm.LegLink = link;
                cfm.LegLinkSpecified = true;
                cfm.Quantity = int.Parse(job.OrderLine.OrderQty.ToString());
                cfm.QuantitySpecified = true;

                cfmColl.Add(cfm);
                pl.ConfirmationCollection = cfmColl.ToArray();

                picLegColl.Add(pl);

                pline = new PackingLine();
                    //pline.HeightSpecified = true;
                    //pline.Height = line.Height;
                    //pline.LengthSpecified = true;
                    //pline.Length = line.PackingSize;
                    //pline.LengthUnit = new UnitOfLength
                    //{
                    //    Code = "CM",
                    //    Description = "Centimetres"
                    //};

                    pline.Link = link;
                    pline.LinkSpecified = true;
                    pline.PackQty = int.Parse(job.OrderLine.OrderQty.ToString());
                    pline.PackQtySpecified = true;
                    pline.PackType = new PackageType
                    {
                        Code = "CTN"

                    };

                    pline.Volume = job.OrderLine.TotalCubage;
                    pline.VolumeSpecified = true;
                    pline.VolumeUnit = new UnitOfVolume
                    {
                        Code = "M3",
                        Description = "Cubic Metres"
                    };

                    pline.Weight = job.OrderLine.Weight;
                    pline.WeightSpecified = true;
                    pline.WeightUnit = new UnitOfWeight
                    {
                        Code = "KG",
                        Description = "Kilograms"
                    };
                    pline.WidthSpecified = true;
                    //pline.Width = line.Width;

                    plines.Add(pline);

                    tl = new TransportLeg();

                    tl.LegOrder = (byte)link;
                    tl.LegTypeSpecified = true;
                    tl.LegType = TransportLegLegType.LocalTransport;
                    tl.Link = link;
                    tl.LinkSpecified = true;
                    tl.TransportMode = TransportLegTransportMode.Road;
                    tl.TransportModeSpecified = true;
                    tlColl.Add(tl);

                

                inst.InstructionPackingLineLinkCollection = plColl.ToArray();

                instColl.Add(inst);
            }



            instColl.ElementAt(0).InstructionPackingLineLinkCollection = picLegColl.ToArray();

                shipment.InstructionCollection = instColl.ToArray();

                List<OrganizationAddress> orgColl = new List<OrganizationAddress>();

                OrganizationAddress org = new OrganizationAddress();

                org = new OrganizationAddress
                {
                    AddressType = "SendersLocalClient",
                    //Address1 = "PO BOX 973",
                    //AddressShortCode = "PO BOX 973",
                    //City = "HAMILTON CENTRAL",
                    //CompanyName = "NORMAN AUSTRALIA PTY LTD",
                    //Country = new Country
                    //{
                    //    Code = "AU",
                    //    Name = "Australia"
                    //},
                    //Email = "",
                    //GovRegNum = "50629428780",
                    //GovRegNumType = new RegistrationNumberType
                    //{
                    //    Code = "ABN",
                    //    Description = "Australian Business Number (GST Reg"
                    //},
                    OrganizationCode = "NORAUSBNE"
                    //Phone = "+61732561068",
                    //Port = new UNLOCO
                    //{
                    //    Code = "AUBNE",
                    //    Name = "Brisbane"
                    //},
                    //Postcode = "4007",
                    //ScreeningStatus = new CodeDescriptionPair
                    //{
                    //    Code = "NOT",
                    //    Description = "Not Screened"
                    //},
                    //State = "Queensland",
                };


                orgColl.Add(org);
                shipment.OrganizationAddressCollection = orgColl.ToArray();




                ShipmentPackingLineCollection plineColl = new ShipmentPackingLineCollection();

                plineColl.PackingLine = plines.ToArray();
                shipment.PackingLineCollection = plineColl;

                ShipmentTransportLegCollection tls = new ShipmentTransportLegCollection();
                
                
                tls.TransportLeg = tlColl.ToArray();

                shipment.TransportLegCollection = tls;

                uic.Body = new UniversalInterchangeBody
                {
                    BodyField = new UniversalShipmentData
                    {
                        Shipment = shipment,
                        version = "1.1"
                    }
                };
                ToCargowise toCargowise = new ToCargowise(LogPath);
                char[] chars = new char[] { '/', '#', '&', '\\', '?', '\'', ',', '\"' };

                toCargowise.ToFile(uic, common.IdendtityMatrix.SenderId + "-JOB-" + orderNo, outputpath);
            
            return null;

        }



        public UniversalInterchange CreateTMCFile(NodeFile common, string outputpath)
        {

            string orderNo = "FSCU4641830";

            XUS.UniversalInterchange uic = CreateInterchange(common.IdendtityMatrix);
            ShipmentOrder order = new ShipmentOrder();
            List<UniversalShipmentData> orderColl = new List<UniversalShipmentData>();
            UniversalShipmentData shipmentData = new UniversalShipmentData();
            List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            Shipment shipment = new Shipment();

            DataContext dc = new DataContext();
            List<DataTarget> dtsColl = new List<DataTarget>();
            DataTarget dts = new DataTarget();
            dc.ActionPurpose = new CodeDescriptionPair
            {
                Code = "CTC",
                Description = "Imported by CTC Node"
            };

            dc.EventType = new CodeDescriptionPair
            {
                Code = "DIM",
                Description = "Data Imported"
            };

            dts.Type = "LocalTransport";
            dtsColl.Add(dts);
            dc.DataTargetCollection = dtsColl.ToArray();
            dc.DataProvider = common.IdendtityMatrix.SenderId;
            shipment.DataContext = dc;

            //shipment.Branch = new Branch
            //{
            //    Code = "SYD"
            //};

            shipment.ContainerMode = new ContainerMode
            {
                Code = common.TransportJob[0].TransportMode
            };

            shipment.GoodsDescription = common.TransportJob[0].ShipmentNo;
            //shipment.LocalTransportEquipmentNeeded = new CodeDescriptionPair
            //{
            //    Code = "PSL"
            //};


            //shipment.LocalTransportJobType = new CodeDescriptionPair4Char
            //{
            //    Code = "DRLC"
            //};
            shipment.Order = new ShipmentOrder
            {
                OrderNumber = common.TransportJob[0].OrderNo
        };


            

            List<ShipmentInstruction> instColl = new List<ShipmentInstruction>();
            ShipmentInstruction inst = new ShipmentInstruction();
            int sequence = 0;
            foreach (CompanyElement comp in common.TransportJob[0].Companies)
            {
                
                inst = new ShipmentInstruction();
                if (comp.CompanyType == CompanyElementCompanyType.PickupAddress) {
                    sequence += 1;
                    inst.Address = new OrganizationAddress
                    {
                        AddressType = "LocalCartageCFS",
                        Address1 = comp.Address1,
                        AddressOverrideSpecified = true,
                        AddressOverride = true,
                        AddressShortCode = comp.Address1,
                        City = comp.City,
                        CompanyName = comp.CompanyName,
                        Contact = comp.ContactName,
                        Country = new Country
                        {
                            Code = comp.Country
                        },
                        Phone = comp.PhoneNo,
                        State = comp.State,
                    };

                    inst.Type = new CodeDescriptionPair
                    {
                        Code = "PIC",
                        Description = "Pickup"
                    };

                    inst.SequenceSpecified = true;

                    inst.Sequence = sequence;
                    instColl.Add(inst);
                }
                else if (comp.CompanyType == CompanyElementCompanyType.DeliveryAddress)
                {
                    sequence += 1;
                    inst.Address = new OrganizationAddress
                    {
                        AddressType = "LocalCartageImporter",
                        Address1 = comp.Address1,
                        AddressOverrideSpecified = true,
                        AddressOverride = true,
                        AddressShortCode = comp.Address1,
                        City = comp.City,
                        CompanyName = comp.CompanyName,
                        Contact = comp.ContactName,
                        Country = new Country
                        {
                            Code = comp.Country
                        },
                        Phone = comp.PhoneNo,
                        State = comp.State,
                    };

                    inst.Type = new CodeDescriptionPair
                    {
                        Code = "DLV",
                        Description = "Delivery"
                    };

                    inst.SequenceSpecified = true;

                    inst.Sequence = sequence;
                    instColl.Add(inst);
                }
                

                

            }

            shipment.InstructionCollection = instColl.ToArray();
            List<PackingLine> plines = new List<PackingLine>();
            List<TransportLeg> tlColl = new List<TransportLeg>();
            PackingLine pline = new PackingLine();
            TransportLeg tl = new TransportLeg();
            int link = 0;
            foreach (OrderLineElement line in common.TransportJob[0].OrderLines)
            {
                link += 1;
                pline = new PackingLine();

                pline.ItemNo = (short)line.LineNo;
                pline.ItemNoSpecified = true;
                pline.HeightSpecified = true;
                pline.Height = line.Height;
                pline.LengthSpecified = true;
                pline.Length = line.PackingSize;
                pline.Link = line.LineNo;
                pline.LinkSpecified = true;
                pline.PackQty = int.Parse(line.OrderQty.ToString());
                pline.PackQtySpecified = true;
                pline.PackType = new PackageType
                {
                    Code = line.PackageUnit

                };

                pline.Volume = line.Volume;
                pline.VolumeSpecified = true;

                pline.Weight = line.Weight;
                pline.WeightSpecified = true;

                plines.Add(pline);

                tl = new TransportLeg();

                tl.LegOrder = (byte)link;
                tl.LegTypeSpecified = true;
                tl.LegType = TransportLegLegType.LocalTransport;
                tl.Link = line.LineNo;
                tl.LinkSpecified = true;
                tl.TransportMode = TransportLegTransportMode.Road;
                tl.TransportModeSpecified = true;

                tlColl.Add(tl);
            }

            List<OrganizationAddress> orgColl = new List<OrganizationAddress>();

            OrganizationAddress org = new OrganizationAddress();

            org = new OrganizationAddress
            {
                AddressType = "SendersLocalClient",
                OrganizationCode = "TMC ELC"
            };


            orgColl.Add(org);
            shipment.OrganizationAddressCollection = orgColl.ToArray();



            ShipmentPackingLineCollection plineColl = new ShipmentPackingLineCollection();

            plineColl.PackingLine = plines.ToArray();
            shipment.PackingLineCollection = plineColl;


            ShipmentTransportLegCollection tls = new ShipmentTransportLegCollection();


            tls.TransportLeg = tlColl.ToArray();

            shipment.TransportLegCollection = tls;

            uic.Body = new UniversalInterchangeBody
            {
                BodyField = new UniversalShipmentData
                {
                    Shipment = shipment,
                    version = "1.1"
                }
            };
            ToCargowise toCargowise = new ToCargowise(LogPath);
            char[] chars = new char[] { '/', '#', '&', '\\', '?', '\'', ',', '\"' };

            toCargowise.ToFile(uic, common.IdendtityMatrix.SenderId + "-JOB-" + common.TransportJob[0].OrderNo, outputpath);

            return null;

        }


        private List<CustomizedField> GetCustomerReferencesFromOrder(NodeFileTrackingOrder ord)
        {
            List<CustomizedField> custfields = new List<CustomizedField>();
            if (ord.CustomerReferences == null)
            {
                return null;
            }
            foreach (var custref in ord.CustomerReferences)
            {
                CustomizedField custField = new CustomizedField
                {
                    DataType = CustomizedFieldDataType.String,
                    Key = custref.RefType,
                    Value = custref.RefValue
                };
                custfields.Add(custField);

            }
            if (custfields.Count > 0)
            {
                return custfields;
            }

            return null;



        }

        private ShipmentOrder GetOrderFromOrder(NodeFileTrackingOrder ord)
        {
            ShipmentOrder cwOrder = new ShipmentOrder
            {
                OrderNumber = ord.OrderNo,
                ClientReference = ord.SupplierOrderNo,
                OrderLineCollection = GetOrderLinesFromOrder(ord.OrderLines)



            };

            return cwOrder;
        }

        private ShipmentOrderOrderLineCollection GetOrderLinesFromOrder(OrderLineElement[] orderLines)
        {
            ShipmentOrderOrderLineCollection shipmentOrderLines = new ShipmentOrderOrderLineCollection { Content = CollectionContent.Partial, ContentSpecified = true };
            List<ShipmentOrderOrderLineCollectionOrderLine> orderLineColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            foreach (var line in orderLines)
            {
                ShipmentOrderOrderLineCollectionOrderLine cwLine = new ShipmentOrderOrderLineCollectionOrderLine
                {
                    LineNumber = line.LineNo,
                    LineNumberSpecified = true,
                    Product = new Product { Code = line.Product.Code, Description = line.Product.Description }
                };
                if (line.Weight > 0)
                {
                    cwLine.Weight = line.Weight;
                    cwLine.WeightSpecified = true;
                    cwLine.WeightUnit = new UnitOfWeight { Code = "KG" };
                }
                if (line.OrderQty > 0)
                {
                    cwLine.OrderedQty = line.OrderQty;
                    cwLine.OrderedQtySpecified = true;
                }
                if (line.ExpectedQuantity > 0)
                {
                    cwLine.ExpectedQuantity = line.ExpectedQuantity;
                    cwLine.ExpectedQuantitySpecified = true;
                }


                orderLineColl.Add(cwLine);
            }
            shipmentOrderLines.OrderLine = orderLineColl.ToArray();
            return shipmentOrderLines;
        }

        private List<XUS.Date> GetDatesFromCompanies(DateElement[] dates)
        {
            List<XUS.Date> cwDates = new List<Date>();
            foreach (var date in dates)
            {
                XUS.Date cwDate = new Date
                {
                    Type = ReturnCommonDateType(date.DateType),
                    Value = !string.IsNullOrEmpty(date.ActualDate) ? CwDate(date.ActualDate) : !string.IsNullOrEmpty(CwDate(date.EstimateDate)) ? CwDate(date.EstimateDate) : "",
                    IsEstimate = !string.IsNullOrEmpty(date.ActualDate) ? false : !string.IsNullOrEmpty(date.EstimateDate) ? true : false,
                    IsEstimateSpecified = true
                };
                cwDates.Add(cwDate);
            }
            return cwDates;
        }


        private string CwDate(string dt)
        {
            try
            {
                return DateTime.FromOADate(double.Parse(dt)).ToString("yyyy-MM-ddTHH\\:mm\\:ss");
            }
            catch (Exception)
            {
                return dt;
            }

        }
        private DateType ReturnCommonDateType(DateElementDateType dateType)
        {
            DateType result = DateType.OrderDate;
            switch (dateType)
            {
                case DateElementDateType.Arrival: result = DateType.Arrival; break;
                case DateElementDateType.Available: result = DateType.AvailableExFactory; break;
                case DateElementDateType.Booked: result = DateType.BookingConfirmed; break;
                case DateElementDateType.Closing: result = DateType.CutOffDate; break;
                case DateElementDateType.DeliverBy: result = DateType.DeliveryRequiredBy; break;
                case DateElementDateType.Ordered: result = DateType.OrderDate; break;
                case DateElementDateType.Departure: result = DateType.Departure; break;
                case DateElementDateType.Delivered: result = DateType.Delivery; break;
                case DateElementDateType.InStoreDateReq: result = DateType.DeliveryRequiredBy; break;
            }
            return result;
        }

        private OrganizationAddress GetCompanyByType(CompanyElement[] companies, CompanyElementCompanyType compType)
        {
            var org = (from c in companies
                       where c.CompanyType == compType
                       select new OrganizationAddress
                       {
                           OrganizationCode = c.CompanyCode,
                           CompanyName = c.CompanyName,
                           Address1 = c.Address1,
                           Address2 = c.Address2,
                           City = c.City,
                           State = c.State,
                           Country = new Country { Code = c.CountryCode },
                           Contact = c.ContactName,
                           Email = c.EmailAddress,
                           Postcode = c.PostCode,
                           AddressType = GetCWOrgType(compType)
                       }).FirstOrDefault();
            if (org != null)
            {
                return org;
            }
            return null;


        }
        private List<OrganizationAddress> GetCompaniesFromCompanies(CompanyElement[] companies)
        {
            List<OrganizationAddress> orgColl = new List<OrganizationAddress>();
            foreach (var comp in companies)
            {
                OrganizationAddress oa = new OrganizationAddress
                {
                    Address1 = comp.Address1.Truncate(50),
                    Address2 = comp.Address2.Truncate(50),
                    City = comp.City.Truncate(25),
                    CompanyName = comp.CompanyName.Truncate(100),
                    OrganizationCode = comp.CompanyOrgCode,
                    AddressShortCode = comp.CompanyCode.Truncate(25),
                    State = comp.State.Truncate(25),
                    Postcode = comp.PostCode.Truncate(10),
                    AddressType = GetCWOrgType(comp.CompanyType)
                };
                orgColl.Add(oa);

            }

            return orgColl;
        }

        private string GetCWOrgType(CompanyElementCompanyType companyType)
        {
            string result = string.Empty;

            switch (companyType)
            {
                case CompanyElementCompanyType.Consignee:
                    result = "ConsigneeDocumentaryAddress";
                    break;
                case CompanyElementCompanyType.Buyer:
                    result = "ImporterDocumentaryAddress";
                    break;
                case CompanyElementCompanyType.Consignor:
                    result = "ConsignorDocumentaryAddress";
                    break;
                case CompanyElementCompanyType.Supplier:
                    result = "SupplierDocumentaryAddress";
                    break;
                case CompanyElementCompanyType.DeliveryAddress:
                    result = "ConsigneePickupDeliveryAddress";
                    break;
                // case CompanyElementCompanyType.DeliveryAddress: result = "ImporterPickupDeliveryAddress";
                //     break;
                case CompanyElementCompanyType.TransportCompany:
                    result = "DeliveryLocalCartage";
                    break;
                case CompanyElementCompanyType.PickupAddress:
                    result = "ConsignorPickupDeliveryAddress";
                    break;
                case CompanyElementCompanyType.ForwardingAgent:
                    result = "SendingForwarderAddress";
                    break;
                case CompanyElementCompanyType.ImportBroker:
                    result = "ImportBroker";
                    break;
                //case CompanyElementCompanyType.ShippingLine: result = "ShippingLineAddress";
                //    break;
                case CompanyElementCompanyType.ShippingLine:
                    result = "ShippingLine";
                    break;
                case CompanyElementCompanyType.ReceivingAgent:
                    result = "ReceivingForwarderAddress";
                    break;
            }
            return result;
        }

        private UniversalInterchange CreateInterchange(NodeFileIdendtityMatrix idendtityMatrix)
        {
            XUS.UniversalInterchange uic = new UniversalInterchange
            {
                Header = new UniversalInterchangeHeader
                {
                    SenderID = idendtityMatrix.SenderId,
                    RecipientID = idendtityMatrix.CustomerId
                }
            };
            return uic;

        }

        public bool CreateProductFile(NodeFile common, XMLLocker.Cargowise.NDM.Action partOperation, string outputPath)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            if (common.ProductImport != null)
            {
                string lo = LogPath;
                string cp = string.Empty;
                string de = "Processing Folder Queue";
                DateTime ti = DateTime.Now;
                string er = string.Empty;
                string di = string.Empty;
                foreach (ProductElement product in common.ProductImport)
                {
                    int iRec = 0;
                    try
                    {
                        Cargowise.NDM.UniversalInterchange universalInterchange = new Cargowise.NDM.UniversalInterchange();
                        Cargowise.NDM.UniversalInterchangeHeader universalInterchangeHeader = new Cargowise.NDM.UniversalInterchangeHeader();
                        Cargowise.NDM.UniversalInterchangeBody universalInterchangeBody = new Cargowise.NDM.UniversalInterchangeBody();
                        Cargowise.NDM.ProductData productData = new Cargowise.NDM.ProductData();
                        Cargowise.NDM.NativeProduct nativeProduct = new Cargowise.NDM.NativeProduct();
                        List<Cargowise.NDM.NativeProductOrgPartRelation> nativeProductOrgRelationColl = new List<Cargowise.NDM.NativeProductOrgPartRelation>();
                        Cargowise.NDM.NativeProductOrgPartRelation nativeProductOrgPartRelation = new Cargowise.NDM.NativeProductOrgPartRelation();
                        Cargowise.NDM.NativeProductOrgPartRelationOrgHeader orgPartRelationOrgHeader = new Cargowise.NDM.NativeProductOrgPartRelationOrgHeader();
                        List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode> nativeProductBarcodeColl = new List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode>();
                        Cargowise.NDM.NativeProductOrgSupplierPartBarcode nativeProductBarcode = new Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                        List<Cargowise.NDM.NativeProductOrgPartUnit> nativeProductOrgPartUnitColl = new List<Cargowise.NDM.NativeProductOrgPartUnit>();
                        iRec++;

                        nativeProduct = new Cargowise.NDM.NativeProduct();
                        nativeProduct.PartNum = product.Code.Replace("\"", "");
                        nativeProduct.Weight = product.Weight > 0 ? product.Weight : 0;
                        nativeProduct.WeightSpecified = product.Weight > 0 ? true : false;
                        nativeProduct.WeightUQ = product.WeightUnit != null ? product.WeightUnit.ToUpper() : null;
                        nativeProduct.WeightSpecified = product.WeightUnit != null ? true : false;
                        nativeProduct.Height = product.Height > 0 ? product.Height : 0;
                        nativeProduct.HeightSpecified = product.Height > 0 ? true : false;
                        nativeProduct.Width = product.Width > 0 ? product.Width : 0;
                        nativeProduct.WidthSpecified = product.Width > 0 ? true : false;
                        nativeProduct.Depth = product.Depth > 0 ? product.Depth : 0;
                        nativeProduct.DepthSpecified = product.Depth > 0 ? true : false;
                        nativeProduct.StockKeepingUnit = string.IsNullOrEmpty(product.StockKeepingUnit) ? "UNT" : product.StockKeepingUnit;

                        int volFactor = 1;
                        switch (product.DimsUnit)
                        {
                            case "MM":
                                volFactor = 1000;
                                break;
                            case "CM":
                                volFactor = 100;
                                break;
                            case "IN":
                                volFactor = 1;
                                break;
                            case "M":
                                volFactor = 1;
                                break;

                            default:
                                volFactor = 100;
                                break;
                        }
                        decimal volume = 0;
                        if (nativeProduct.Height > 0 && nativeProduct.Width > 0 && nativeProduct.Depth > 0)
                        {
                            volume = ((product.Height / volFactor) * (product.Width / volFactor) * (product.Depth / volFactor));
                        }
                        nativeProduct.Cubic = volume > 0 ? volume : 0;
                        nativeProduct.CubicSpecified = volume > 0 ? true : false;
                        nativeProduct.CubicUQ = volume > 0 ? "M3" : null;
                        nativeProduct.MeasureUQ = product.DimsUnit != null ? product.DimsUnit : null;
                        nativeProduct.Desc = product.Description.Truncate(80).Replace("\"", "");
                        nativeProduct.ActionSpecified = true;
                        nativeProduct.Action = partOperation;
                        nativeProduct.IsActive = true;
                        nativeProduct.IsWarehouseProduct = true;
                        nativeProduct.IsWarehouseProductSpecified = true;
                        nativeProduct.IsBarcoded = true;
                        nativeProduct.IsBarcodedSpecified = true;
                        orgPartRelationOrgHeader = new Cargowise.NDM.NativeProductOrgPartRelationOrgHeader();
                        nativeProductOrgRelationColl = new List<Cargowise.NDM.NativeProductOrgPartRelation>();
                        orgPartRelationOrgHeader.Code = common.IdendtityMatrix.SenderId;
                        nativeProductOrgPartRelation.OrgHeader = orgPartRelationOrgHeader;
                        nativeProductOrgPartRelation.Relationship = "OWN";
                        nativeProductOrgPartRelation.Action = partOperation;
                        nativeProductOrgPartRelation.ActionSpecified = true;
                        nativeProductOrgRelationColl.Add(nativeProductOrgPartRelation);

                        nativeProduct.OrgPartRelationCollection = nativeProductOrgRelationColl.ToArray();
                        if (product.Barcode != null)
                        {
                            nativeProductBarcodeColl = new List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode>();
                            nativeProduct.IsBarcoded = true;
                            //nativeProductBarcode = new Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                            //nativeProductBarcode.Action = Cargowise.NDM.Action.DELETE;
                            //nativeProductBarcode.ActionSpecified = true;
                            //nativeProductBarcode.Barcode = product.Barcode;
                            //nativeProductBarcode.PackType.TableName = "RefPackType";
                            Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType productPackType = new Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType();

                            //productPackType.TableName = "RefPackType";
                            //productPackType.Code = "UNT";
                            //nativeProductBarcode.PackType = productPackType;
                            //nativeProductBarcodeColl.Add(nativeProductBarcode);

                            nativeProductBarcode = new Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                            nativeProductBarcode.Action = partOperation;
                            nativeProductBarcode.ActionSpecified = true;
                            nativeProductBarcode.Barcode = product.Barcode;
                            //nativeProductBarcode.PackType.TableName = "RefPackType";
                            productPackType = new Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType();
                            nativeProductBarcodeColl.Add(nativeProductBarcode);
                            productPackType.TableName = "RefPackType";
                            productPackType.Code = string.IsNullOrEmpty(product.StockKeepingUnit) ? "UNT" : product.StockKeepingUnit;
                            nativeProductBarcode.PackType = productPackType;
                            nativeProduct.OrgSupplierPartBarcodeCollection = nativeProductBarcodeColl.ToArray();
                        }
                        else
                        {
                            nativeProduct.IsBarcoded = false;
                        }
                        var cwNSUniversal = new XmlSerializerNamespaces();
                        cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                        var cwNSNative = new XmlSerializerNamespaces();
                        cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
                        productData.OrgSupplierPart = nativeProduct;
                        productData.version = "2.0";
                        Cargowise.NDM.Native native = new Cargowise.NDM.Native
                        {
                            Body = new Cargowise.NDM.NativeBody
                            {
                                Any = new[] { productData.AsXmlElement(cwNSNative) }
                            },
                            Header = new Cargowise.NDM.NativeHeader
                            {
                                OwnerCode = common.IdendtityMatrix.SenderId,
                                EnableCodeMapping = true,
                                EnableCodeMappingSpecified = true,
                                DataContext = new Cargowise.NDM.DataContext
                                {
                                    ActionPurpose = new Cargowise.NDM.CodeDescriptionPair { Code = "IMP", Description = "Product Import" },
                                    EventType = new Cargowise.NDM.CodeDescriptionPair { Code = string.IsNullOrEmpty(common.IdendtityMatrix.EventCode) ? "ADD" : common.IdendtityMatrix.EventCode, Description = "Record Added" },
                                    DataProvider = common.IdendtityMatrix.SenderId,
                                },
                            },
                            version = "2.0"
                        };
                        universalInterchange.version = "1.1";
                        universalInterchangeHeader.SenderID = common.IdendtityMatrix.SenderId;
                        universalInterchangeHeader.RecipientID = common.IdendtityMatrix.CustomerId;
                        universalInterchangeBody.Any = new[] { native.AsXmlElement(cwNSUniversal) };
                        universalInterchange.Body = universalInterchangeBody;
                        universalInterchange.Header = universalInterchangeHeader;
                        String cwXML = Path.Combine(outputPath, common.IdendtityMatrix.SenderId + "PRODUCT" + "-" + product.Code.Replace('/', '-') + partOperation.ToString() + ".xml");
                        int iFileCount = 0;
                        while (File.Exists(cwXML))
                        {
                            iFileCount++;
                            cwXML = Path.Combine(outputPath, common.IdendtityMatrix.SenderId + "PRODUCT" + "-" + product.Code.Replace('/', '-') + partOperation + iFileCount + ".xml");
                        }
                        using (Stream outputCW = File.Open(cwXML, FileMode.Create))
                        {
                            StringWriter writer = new StringWriter();
                            XmlSerializer xSer = new XmlSerializer(typeof(Cargowise.NDM.UniversalInterchange));
                            xSer.Serialize(outputCW, universalInterchange, cwNSUniversal);
                            outputCW.Flush();
                            outputCW.Close();
                        }
                    }


                    catch (Exception ex)
                    {
                        cp = "Error Creating Product File";
                        de = "Error Processing " + product.Code;
                        ti = DateTime.Now;
                        er = "Error:" + ex.GetType().Name + " :" + ex.Message;
                        di = outputPath;
                        using (AppLogger log = new AppLogger(lo, cp, ex.Message, m.Name, ti, ex.InnerException.Message, di))
                        {
                            log.AddLog();
                        }
                        return false;

                    }
                }


            }
            else
            {
                return false;
            }
            return true;
        }

        public UniversalInterchange CreateCommercialInvoice(NodeFile common, string outLocation)
        {
            XUS.UniversalInterchange uic = CreateInterchange(common.IdendtityMatrix);
            UniversalShipmentData shipmentData = new UniversalShipmentData();
            //Create Universal Interchange Body and framework
            UniversalInterchangeBody uBody = new UniversalInterchangeBody
            {
                BodyField = new UniversalShipmentData
                {
                    Shipment = new Shipment
                    {
                        DataContext = new DataContext
                        {
                            DataTargetCollection = new List<DataTarget> {
                    new DataTarget
                    {
                        Type = "CustomsCommercialInvoice"
                    }}.ToArray(),
                            ActionPurpose = new CodeDescriptionPair
                            {
                                Code = "DIM"
                            },
                            DataProvider = common.IdendtityMatrix.SenderId,
                        }
                    }
                }
            };
            uic.Body = uBody;
            uBody.BodyField.Shipment.OrganizationAddressCollection = GetCompaniesFromCompanies(common.CommercialInvoice.Companies).ToArray();
            uBody.BodyField.Shipment.DateCollection = GetDatesFromCompanies(common.CommercialInvoice.Dates).ToArray();
            uBody.BodyField.Shipment.CommercialInfo = new CommercialInfo
            {
                Name = "All Invoices",
                CommercialInvoiceCollection = GetCommercialInvoiceFromCommon(common.CommercialInvoice).ToArray()

            };
            return uic;
        }

        private List<CommercialInfoCommercialInvoice> GetCommercialInvoiceFromCommon(NodeFileCommercialInvoice commercialInvoice)
        {
            List<CommercialInfoCommercialInvoice> invColl = new List<CommercialInfoCommercialInvoice>();
            var invoice = new CommercialInfoCommercialInvoice();
            invoice.InvoiceNumber = commercialInvoice.InvoiceNo;
            invoice.IncoTerm = new CodeDescriptionPair { Code = commercialInvoice.IncoTerm };
            invoice.InvoiceCurrency = new Currency { Code = commercialInvoice.InvoiceCurrency };
            if (commercialInvoice.TotalInvoiceAmount > 0)
            {
                invoice.InvoiceAmountSpecified = true;
                invoice.InvoiceAmount = commercialInvoice.TotalInvoiceAmount;

            }
            invoice.InvoiceDate = GetDateFromDatesByType(commercialInvoice.Dates, DateElementDateType.Invoiced);
            invoice.Supplier = GetCompanyByType(commercialInvoice.Companies, CompanyElementCompanyType.Supplier);
            invoice.Buyer = GetCompanyByType(commercialInvoice.Companies, CompanyElementCompanyType.Buyer);
            //invoice.CommercialInvoiceLineCollection.CommercialInvoiceLine = GetInvoiceLines(commercialInvoice.CommercialInvoiceLines).ToArray();
            var commercialInvoiceLineCollection = new CommercialInfoCommercialInvoiceCommercialInvoiceLineCollection();
            commercialInvoiceLineCollection.CommercialInvoiceLine = GetInvoiceLines(commercialInvoice.CommercialInvoiceLines).ToArray();
            invoice.CommercialInvoiceLineCollection = commercialInvoiceLineCollection;
            invColl.Add(invoice);
            return invColl;
        }


        private string GetDateFromDatesByType(DateElement[] dates, DateElementDateType invoiced)
        {
            var date = (from d in dates
                        where d.DateType == DateElementDateType.Raised
                        select d.ActualDate.ToCWDate()).FirstOrDefault();
            return string.IsNullOrEmpty(date) ? string.Empty : date;
        }

        private List<CommercialInfoCommercialInvoiceCommercialInvoiceLineCollectionCommercialInvoiceLine> GetInvoiceLines(CommercialInvoiceLineElement[] commercialInvoiceLines)
        {
            List<CommercialInfoCommercialInvoiceCommercialInvoiceLineCollectionCommercialInvoiceLine> lines = new List<CommercialInfoCommercialInvoiceCommercialInvoiceLineCollectionCommercialInvoiceLine>();
            var iLines = (from l in commercialInvoiceLines
                          select new CommercialInfoCommercialInvoiceCommercialInvoiceLineCollectionCommercialInvoiceLine
                          {
                              LineNo = l.LineNo,
                              LineNoSpecified = true,
                              Link = l.LineNo,
                              LinkSpecified = true,
                              Description = (l.Product.Description != null) ? l.Product.Description : "No Description",
                              InvoiceQuantity = l.InvoiceQuantity,
                              InvoiceQuantitySpecified = l.InvoiceQuantitySpecified,
                              PartNo = l.Product.Code,
                              Volume = l.Volume,
                              VolumeUnit = new UnitOfVolume { Code = l.VolumeUnit.ToString() },
                              Weight = l.Weight,
                              WeightUnit = new UnitOfWeight { Code = l.WeightUnit.ToString() },
                              VolumeSpecified = l.Volume >= 0 ? true : false,
                              WeightSpecified = l.Weight >= 0 ? true : false,
                              UnitPrice = l.UnitPrice,
                              UnitPriceSpecified = l.UnitPrice > 0 ? true : false,
                              LinePrice = l.LineTotal,
                              LinePriceSpecified = l.LineTotal > 0 ? true : false,

                          }).ToList();
            if (iLines.Count > 0)
            {
                return iLines;
            }
            return null;
        }

    }
}

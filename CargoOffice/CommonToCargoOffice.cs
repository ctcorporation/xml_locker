﻿
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace XMLLocker.CargoOffice
{
    public class CommonToCargoOffice
    {
        public Ivw_CustomerProfile Profile { get; set; }
        public string AppLogPath { get; set; }
        public string ErrString { get; set; }
        public string OutPutLocation { get; set; }
        public NodeFile CommonXml { get; set; }

        public CommonToCargoOffice(string appLogPath, Ivw_CustomerProfile profile)
        {
            Profile = profile;
            AppLogPath = appLogPath;
        }

        public string BuildFile()
        {
            string result = string.Empty;

            var ccNS = new XmlSerializerNamespaces();
            ccNS.Add("ns0", "http://cNode.ctc.net.au/09/2019");
            foreach (var timeSlot in CommonXml.TimeSlotRequests)
            {
                var outFile = ConvertFromCommon(timeSlot);
                var pathresult = Path.Combine(OutPutLocation, CommonXml.IdendtityMatrix.SenderId + "CargoOffice" + "-" + timeSlot.ShipmentNo + ".xml");
                int iFileCount = 0;
                while (File.Exists(pathresult))
                {
                    iFileCount++;
                    pathresult = Path.Combine(OutPutLocation, CommonXml.IdendtityMatrix.SenderId + "CargoOffice" + "-" + timeSlot.ShipmentNo + "-" + iFileCount + ".xml");
                }
                using (Stream outputCW = File.Open(pathresult, FileMode.Create))
                {
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(orderData));
                    xSer.Serialize(outputCW, outFile, ccNS);
                    outputCW.Flush();
                    outputCW.Close();
                }
                result = pathresult;

            }

            return result;
        }


        private orderData ConvertFromCommon(NodeFileTimeSlotRequest timeSlotRequest)
        {
            orderData cargoOfficeFile = new orderData();
            List<orderDataOrder> orders = new List<orderDataOrder>();
            orderDataOrder order = new orderDataOrder();
            order = GetTimeSlotFromCommon(timeSlotRequest);
            order.orderDate = DateTime.Now.ToString("yyyy-MM-dd");
            orders.Add(order);
            cargoOfficeFile.orders = orders.ToArray();
            return cargoOfficeFile;
        }

        private orderDataOrder GetTimeSlotFromCommon(NodeFileTimeSlotRequest timeSlotRequest)
        {
            orderDataOrder order = new orderDataOrder();
            order.orderReference = timeSlotRequest.ShipmentNo;
            order.shipments = GetShipmentDataFromTimeSlot(timeSlotRequest).ToArray();
            return order;
        }

        private List<orderDataOrderShipmentsShipment> GetShipmentDataFromTimeSlot(NodeFileTimeSlotRequest timeSlotRequest)
        {
            List<orderDataOrderShipmentsShipment> shipments = new List<orderDataOrderShipmentsShipment>();
            foreach (var container in timeSlotRequest.Containers)
            {
                orderDataOrderShipmentsShipment shipment = new orderDataOrderShipmentsShipment
                {
                    containerNbr = container.ContainerNo,
                    shipmentType = timeSlotRequest.Direction == NodeFileTimeSlotRequestDirection.I ? "IMP" : "EXP",
                    vesselName = timeSlotRequest.Vessel,
                    containerService = container.EquipmentNeeded == ContainerElementEquipmentNeeded.SDL ? "SideLoader" : "DropTrailer",
                    containerType = container.ISOCode,
                    containerSize = GetContainerSizeFromContainerCode(container.ISOCode),
                    sealNumber = container.SealNo,
                    deliveryName = GetCompanyDetailsfromCompany(CompanyElementCompanyType.DeliveryAddress, "CompanyName", timeSlotRequest.Companies),
                    deliveryAddress = GetCompanyDetailsfromCompany(CompanyElementCompanyType.DeliveryAddress, "Address1", timeSlotRequest.Companies),
                    deliveryAddress2 = GetCompanyDetailsfromCompany(CompanyElementCompanyType.DeliveryAddress, "Address2", timeSlotRequest.Companies),
                    deliveryCity = GetCompanyDetailsfromCompany(CompanyElementCompanyType.DeliveryAddress, "City", timeSlotRequest.Companies),
                    deliveryPostcode = GetCompanyDetailsfromCompany(CompanyElementCompanyType.DeliveryAddress, "PostCode", timeSlotRequest.Companies),
                    deliveryState = GetCompanyDetailsfromCompany(CompanyElementCompanyType.DeliveryAddress, "State", timeSlotRequest.Companies),
                    deliveryDate = GetDatesfromDate(DateElementDateType.DeliverBy, timeSlotRequest.Dates)

                };
                shipments.Add(shipment);

            }
            return shipments;
        }

        private string GetDatesfromDate(DateElementDateType dateType, DateElement[] dates)
        {
            string result = string.Empty;
            var date = (from d in dates
                        where d.DateType == dateType
                        select d.EstimateDate).FirstOrDefault();
            if (date != null)
            {
                result = date;
            }
            return date;
        }

        private string GetCompanyDetailsfromCompany(CompanyElementCompanyType companyType, string elementToReturn, CompanyElement[] companies)
        {
            string result = string.Empty;
            var company = (from x in companies
                           where x.CompanyType == companyType
                           select x).FirstOrDefault();
            if (company != null)
            {
                var prop = typeof(CompanyElement).GetProperty(elementToReturn);
                result = (string)prop.GetValue(company, null);
            }
            return result;
        }

        private string GetContainerSizeFromContainerCode(string iSOCode)
        {
            if (iSOCode.StartsWith("2"))
            {
                return "20";
            }
            if (iSOCode.StartsWith("4"))
            {
                return "40";
            }
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace XML_Locker.CargoOffice
{
    public class CargoOfficeToCommon
    {
        #region members
        string _senderid;
        string _recipientid;
        string _fileName;

        #endregion

        #region properties
        public string SenderId
        {
            get { return _senderid; }
            set { _senderid = value; }
        }

        public string RecipientId
        {
            get { return _recipientid; }
            set { _recipientid = value; }
        }
        #endregion

        #region constructors
        public CargoOfficeToCommon(string fileName, string senderId, string recipientId)
        {
            _fileName = fileName;
            _senderid = senderId;
            _recipientid = recipientId;

        }
        #endregion

        #region methods

        public NodeFile CreateCommon()
        {
            job_message jm = GetCargoOfficeJobMessage(_fileName);
            NodeFile common = new NodeFile();
            common.IdendtityMatrix = CreateIdentity(jm.CarrierEventCode);
            common.TimeSlotRequests = CreateTimeSlotResponses(jm).ToArray();
            return common;
        }

        private List<NodeFileTimeSlotRequest> CreateTimeSlotResponses(job_message jm)
        {
            List<NodeFileTimeSlotRequest> tsRequests = new List<NodeFileTimeSlotRequest>();
            NodeFileTimeSlotRequest timeSlot = new NodeFileTimeSlotRequest();
            timeSlot.Containers = ReturnContainers(jm.ContainerNo).ToArray();
            List<DateElement> dates = new List<DateElement>();
            DateElement date = new DateElement();
            switch (jm.CarrierEventCode)
            {
                case "BookingAck":
                    date.DateType = DateElementDateType.Booked;
                    break;
                case "Assigned":
                    date.DateType = DateElementDateType.GateOut;
                    break;
                case "Container Delivered":
                    date.DateType = DateElementDateType.Delivered;
                    break;
                case "Empty Returned":
                    date.DateType = DateElementDateType.Dehired;
                    break;
            }
            string time = !string.IsNullOrEmpty(jm.Time_Booked) ? jm.Time_Booked : "12:00:00";
            DateTime dt = DateTime.Parse(jm.Date_Booked);
            date.ActualDate = dt.ToString("dd/MM/yyyy") + time;
            dates.Add(date);
            timeSlot.Dates = dates.ToArray();
            timeSlot.CustomerReference =new CustomerReferenceElement { RefType = "Customer Reference", RefValue = jm.CustomerRef };
            tsRequests.Add(timeSlot);
            return tsRequests;
        }

        private List<ContainerElement> ReturnContainers(string containerNo)
        {
            List<ContainerElement> cn = new List<ContainerElement>
            {
                new ContainerElement{ ContainerNo = containerNo}
            };
            return cn;
        }

        private job_message GetCargoOfficeJobMessage(string fileName)
        {
            job_message jmxml = new job_message();
            using (FileStream fStream = new FileStream(fileName, FileMode.Open))
            {
                XmlSerializer jmcConvert = new XmlSerializer(typeof(job_message));
                jmxml = (job_message)jmcConvert.Deserialize(fStream);
            }
            return jmxml;
        }

        private NodeFileIdendtityMatrix CreateIdentity(string carrierEventCode)
        {
            NodeFileIdendtityMatrix idMat = new NodeFileIdendtityMatrix();
            idMat.SenderId = _senderid;
            idMat.CustomerId = _recipientid;
            idMat.FileDateTime = DateTime.Now.ToString("dd/MM/yyyy:hh:mm:ss");
            idMat.OriginalFileName = _fileName;
            string code = string.Empty;
            switch (carrierEventCode)
            {
                case "BookingAck":
                    code = "Booking Acknowledgement";
                    break;
                case "Assigned":
                    code = "Driver Assigned";
                    break;
                case "Container Delivered":
                    code = "Delivered";
                    break;
                case "Empty Returned":
                    code = "Dehired";
                    break;


            }
            idMat.DocumentType = code;
            return idMat;
        }
        #endregion

        #region helpers
        #endregion
    }
}

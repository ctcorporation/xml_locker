﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace XMLLocker.SAP
{
    public class SAPToCommon
    {
        #region Properties 
        public string OutputPath
        {
            get; set;
        }

        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }
        public Ivw_CustomerProfile Profile
        {
            get
            { return _profile; }
            set
            {
                _profile = value;
            }
        }
        public string ErrorString
        {
            get { return _errString; }
            set { _errString = value; }
        }


        public string targetSchema { get; set; }
        #endregion

        #region Members
        private string _errString;
        private string _fileToConvert;
        private string _connString;
        private Ivw_CustomerProfile _profile;
        #endregion

        #region Constructors
        public SAPToCommon(string fileToConvert, string connString, Ivw_CustomerProfile profile)
        {
            _fileToConvert = fileToConvert;
            _profile = profile;
            _connString = connString;
        }
        #endregion

        #region Methods
        public NodeFile ConvertSAP()
        {
            var sapXml = XDocument.Load(_fileToConvert);
            var xmlType = sapXml.Root.Name.ToString();
            NodeFile common = new NodeFile();
            switch (xmlType)
            {
                case "DELVRY03":
                    return Del03ToCommon();
                    break;
            }

            return null;
        }
        public NodeFile Del03ToCommon()
        {
            NodeFile result = new NodeFile();
            var sapDel03 = GetSapObject(_fileToConvert);
            result = ConvertSapToCommon(sapDel03);
            return result;
        }

        private NodeFile ConvertSapToCommon(DELVRY03 sapDel)
        {
            NodeFile common = new NodeFile();
            foreach (var sapOrder in sapDel.IDOC.E1EDL20)
            {
                common.IdendtityMatrix = ReturnIdentityMatrix();
                List<NodeFileTrackingOrder> orders = new List<NodeFileTrackingOrder>();
                NodeFileTrackingOrder order = GetOrderFromSap(sapOrder);
                orders.Add(order);
                common.TrackingOrders = orders.ToArray();


            }
            return common;
        }

        private NodeFileTrackingOrder GetOrderFromSap(DELVRY03IDOCE1EDL20 sapDel)
        {
            NodeFileTrackingOrder order = new NodeFileTrackingOrder();
            order.Companies = GetCompaniesFromOrder(sapDel).ToArray();
            order.Dates = GetOrderDates(sapDel).ToArray();
            order.OrderLines = GetLinesfromOrder(sapDel).ToArray();
            order.CustomerReferences = GetCustomFields(sapDel).ToArray();
            order.SupplierOrderNo = sapDel.INCO2_L;
            order.OrderNo = sapDel.E1EDL24[0].VGBEL;
            order.Containers = GetContainersFromOrder(sapDel).ToArray();
            if (order.OrderLines.Length > 0)
            {
                order.GoodsDescription = order.OrderLines[0].Product.Description;
            }


            return order;
        }

        private List<ContainerElement> GetContainersFromOrder(DELVRY03IDOCE1EDL20 sapDel)
        {
            var containers = new List<ContainerElement>();
            containers.Add(new ContainerElement
            {
                ContainerNo = sapDel.ABLAD,
                ContainerCode = decimal.Parse(sapDel.VOLUM) > 27 ? "40GP" : "20GP",
                GoodsWeight = decimal.Parse(sapDel.NTGEW),
                GoodsWeightSpecified = true,
                Volume = decimal.Parse(sapDel.VOLUM),
                VolumeSpecified = true,
                VolumeUnit = ContainerElementVolumeUnit.M3
            });
            return containers;
        }


        private List<CustomerReferenceElement> GetCustomFields(DELVRY03IDOCE1EDL20 sapDel)
        {
            List<CustomerReferenceElement> customerReferences = new List<CustomerReferenceElement>();
            // Inbound Delivery Number (008* type Number)
            customerReferences.Add(AddCustomerReference("Inbound Delivery Number", sapDel.LIFEX));
            //Transfer Order Number (62* type number) 
            customerReferences.Add(AddCustomerReference("Transfer Order Number", sapDel.E1EDL24[0].VGBEL));
            // Supplier Original Order Number (44* Type number) 
            customerReferences.Add(AddCustomerReference("Original Purchase Order", sapDel.INCO2_L));
            // Transfer Delivery Number (86* Type Number
            customerReferences.Add(AddCustomerReference("Transfer Delivery Number", sapDel.VBELN));

            return customerReferences;
        }
        private List<OrderLineElement> GetLinesfromOrder(DELVRY03IDOCE1EDL20 sapDel)
        {
            List<OrderLineElement> orders = new List<OrderLineElement>();
            foreach (DELVRY03IDOCE1EDL20E1EDL24 line in sapDel.E1EDL24)
            {
                OrderLineElement orderLine = new OrderLineElement
                {
                    LineNo = int.Parse(line.POSNR),
                    Product = new ProductElement
                    {
                        Code = line.MATNR,
                        Description = line.ARKTX,
                        Barcode = line.EAN11
                    },
                    OrderQty = decimal.Parse(line.ORMNG),
                    UnitOfMeasure = line.MEINS,
                    OrderQtySpecified = true,
                    Volume = decimal.Parse(line.VOLUM),
                    VolumeSpecified = true,
                    VolumeUnit = line.VOLEH == "MTQ" ? OrderLineElementVolumeUnit.M3 : OrderLineElementVolumeUnit.FT,
                    VolumeUnitSpecified = true,
                    Weight = decimal.Parse(line.NTGEW),
                    WeightSpecified = true,
                    WeightUnit = line.GEWEI == "KGM" ? OrderLineElementWeightUnit.KG : OrderLineElementWeightUnit.LB,
                    WeightUnitSpecified = true
                };
                orders.Add(orderLine);
            }
            return orders;
        }

        private List<DateElement> GetOrderDates(DELVRY03IDOCE1EDL20 sapDel)
        {
            var foundlst = (from x in sapDel.E1EDT13
                            where x.QUALF == "007"
                            select x.NTANF).FirstOrDefault();
            List<DateElement> dates = new List<DateElement>();
            dates.Add(new DateElement
            {
                DateType = DateElementDateType.Ordered,
                ActualDate = FormatDate(foundlst)
            }
            );
            return dates;
        }


        private List<CompanyElement> GetCompaniesFromOrder(DELVRY03IDOCE1EDL20 sapDel)
        {
            List<CompanyElement> companies = new List<CompanyElement>();

            companies.Add(GetCompany(CompanyElementCompanyType.Consignor, sapDel.XABLN));
            companies.Add(GetCompany(CompanyElementCompanyType.Consignee, Profile.P_SENDERID));
            return companies;
        }

        private CompanyElement GetCompany(CompanyElementCompanyType companyType, string companyCode)
        {
            CompanyElement company = new CompanyElement
            {
                CompanyType = companyType,
                CompanyOrgCode = companyCode,
                CompanyCode = companyCode
            };
            return company;
        }

        private NodeFileIdendtityMatrix ReturnIdentityMatrix()
        {
            NodeFileIdendtityMatrix identityMatrix = new NodeFileIdendtityMatrix();
            identityMatrix.CustomerId = Profile.C_CODE;
            //identityMatrix.DocumentIdentifier = 
            identityMatrix.SenderId = Profile.P_SENDERID;
            identityMatrix.DocumentType = "DELVRY03";
            identityMatrix.FileDateTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            identityMatrix.OriginalFileName = _fileToConvert;
            return identityMatrix;
        }

        private DELVRY03 GetSapObject(string fileNameToConvert)
        {
            DELVRY03 sapDel = new DELVRY03();
            FileInfo processingFile = new FileInfo(fileNameToConvert);
            DELVRY03 sapFile = new DELVRY03();
            using (FileStream fStream = new FileStream(fileNameToConvert, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(DELVRY03));
                sapDel = (DELVRY03)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            return sapDel;
        }
        #endregion

        #region Helpers
        private string GetEnum(string fieldName, string valueToFind)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(ConnString)))
            {
                var val = uow.CargowiseEnums.Find(x => x.CW_ENUMTYPE == fieldName && x.CW_ENUM == valueToFind).FirstOrDefault();

                if (val == null)
                {
                    return null;
                }

                return val.CW_MAPVALUE;
            }
        }
        private CustomerReferenceElement AddCustomerReference(string refType, string refValue)
        {
            CustomerReferenceElement customerReference = new CustomerReferenceElement
            {
                RefType = refType,
                RefValue = refValue
            };
            return customerReference;
        }
        private string FormatDate(string foundlst)
        {
            var newDate = foundlst.Substring(0, 4) + "-" + foundlst.Substring(4, 2) + "-" + foundlst.Substring(6, 2) + "T00:00:00";

            return newDate;
        }
        #endregion





    }
}

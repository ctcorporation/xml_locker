﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace XMLLocker.SAP
{
    class ProfileParams
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class CommonToSAP
    {
        #region members
        NodeData.Models.Ivw_CustomerProfile _profile;
        private string _connString;
        private string _outputPath;
        #endregion

        #region properties
        public string ConnString
        {
            get
            {
                return _connString;
            }
            set
            {
                _connString = value;
            }
        }

        public string OutputPath
        {
            get
            { return _outputPath; }
            set
            { _outputPath = value; }
        }

        public string ErrorString { get; set; }
        #endregion

        #region constructors
        public CommonToSAP(NodeData.Models.Ivw_CustomerProfile profile)
        {
            _profile = profile;

        }
        #endregion

        #region methods
        public object CreateSAP(NodeFileTrackingOrder commonOrder, string docType)
        {
            string sapType = GetCustomMethod(docType);
            Type custType = this.GetType();
            MethodInfo custMethodToRun = custType.GetMethod(sapType);
            try
            {
                var sapFile = custMethodToRun.Invoke(this, new object[] { commonOrder, _profile });
                if (sapFile != null)
                {
                    return sapFile;
                }
            }
            catch (Exception)
            {

            }


            return null;
        }

        private string GetCustomMethod(string documentType)
        {
            string result = string.Empty;
            switch (documentType)
            {
                case "DELVRY03":
                    result = "CommonToDelvry03";
                    break;
            }
            return result;
        }

        public DELVRY03 CommonToDelvry03(NodeFileTrackingOrder commonOrder, NodeData.Models.Ivw_CustomerProfile profile)
        {
            var profParams = GetProfileParams(profile.P_PARAMLIST, "Milestone");

            DELVRY03 sapDelvry03 = new DELVRY03();
            List<DELVRY03IDOCE1EDL20> e1edl20s = new List<DELVRY03IDOCE1EDL20>();
            List<DELVRY03IDOCE1EDL20E1EDL18> e1edl18s = new List<DELVRY03IDOCE1EDL20E1EDL18>();
            e1edl18s.Add(new DELVRY03IDOCE1EDL20E1EDL18 { QUALF = "CHG" });
            List<DELVRY03IDOCE1EDL20E1EDT13> e1edt13s = new List<DELVRY03IDOCE1EDL20E1EDT13>();
            string sapDate = string.Empty;
            string sapTime = string.Empty;
            DateTime dt;
            if (profParams.Count > 0)
            {
                var milestoneCode = (from f in profParams
                                     where f.Key == "Milestone"
                                     select f.Value).FirstOrDefault();
                var dateType = (from f in profParams
                                where f.Key == "DateType"
                                select f.Value).FirstOrDefault();
                if (!string.IsNullOrEmpty(milestoneCode))
                {
                    var milestone = GetMileStone(commonOrder.Milestones, milestoneCode);
                    if (milestone != null)
                    {
                        var milestoneDate = dateType == "Estimated" ? milestone.MilestoneDate.EstimateDate : milestone.MilestoneDate.ActualDate;
                        if (!string.IsNullOrEmpty(milestoneDate))
                        {
                            if (DateTime.TryParse(milestoneDate, out dt))
                            {
                                sapDate = dt.ToString("yyyyMMdd");
                                sapTime = dt.ToString("hhmmss");
                            }
                        }
                    }

                }
            }



            //Is the sapDate is null then Milestone date has not been found. 
            // Get the ETA if Possible. 
            if (string.IsNullOrEmpty(sapDate))
            {
                sapDate = GetDateFromCommon(commonOrder.Dates, DateElementDateType.Arrival)?.ToString("yyyyMMdd");
                sapTime = GetDateFromCommon(commonOrder.Dates, DateElementDateType.Arrival)?.ToString("hhmmss");
                if (string.IsNullOrEmpty(sapDate))
                {
                    //if still no date has been found no need to continue on.
                    ErrorString = "No Dates are Found.";
                    return null;
                }                
            }
            e1edt13s.Add(new DELVRY03IDOCE1EDL20E1EDT13
            {
                QUALF = "007",
                NTANF = sapDate,
                NTANZ = sapTime

            });

            var custOrderNo = GetCustomerReference(commonOrder.CustomerReferences, "Inbound Delivery Number");
            if (string.IsNullOrEmpty(custOrderNo))
            {
                //Customer Order Number not found. 
                ErrorString = "Order number :" + commonOrder.OrderNo + ", Inbound Delivery Order Number not found in References ";
                return null;
            }
            e1edl20s.Add(new DELVRY03IDOCE1EDL20
            {
                VBELN = custOrderNo,
                E1EDL18 = e1edl18s.ToArray(),
                E1EDT13 = e1edt13s.ToArray()
            });

            DELVRY03IDOC sapIdoc = new DELVRY03IDOC
            {
                BEGIN = DELVRY03IDOCBEGIN.Item1,
                EDI_DC40 = new DELVRY03IDOCEDI_DC40
                {
                    TABNAM = "EDI_DC40",
                    MANDT = "330",
                    DIRECT = DELVRY03IDOCEDI_DC40DIRECT.Item2,
                    IDOCTYP = "DELVRY03",
                    MESTYP = "SHPCON",
                    MESCOD = string.Empty,
                    MESFCT = "MON",
                    SNDPOR = "ZDOP",
                    SNDPRT = "LS",
                    SNDPRN = "SYSTCX330",
                    RCVPOR = "SAPTX2",
                    RCVPRT = "LS",
                    RCVPFC = "LS",
                    RCVPRN = "3PL_PRD",
                    SERIAL = GetNewSerial("IDOC", profile.P_RECIPIENTID)
                },
                E1EDL20 = e1edl20s.ToArray()
            };
            sapDelvry03.IDOC = sapIdoc;

            return sapDelvry03;
        }

        private List<ProfileParams> GetProfileParams(string paramList, string paramtoFind)
        {
            if (string.IsNullOrEmpty(paramList))
                return null;
            var pArray = paramList.Split('|').ToList();
            var pList = new List<ProfileParams>();
            if (pArray.Count > 0)
            {

                foreach (string p in pArray)
                {
                    if (p.Contains(paramtoFind))
                    {
                        var t = p.Split(',');
                        if (t.Length > 0)
                        {
                            foreach (var sParam in t)
                            {
                                var sParamLine = sParam.Split(':');
                                pList.Add(new ProfileParams { Key = sParamLine[0], Value = sParamLine[1] });

                            }


                        }
                    }

                }
            }
            return pList;
        }

        private MilestoneElement GetMileStone(MilestoneElement[] milestones, string milestoneCode)
        {
            if (milestones.Length > 0)
            {
                return (from x in milestones.ToList()
                        where x.Code == milestoneCode
                        select x).FirstOrDefault();
            }
            else
                return null;
        }

        private string GetNewSerial(string fieldName, string valueToFind)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(ConnString)))
            {
                var val = uow.CargowiseEnums.Find(x => x.CW_ENUMTYPE == fieldName && x.CW_ENUM == valueToFind).FirstOrDefault();
                int iSerial = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                if (val == null)
                {
                    val = new Cargowise_Enums();
                    val.CW_ENUMTYPE = fieldName;
                    val.CW_ENUM = valueToFind;
                    val.CW_MAPVALUE = iSerial.ToString();
                    uow.CargowiseEnums.Add(val);
                }
                else
                {
                    iSerial = int.Parse(val.CW_MAPVALUE) + 1;
                    val.CW_MAPVALUE = iSerial.ToString();
                }
                uow.Complete();
                return iSerial.ToString();
            }
        }

        #endregion

        #region helpers
        private string GetCustomerReference(CustomerReferenceElement[] customerReferences, string reference)
        {
            var value = (from x in customerReferences
                         where x.RefType == reference
                         select x.RefValue).FirstOrDefault();
            return value;

        }

        private DateTime? GetDateFromCommon(DateElement[] dates, DateElementDateType dateType)
        {
            DateTime d;
            var date = (from x in dates
                        where x.DateType == dateType
                        select x).FirstOrDefault();
            if (date != null)
            {
                if (!string.IsNullOrEmpty(date.ActualDate))
                {
                    if (DateTime.TryParse(date.ActualDate, out d))
                    {
                        return d;
                    }

                }

                if (!string.IsNullOrEmpty(date.EstimateDate))
                {
                    if (DateTime.TryParse(date.EstimateDate, out d))
                    {
                        return d;
                    }
                }
            }
            return null;
        }

        public string BuildFile(object sapFile, Type type)
        {
            var ccNS = new XmlSerializerNamespaces();
            ccNS.Add("", "");

            var pathresult = Path.Combine(_outputPath, "SHPCON-" + DateTime.Now.ToString("yyyyMMddhhmm") + ".xml");
            int iFileCount = 0;
            while (File.Exists(pathresult))
            {
                iFileCount++;
                pathresult = Path.Combine(_outputPath, "SHPCON-" + DateTime.Now.ToString("yyyyMMddhhmm") + "-" + iFileCount + ".xml");
            }
            using (Stream outputCW = File.Open(pathresult, FileMode.Create))
            {
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(type);
                xSer.Serialize(outputCW, sapFile, ccNS);
                outputCW.Flush();
                outputCW.Close();
            }
            return pathresult;

        }

        #endregion
    }
}

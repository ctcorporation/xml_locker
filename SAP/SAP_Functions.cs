﻿using NodeResources;
using System.IO;
using System.Xml.Serialization;

namespace XMLLocker.SAP
{
    public class SAP_Functions<T>
    {
        #region members
        private string _outputPath;
        private DELVRY03 _sapDel;
        #endregion

        #region properties
        public string OutputPath
        {
            get
            { return _outputPath; }
            set
            {
                _outputPath = value;
            }
        }

        public DELVRY03 SapDel
        {
            get
            {
                return _sapDel;
            }
            set
            {
                _sapDel = value;
            }
        }
        public Ivw_CustomerProfile Profile
        {
            get; set;
        }
        #endregion

        #region constructors

        #endregion

        #region methods
        public void BuildSAPFile()
        {
            var Sender = Profile.P_SENDERID;
            var RefNo = SapDel.IDOC.EDI_DC40.SERIAL;

            string xmlFile = Sender + RefNo;
            int i = 1;
            while (File.Exists(Path.Combine(OutputPath, xmlFile + ".XML")))
            {
                xmlFile = Sender + RefNo + i.ToString();
                i++;
            }
            var sapNs = new XmlSerializerNamespaces();
            sapNs.Add("", "");
            using (Stream outputCW = File.Open(Path.Combine(OutputPath, xmlFile + ".XML"), FileMode.Create))
            {
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(DELVRY03));
                xSer.Serialize(outputCW, SapDel, sapNs);
                outputCW.Flush();
                outputCW.Close();
            }

        }
        #endregion

        #region helpers

        #endregion
    }
}

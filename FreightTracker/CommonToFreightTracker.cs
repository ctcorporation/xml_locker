﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace XML_Locker.FreightTracker
{
    class OrgMap
    {
        internal CompanyElementCompanyType CompType { get; set; }
        internal string MapFrom { get; set; }
        internal string MapTo { get; set; }
    }
    public class CommonToFreightTracker
    {
        #region members
        string _custCode;
        string _errString;
        vw_CustomerProfile _prof;
        #endregion
        #region properties

        string CustCode
        {
            get
            {
                return _custCode;
            }
            set
            {
                _custCode = value;
            }
        }
        string Error
        {
            get
            {
                return _errString;
            }
            set
            {
                _errString = value;
            }
        }

        vw_CustomerProfile CustProfile
        {
            get
            {
                return _prof;
            }
            set
            {
                _prof = value;
            }
        }
        #endregion

        #region constructors
        public CommonToFreightTracker()
        {

        }
        public CommonToFreightTracker(vw_CustomerProfile custprofile)
        {
            CustProfile = custprofile;
        }

        #endregion

        #region methods

        public Booking CreateBooking(NodeFileTimeSlotRequest tsr)
        {


            var booking = GetTimeSlotRequest(tsr);
            booking.EDIDateTime = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz");
            booking.CustomerCode = _custCode;
            return booking;

        }


        private Booking GetTimeSlotRequest(NodeFileTimeSlotRequest timeSlotRequest)
        {
            Booking booking = new Booking();
            var companies = GetCompanies(timeSlotRequest.Companies);
            //  companies = CheckDCABillTo(companies);
            booking.AddressCollection = companies.ToArray();
            booking.ModeOfTransport = timeSlotRequest.ModeOfTransport.ToString().ToEnum<BookingModeOfTransport>(BookingModeOfTransport.S);
            booking.ContainerCollection = timeSlotRequest.Containers != null ? GetContainers(timeSlotRequest.Containers).ToArray() : null;
            if (booking.ModeOfTransport != BookingModeOfTransport.S)
            {
                booking.Pieces = timeSlotRequest.Pieces;
                booking.PiecesSpecified = timeSlotRequest.Pieces > 0;
                booking.Weight = timeSlotRequest.Weight;
                booking.WeightSpecified = timeSlotRequest.Weight > 0;
                booking.UnitOfMeasure = new BookingUnitOfMeasure { Code = timeSlotRequest.UnitOfMeasure };
                var requiredDate = GetDateFromDates(timeSlotRequest.Dates, DateElementDateType.DeliverBy);
                if (requiredDate != null)
                {
                    booking.RequiredDate = !string.IsNullOrEmpty(requiredDate.ActualDate) ? requiredDate.ActualDate : !string.IsNullOrEmpty(requiredDate.EstimateDate) ? requiredDate.EstimateDate : string.Empty;
                }


            }

            booking.BookingReference = timeSlotRequest.BookingReference;

            switch (timeSlotRequest.Direction)
            {
                case NodeFileTimeSlotRequestDirection.E:
                    booking.JobType = BookingJobType.Export;
                    break;
                case NodeFileTimeSlotRequestDirection.I:
                    booking.JobType = BookingJobType.Import;
                    break;

            }
            booking.JobTypeSpecified = true;
            booking.VesselLloydsIMO = timeSlotRequest.VesselNo;
            booking.VesselName = timeSlotRequest.Vessel;
            booking.VoyageNumber = timeSlotRequest.Voyage;
            if (timeSlotRequest.CustomerReference != null)
            {
                booking.CustomerReference = timeSlotRequest.CustomerReference.RefValue;
            }

            return booking;
        }

        private DateElement GetDateFromDates(DateElement[] dates, DateElementDateType dateType)
        {
            var date = (from x in dates.ToList()
                        where x.DateType == dateType
                        select x).FirstOrDefault();
            return date;
        }

        private List<BookingAddressCollectionAddress> CheckDCABillTo(List<BookingAddressCollectionAddress> companies)
        {
            var delivery = (from d in companies
                            where d.AddressType == BookingAddressCollectionAddressAddressType.Delivery
                            select d.AddressName).FirstOrDefault();
            var dca = (from t in companies
                       where t.AddressType == BookingAddressCollectionAddressAddressType.Transport
                       select t.AddressName).FirstOrDefault();
            if (delivery != dca)
            {
                var billto = (from b in companies
                              where b.AddressType == BookingAddressCollectionAddressAddressType.BillTo
                              select b).FirstOrDefault();
                //dont understand why set then remove.
                //if (billto != null)
                //{
                //    companies.Remove(billto);
                //}

            }
            return companies;

        }

        private List<BookingContainerCollectionContainer> GetContainers(ContainerElement[] containers)
        {
            List<BookingContainerCollectionContainer> bookingcontainers = new List<BookingContainerCollectionContainer>();
            foreach (var container in containers)
            {
                BookingContainerCollectionContainer bookingcontainer = new BookingContainerCollectionContainer();
                bookingcontainer.ContainerNumber = container.ContainerNo;
                bookingcontainer.ISO = container.ContainerCode;
                bookingcontainer.SealNumber = container.SealNo;
                bookingcontainer.GrossWeight = container.GrossWeight.ToString();
                bookingcontainer.DropMode = Extensions.ToEnum<BookingContainerCollectionContainerDropMode>(container.EquipmentNeeded.ToString(), BookingContainerCollectionContainerDropMode.ASK);
                bookingcontainer.DropModeSpecified = true;
                if (container.Dates != null)
                {
                    var cd = (from x in container.Dates
                              where x.DateType == DateElementDateType.DeliverBy
                              select x).FirstOrDefault();
                    if (cd != null)
                    {
                        bookingcontainer.RequiredDate = string.IsNullOrEmpty(cd.ActualDate) ? cd.EstimateDate : cd.ActualDate;
                    }
                }
                if (container.CommoditySpecified)
                {
                    if (container.Commodity == ContainerElementCommodity.HAZ)
                    {
                        bookingcontainer.Cargo = "HZ";
                    }
                }
                bookingcontainers.Add(bookingcontainer);

            }
            return bookingcontainers;
        }

        private List<BookingAddressCollectionAddress> GetCompanies(CompanyElement[] companies)
        {
            var orgMap = GetCompanyMap(_prof);
            List<BookingAddressCollectionAddress> addresses = new List<BookingAddressCollectionAddress>();
            foreach (var company in companies)
            {
                if (company != null)
                {
                    BookingAddressCollectionAddress address = new BookingAddressCollectionAddress();
                    address.AddressCode = company.CompanyOrgCode;
                    address.AddressLine1 = company.Address1;
                    address.AddressLine2 = company.Address2;
                    address.AddressName = company.CompanyName;
                    address.AddressState = company.State;
                    address.AddressPostCode = company.PostCode;
                    address.AddressSuburb = company.City;
                    if (orgMap != null)
                    {
                        var mapping = (from m in orgMap
                                       where m.CompType == company.CompanyType
                                       select m).FirstOrDefault();
                        if (mapping != null)
                        {
                            var myType = typeof(BookingAddressCollectionAddress);
                            var toProp = address.GetType().GetProperty(mapping.MapTo);
                            if (toProp != null)
                            {
                                var fromProp = company.GetType().GetProperty(mapping.MapFrom);
                                if (fromProp != null)
                                {
                                    var newCode = company.GetType().GetProperty(mapping.MapFrom).GetValue(company, null);
                                    if (newCode != null)
                                    {
                                        address.GetType().GetProperty(mapping.MapTo).SetValue(address, newCode.ToString());
                                    }

                                }
                                else
                                {
                                    _errString += "Unable to find Mapping From Field " + mapping.MapFrom + " in " + company.GetType().Name + Environment.NewLine;
                                }

                            }
                            else
                            {
                                _errString += "Unable to find Mapping To Field " + mapping.MapTo + " in " + address.GetType().Name + Environment.NewLine;
                            }
                        }
                    }


                    switch (company.CompanyType)
                    {
                        case CompanyElementCompanyType.Consignee:
                            address.AddressType = BookingAddressCollectionAddressAddressType.Consignee;

                            break;
                        case CompanyElementCompanyType.Consignor:
                            address.AddressType = BookingAddressCollectionAddressAddressType.Consignor;

                            break;
                        case CompanyElementCompanyType.DeliveryAddress:
                            address.AddressType = BookingAddressCollectionAddressAddressType.Delivery;

                            break;
                        case CompanyElementCompanyType.PickupAddress:
                            address.AddressType = BookingAddressCollectionAddressAddressType.Pickup;

                            break;
                        case CompanyElementCompanyType.CTO:
                            address.AddressType = BookingAddressCollectionAddressAddressType.CTO;

                            break;
                        case CompanyElementCompanyType.ContainerYard:
                            address.AddressType = BookingAddressCollectionAddressAddressType.CFS;

                            break;
                        case CompanyElementCompanyType.Buyer:
                            address.AddressType = BookingAddressCollectionAddressAddressType.Consignee;

                            break;

                        case CompanyElementCompanyType.BillToAddress:
                            address.AddressType = BookingAddressCollectionAddressAddressType.BillTo;
                            break;
                        case CompanyElementCompanyType.TransportCompany:
                            address.AddressType = BookingAddressCollectionAddressAddressType.Transport;

                            break;
                    }
                    address.AddressTypeSpecified = true;

                    addresses.Add(address);
                }
            }
            return addresses;
        }


        #endregion

        #region helpers
        private List<OrgMap> GetCompanyMap(vw_CustomerProfile prof)
        {
            List<OrgMap> orgMap = new List<OrgMap>();
            var paramlist = prof.P_PARAMLIST.Split('|').ToList();
            if (paramlist.Count > 0)
            {
                var pList = (from param in paramlist
                             where param.StartsWith("MAP")
                             select param).ToList();
                if (pList.Count > 0)
                {
                    foreach (var param in pList)
                    {
                        var p = param.Split(',');
                        OrgMap om = new OrgMap();
                        foreach (var str in p)
                        {
                            var entry = str.Split(':');

                            switch (entry[0])
                            {
                                case "MAP":
                                    break;
                                case "MapFrom":
                                    om.MapFrom = entry[1];
                                    break;
                                case "MapTo":
                                    om.MapTo = entry[1];
                                    break;
                                case "CompType":
                                    om.CompType = Extensions.ToEnum<CompanyElementCompanyType>(entry[1], CompanyElementCompanyType.DeliveryAddress);
                                    break;
                            }

                        }
                        if (!string.IsNullOrEmpty(om.MapFrom))
                        {
                            orgMap.Add(om);
                        }

                    }
                    return orgMap;

                }
            }

            return null;
        }
        #endregion
    }
}


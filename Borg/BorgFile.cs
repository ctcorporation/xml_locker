﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XML_Locker.Borg
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.18020")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.bluenet.rohlig.com/rbn/RohligService/")]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.bluenet.rohlig.com/rbn/RohligService/", IsNullable = false)]
    public partial class File
    {
        private FileHeader headerField;
        private FileContent contentField;
        private string jobNumberField;
        private string conversionTypeField;

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public FileHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public FileContent Content
        {
            get
            {
                return this.contentField;
            }
            set
            {
                this.contentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public string JobNumber
        {
            get
            {
                return this.jobNumberField;
            }
            set
            {
                this.jobNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public string ConversionType
        {
            get
            {
                return this.conversionTypeField;
            }
            set
            {
                this.conversionTypeField = value;
            }
        }

    }

    public partial class FileHeader
    {
        private string dateField;
        private string filenameField;
        private TriggerInformation triggerInformationField;

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Filename
        {
            get
            {
                return this.filenameField;
            }
            set
            {
                this.filenameField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TriggerInformation TriggerInformation
        {
            get
            {
                return this.triggerInformationField;
            }
            set
            {
                this.triggerInformationField = value;
            }
        }

    }

    public partial class FileContent
    {
        private Consol[] consolsField;
        private CustomsDeclaration[] customsDeclarationsField;

        [System.Xml.Serialization.XmlArrayItemAttribute("CustomsDeclaration", IsNullable = true)]
        public CustomsDeclaration[] CustomsDeclarations
        {
            get
            {
                return this.customsDeclarationsField;
            }
            set
            {
                this.customsDeclarationsField = value;
            }
        }

        
        [System.Xml.Serialization.XmlArrayItemAttribute("Consol", IsNullable = true)]
        public Consol[] Consols
        {
            get
            {
                return this.consolsField;
            }
            set
            {
                this.consolsField = value;
            }
        }

    }

    public partial class TriggerInformation
    {
        private string statusCodeField;
        private string descriptionField;
        private string dateField;

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string StatusCode
        {
            get
            {
                return this.statusCodeField;
            }
            set
            {
                this.statusCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }


    
    public partial class Consol
    {
        private Shipment[] shipmentsField;
        private Routing[] routingsField;

        
        [System.Xml.Serialization.XmlArrayItemAttribute("Shipment", IsNullable = true)]
        public Shipment[] Shipments
        {
            get
            {
                return this.shipmentsField;
            }
            set
            {
                this.shipmentsField = value;
            }
        }

        
        [System.Xml.Serialization.XmlArrayItemAttribute("Routing", IsNullable = true)]
        public Routing[] Routings
        {
            get
            {
                return this.routingsField;
            }
            set
            {
                this.routingsField = value;
            }
        }
    }

    public partial class CustomsDeclaration
    {
        private string jobNumberField;

        private string masterBillField;

        private string transportModeField;

        private bool transportModeFieldSpecified;

        private string containerModeField;

        private bool containerModeFieldSpecified;

        private Company supplierField;

        private Company importerField;

        private string ownerReferenceField;

        private TypeValuePair totalWeightField;

        private TypeValuePair totalVolumeField;

        private Term incoTermField;

        private Container[] containersField;

        private Routing[] routingsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string JobNumber
        {
            get
            {
                return this.jobNumberField;
            }
            set
            {
                this.jobNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MasterBill
        {
            get
            {
                return this.masterBillField;
            }
            set
            {
                this.masterBillField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TransportMode
        {
            get
            {
                return this.transportModeField;
            }
            set
            {
                this.transportModeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TransportModeSpecified
        {
            get
            {
                return this.transportModeFieldSpecified;
            }
            set
            {
                this.transportModeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ContainerMode
        {
            get
            {
                return this.containerModeField;
            }
            set
            {
                this.containerModeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ContainerModeSpecified
        {
            get
            {
                return this.containerModeFieldSpecified;
            }
            set
            {
                this.containerModeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Term IncoTerm
        {
            get
            {
                return this.incoTermField;
            }
            set
            {
                this.incoTermField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TypeValuePair TotalWeight
        {
            get
            {
                return this.totalWeightField;
            }
            set
            {
                this.totalWeightField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TypeValuePair TotalVolume
        {
            get
            {
                return this.totalVolumeField;
            }
            set
            {
                this.totalVolumeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Company Supplier
        {
            get
            {
                return this.supplierField;
            }
            set
            {
                this.supplierField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Company Importer
        {
            get
            {
                return this.importerField;
            }
            set
            {
                this.importerField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Container[] Containers
        {
            get
            {
                return this.containersField;
            }
            set
            {
                this.containersField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string OwnerReference
        {
            get
            {
                return this.ownerReferenceField;
            }
            set
            {
                this.ownerReferenceField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Routing[] Routings
        {
            get
            {
                return this.routingsField;
            }
            set
            {
                this.routingsField = value;
            }
        }
    }


    public partial class Shipment
    {
        private string shipmentNumberField;

        private string transportModeField;

        private string houseBillNoField;

        private TypeValuePair weightField;

        private TypeValuePair chargeableWeightField;

        private TypeValuePair volumeField;
        
        private Term incoTermsField;

        private Company consignorField;

        private Company consigneeField;

        private string containerModeField;

        private Container[] containersField;

        private string bookingReferenceField;

        private Order[] ordersField;

        

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ShipmentNumber
        {
            get
            {
                return this.shipmentNumberField;
            }
            set
            {
                this.shipmentNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TransportMode
        {
            get
            {
                return this.transportModeField;
            }
            set
            {
                this.transportModeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string HouseBillNo
        {
            get
            {
                return this.houseBillNoField;
            }
            set
            {
                this.houseBillNoField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TypeValuePair Weight
        {
            get
            {
                return this.weightField;
            }
            set
            {
                this.weightField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TypeValuePair ChargeableWeight
        {
            get
            {
                return this.chargeableWeightField;
            }
            set
            {
                this.chargeableWeightField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TypeValuePair Volume
        {
            get
            {
                return this.volumeField;
            }
            set
            {
                this.volumeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Term Term
        {
            get
            {
                return this.incoTermsField;
            }
            set
            {
                this.incoTermsField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Company Consignor
        {
            get
            {
                return this.consignorField;
            }
            set
            {
                this.consignorField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Company Consignee
        {
            get
            {
                return this.consigneeField;
            }
            set
            {
                this.consigneeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ContainerMode
        {
            get
            {
                return this.containerModeField;
            }
            set
            {
                this.containerModeField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Container[] Containers
        {
            get
            {
                return this.containersField;
            }
            set
            {
                this.containersField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string BookingReference
        {
            get
            {
                return this.bookingReferenceField;
            }
            set
            {
                this.bookingReferenceField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public Order[] Orders
        {
            get
            {
                return this.ordersField;
            }
            set
            {
                this.ordersField = value;
            }
        }


    }

    public partial class Routing
    {
        private int legNumberField;

        private PortElement POLPortField;

        private PortElement PODPortField;

        private string tranportModeField;

        private string vesselField;

        private string voyageNoField;


        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int LegNumber
        {
            get
            {
                return this.legNumberField;
            }
            set
            {
                this.legNumberField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public PortElement POLPort
        {
            get
            {
                return this.POLPortField;
            }
            set
            {
                this.POLPortField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public PortElement PODPort
        {
            get
            {
                return this.PODPortField;
            }
            set
            {
                this.PODPortField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TranportMode
        {
            get
            {
                return this.tranportModeField;
            }
            set
            {
                this.tranportModeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Vessel
        {
            get
            {
                return this.vesselField;
            }
            set
            {
                this.vesselField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string VoyageNo
        {
            get
            {
                return this.voyageNoField;
            }
            set
            {
                this.voyageNoField = value;
            }
        }

    }

    public partial class PortElement
    {
        private string portField;

        private string countryField;

        private string cityField;

        private string estimatedField;

        private string actualField;

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Port
        {
            get
            {
                return this.portField;
            }
            set
            {
                this.portField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Estimated
        {
            get
            {
                return this.estimatedField;
            }
            set
            {
                this.estimatedField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Actual
        {
            get
            {
                return this.actualField;
            }
            set
            {
                this.actualField = value;
            }
        }
    }

    public partial class Term
    {
        private string incoTermField;

        public string IncoTerm
        {
            get
            {
                return this.incoTermField;
            }
            set
            {
                this.incoTermField = value;
            }
        }
    }


    public partial class TypeValuePair
    {

        private string typeField;

        private string valueField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    public partial class Company
    {

        private string EDICodeField;

        private string nameField;

        /// <remarks/>
        public string EDICode
        {
            get
            {
                return this.EDICodeField;
            }
            set
            {
                this.EDICodeField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    public partial class Container
    {
        private string containerNrField;

        private TypeValuePair grossWeightField;


        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ContainerNr
        {
            get
            {
                return this.containerNrField;
            }
            set
            {
                this.containerNrField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TypeValuePair GrossWeight
        {
            get
            {
                return this.grossWeightField;
            }
            set
            {
                this.grossWeightField = value;
            }
        }
    }

    public partial class Order
    {
        private string orderNumberField;

        private bool isSystemGeneratedField;

        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string OrderNumber
        {
            get
            {
                return this.orderNumberField;
            }
            set
            {
                this.orderNumberField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public bool IsSystemGenerated
        {
            get
            {
                return this.isSystemGeneratedField;
            }
            set
            {
                this.isSystemGeneratedField = value;
            }
        }
    }




}

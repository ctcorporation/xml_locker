﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XML_Locker.Borg
{
    public class CommonToBorg
    {
        public Borg.File ConvertData(NodeFile commonXML)
        {

            var borgXML = new Borg.File();

            var header = new FileHeader();
            var content = new FileContent();

            if (commonXML.Shipments != null)
            {
                var consols = new List<Consol>();
                var shipments = new List<Shipment>();
                var routings = new List<Routing>();
                Consol con = new Consol();
                header = GetHeader(commonXML);
                foreach (var order in commonXML.Shipments)
                {
                    var shipment = GetShipment(order);
                    borgXML.JobNumber = shipment.ShipmentNumber;
                    var routing = GetShipmentRoutings(order);
                    shipments.Add(shipment);
                    routings.AddRange(routing);
                }

                con.Shipments = shipments.ToArray();
                con.Routings = routings.ToArray();
                consols.Add(con);
                content.Consols = consols.ToArray();
                borgXML.Header = header;
                borgXML.Content = content;
                borgXML.ConversionType = "COS";
                
            }
            else if(commonXML.TimeSlotRequests != null)
            {
                var customs = new List<CustomsDeclaration>();
                CustomsDeclaration cust;
                header = GetHeader(commonXML);
                foreach (var order in commonXML.TimeSlotRequests)
                {
                    cust = new CustomsDeclaration();
                    cust = GetCustomsDeclaration(order);
                    borgXML.JobNumber = cust.JobNumber;
                    customs.Add(cust);
                }

                
                content.CustomsDeclarations = customs.ToArray();
                borgXML.Header = header;
                borgXML.Content = content;
                borgXML.ConversionType = "DEC";
            }

            return borgXML;
        }

        public FileHeader GetHeader(NodeFile order)
        {
            var header = new FileHeader();

            header.TriggerInformation = new TriggerInformation()
            {
                StatusCode = order.IdendtityMatrix.EventCode,
                Description = order.IdendtityMatrix.EventDescription,
                Date = order.IdendtityMatrix.TriggerDate
            };

            return header;
        }

        private Routing[] GetShipmentRoutings(NodeFileShipment order)
        {
            List<Routing> routings = new List<Routing>();
            var routing = new Routing();
            
            foreach(var route in order.Routes)
            {
                foreach(var loc in route.Locations)
                {
                    if(loc.Type == "PortOfLoading")
                    {
                        routing.POLPort = new PortElement()
                        {
                            Port = loc.Code,
                            Country = loc.Code.Substring(0, 2),
                            City = loc.Name,
                            Estimated = loc.Estimated,
                            Actual = loc.Actual
                        };
                    }

                    if (loc.Type == "PortOfDischarge")
                    {
                        routing.PODPort = new PortElement()
                        {
                            Port = loc.Code,
                            Country = loc.Code.Substring(0, 2),
                            City = loc.Name,
                            Estimated = loc.Estimated,
                            Actual = loc.Actual
                        };
                    }
                }
                routing.LegNumber = route.LegOrder;
                routing.TranportMode = route.TransportMode;
                routing.Vessel = route.Vessel;
                routing.VoyageNo = route.VoyageNo;

                routings.Add(routing);
            }

            return routings.ToArray();

        }


        private Routing[] GetCustomRoutings(NodeFileTimeSlotRequest order)
        {
            List<Routing> routings = new List<Routing>();
            var routing = new Routing();

            foreach (var route in order.Routes)
            {
                foreach (var loc in route.Locations)
                {
                    if (loc.Type == "PortOfLoading")
                    {
                        routing.POLPort = new PortElement()
                        {
                            Port = loc.Code,
                            Country = loc.Code.Substring(0, 2),
                            City = loc.Name,
                            Estimated = loc.Estimated,
                            Actual = loc.Actual
                        };
                    }

                    if (loc.Type == "PortOfDischarge")
                    {
                        routing.PODPort = new PortElement()
                        {
                            Port = loc.Code,
                            Country = loc.Code.Substring(0, 2),
                            City = loc.Name,
                            Estimated = loc.Estimated,
                            Actual = loc.Actual
                        };
                    }
                }
                routing.LegNumber = route.LegOrder;
                routing.TranportMode = route.TransportMode;
                routing.Vessel = route.Vessel;
                routing.VoyageNo = route.VoyageNo;

                routings.Add(routing);
            }

            return routings.ToArray();

        }

        private Borg.Shipment GetShipment(NodeFileShipment order)
        {
            var shipment = new Borg.Shipment();

            shipment.ShipmentNumber = order.Key;
            shipment.TransportMode = order.TransportMode.ToString();
            shipment.HouseBillNo = order.HouseBillNo;
            shipment.Weight = new TypeValuePair
            {
                Type = order.TotalWeightUnit.ToString(),
                Value = order.TotalWeight.ToString()
            };

            if(order.TransportMode == NodeFileShipmentTransportMode.SEA)
            {
                shipment.ChargeableWeight = new TypeValuePair
                {
                    Type = order.TotalVolumeUnit.ToString(),
                    Value = order.ChargeableWeight.ToString()
                };
            }
            else
            {
                shipment.ChargeableWeight = new TypeValuePair
                {
                    Type = order.TotalWeightUnit.ToString(),
                    Value = order.ChargeableWeight.ToString()
                };
            }

            

            shipment.Volume = new TypeValuePair
            {
                Type = order.TotalVolumeUnit.ToString(),
                Value = order.TotalVolume.ToString()
            };

            shipment.Term = new Term { IncoTerm = order.IncoTerms };

            shipment.Consignee = new Company
            {
                EDICode = order.Companies.Where(x => x.CompanyType == CompanyElementCompanyType.Consignee).Select(x => x.CompanyOrgCode).FirstOrDefault(),
                Name = order.Companies.Where(x => x.CompanyType == CompanyElementCompanyType.Consignee).Select(x => x.CompanyName).FirstOrDefault()
            };

            shipment.Consignor = new Company
            {
                EDICode = order.Companies.Where(x => x.CompanyType == CompanyElementCompanyType.Consignor).Select(x => x.CompanyOrgCode).FirstOrDefault(),
                Name = order.Companies.Where(x => x.CompanyType == CompanyElementCompanyType.Consignor).Select(x => x.CompanyName).FirstOrDefault()
            };

            shipment.ContainerMode = order.ContainerMode.ToString();

            List<Container> containers = new List<Container>();

            foreach (var con in order.Containers)
            {
                Container c = new Container();

                c.ContainerNr = con.ContainerNo;
                c.GrossWeight = new TypeValuePair
                {
                    Type = con.WeightUnit.ToString(),
                    Value = con.GrossWeight.ToString()
                };

                containers.Add(c);
            }

            shipment.Containers = containers.ToArray();


            List<Order> orders = new List<Order>();

            foreach (var ord in order.ShipmentLines)
            {
                Order o = new Order();

                o.OrderNumber = ord.OrderNumber;
                o.IsSystemGenerated = false;

                orders.Add(o);
            }

            shipment.Orders = orders.ToArray();

            return shipment;
        }

        private Borg.CustomsDeclaration GetCustomsDeclaration(NodeFileTimeSlotRequest order)
        {
            var declaration = new Borg.CustomsDeclaration();

            declaration.JobNumber = order.ShipmentNo;
            declaration.TransportMode = order.TransportType;
            declaration.MasterBill = order.MasterBill;
            declaration.ContainerMode = order.ShipmentType.ToString();
            declaration.ContainerModeSpecified = true;
            declaration.TransportModeSpecified = true;
            declaration.TotalWeight = new TypeValuePair
            {
                Type = order.WeightUnit.ToString(),
                Value = order.TotalWeight.ToString()
            };

            declaration.TotalVolume = new TypeValuePair
            {
                Type = order.VolumeUnit.ToString(),
                Value = order.TotalVolume.ToString()
            };

            declaration.IncoTerm = new Term { IncoTerm = order.IncoTerms };

            foreach(var com in order.Companies)
            {
                if(com != null)
                {
                    if (com.CompanyType == CompanyElementCompanyType.Supplier)
                    {
                        declaration.Supplier = new Company
                        {
                            EDICode = com.CompanyOrgCode,
                            Name = com.CompanyName
                        };
                    }

                    if (com.CompanyType == CompanyElementCompanyType.Buyer)
                    {
                        declaration.Importer = new Company
                        {
                            EDICode = com.CompanyOrgCode,
                            Name = com.CompanyName
                        };
                    }
                }
                
            }

            

            List<Container> containers = new List<Container>();

            foreach (var con in order.Containers)
            {
                Container c = new Container();

                c.ContainerNr = con.ContainerNo;
                c.GrossWeight = new TypeValuePair
                {
                    Type = con.WeightUnit.ToString(),
                    Value = con.GrossWeight.ToString()
                };

                containers.Add(c);
            }

            declaration.Containers = containers.ToArray();
            declaration.Routings = GetCustomRoutings(order);

            return declaration;
        }


        private Borg.CustomsDeclaration GetDeclaration(NodeFileTimeSlotRequest order)
        {
            var cust = new Borg.CustomsDeclaration();

            cust.JobNumber = order.ShipmentNo;
            cust.MasterBill = order.HouseBill;
            cust.TransportMode = order.TransportType;
            cust.ContainerMode = order.ShipmentType.ToString();
            cust.TotalWeight = new TypeValuePair
            {
                Type = order.Weight.ToString(),
                Value = order.UnitOfMeasure.ToString()
            };

            cust.Importer = new Company
            {
                EDICode = "BORMANSYD",
                Name = "BORG MANUFACTURING PTY LTD"
            };

            cust.Supplier = new Company
            {
                EDICode = "",
                Name = order.Companies.Where(x => x.CompanyType == CompanyElementCompanyType.Supplier).Select(x => x.CompanyName).FirstOrDefault()
            };
            

            List<Container> containers = new List<Container>();

            foreach (var con in order.Containers)
            {
                Container c = new Container();

                c.ContainerNr = con.ContainerNo;
                c.GrossWeight = new TypeValuePair
                {
                    Type = con.WeightUnit.ToString(),
                    Value = con.GrossWeight.ToString()
                };

                containers.Add(c);
            }

            cust.Containers = containers.ToArray();


            return cust;
        }
    }

    
}

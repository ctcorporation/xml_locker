﻿using CTCLogging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using XML_Locker.CTC;

namespace XML_Locker.Cargowise
{
    public class CommonToCw
    {
        public string LogPath { get; set; }

        public CommonToCw()
        {

        }

        public CommonToCw(string logPath)
        {
            LogPath = logPath;
        }

        public void CreateProductFile(NodeFile common, CW.NDM.Action partOperation, string outputPath)
        {
            if (common.Operation.ProductImport != null)
            {
                string lo = LogPath;
                string cp = string.Empty;
                string de = "Processing Folder Queue";
                string fu = "Process Pickup";
                DateTime ti = DateTime.Now;
                string er = string.Empty;
                string di = string.Empty;
                foreach (ProductElement product in common.Operation.ProductImport)
                {
                    int iRec = 0;
                    try
                    {
                        CW.NDM.UniversalInterchange universalInterchange = new CW.NDM.UniversalInterchange();
                        CW.NDM.UniversalInterchangeHeader universalInterchangeHeader = new CW.NDM.UniversalInterchangeHeader();
                        CW.NDM.UniversalInterchangeBody universalInterchangeBody = new CW.NDM.UniversalInterchangeBody();
                        CW.NDM.ProductData productData = new CW.NDM.ProductData();
                        CW.NDM.Native native = new CW.NDM.Native();
                        CW.NDM.NativeHeader nativeHeader = new CW.NDM.NativeHeader();
                        CW.NDM.DataContext dataContext = new CW.NDM.DataContext();
                        CW.NDM.NativeBody nativeBody = new CW.NDM.NativeBody();
                        CW.NDM.NativeProduct nativeProduct = new CW.NDM.NativeProduct();
                        List<CW.NDM.NativeProductOrgPartRelation> nativeProductOrgRelationColl = new List<CW.NDM.NativeProductOrgPartRelation>();
                        CW.NDM.NativeProductOrgPartRelation nativeProductOrgPartRelation = new CW.NDM.NativeProductOrgPartRelation();
                        CW.NDM.NativeProductOrgPartRelationOrgHeader orgPartRelationOrgHeader = new CW.NDM.NativeProductOrgPartRelationOrgHeader();
                        List<CW.NDM.NativeProductOrgSupplierPartBarcode> nativeProductBarcodeColl = new List<CW.NDM.NativeProductOrgSupplierPartBarcode>();
                        CW.NDM.NativeProductOrgSupplierPartBarcode nativeProductBarcode = new CW.NDM.NativeProductOrgSupplierPartBarcode();
                        List<CW.NDM.NativeProductOrgPartUnit> nativeProductOrgPartUnitColl = new List<CW.NDM.NativeProductOrgPartUnit>();
                        iRec++;

                        nativeProduct = new CW.NDM.NativeProduct();
                        nativeProduct.PartNum = product.Code;
                        nativeProduct.StockKeepingUnit = product.StockKeepingUnit;
                        nativeProduct.Weight = product.Weight > 0 ? product.Weight : 0;
                        nativeProduct.WeightSpecified = product.Weight > 0 ? true : false;
                        nativeProduct.WeightUQ = product.WeightUnit != null ? product.WeightUnit : null;
                        nativeProduct.WeightSpecified = product.WeightUnit != null ? true : false;
                        nativeProduct.Height = product.Height > 0 ? product.Height : 0;
                        nativeProduct.HeightSpecified = product.Height > 0 ? true : false;
                        nativeProduct.Width = product.Width > 0 ? product.Width : 0;
                        nativeProduct.WidthSpecified = product.Width > 0 ? true : false;
                        nativeProduct.Depth = product.Depth > 0 ? product.Depth : 0;
                        nativeProduct.DepthSpecified = product.Depth > 0 ? true : false;
                        int volFactor = 1;
                        switch (product.DimsUnit)
                        {
                            case "CM":
                                volFactor = 100;
                                break;
                            case "IN":
                                volFactor = 1;
                                break;
                            case "M":
                                volFactor = 1;
                                break;

                            default:
                                volFactor = 100;
                                break;
                        }
                        decimal volume = 0;
                        if (nativeProduct.Height > 0 && nativeProduct.Width > 0 && nativeProduct.Depth > 0)
                        {
                            volume = ((product.Height / volFactor) * (product.Width / volFactor) * (product.Depth / volFactor));
                        }
                        nativeProduct.Cubic = volume > 0 ? volume : 0;
                        nativeProduct.CubicSpecified = volume > 0 ? true : false;
                        nativeProduct.CubicUQ = volume > 0 ? "M3" : null;
                        nativeProduct.MeasureUQ = product.DimsUnit != null ? product.DimsUnit : null;
                        nativeProduct.Desc = product.Description;
                        nativeProduct.ActionSpecified = true;
                        nativeProduct.Action = partOperation;
                        nativeProduct.IsActive = true;
                        nativeProduct.IsWarehouseProduct = true;
                        nativeProduct.IsWarehouseProductSpecified = true;
                        nativeProduct.IsBarcoded = true;
                        nativeProduct.IsBarcodedSpecified = true;
                        orgPartRelationOrgHeader = new CW.NDM.NativeProductOrgPartRelationOrgHeader();
                        nativeProductOrgRelationColl = new List<CW.NDM.NativeProductOrgPartRelation>();
                        orgPartRelationOrgHeader.Code = common.IdendtityMatrix.SenderId;
                        nativeProductOrgPartRelation.OrgHeader = orgPartRelationOrgHeader;
                        nativeProductOrgPartRelation.Relationship = "OWN";
                        nativeProductOrgPartRelation.Action = partOperation;
                        nativeProductOrgPartRelation.ActionSpecified = true;
                        nativeProductOrgRelationColl.Add(nativeProductOrgPartRelation);

                        nativeProduct.OrgPartRelationCollection = nativeProductOrgRelationColl.ToArray();
                        if (product.Barcode != null)
                        {
                            nativeProductBarcodeColl = new List<CW.NDM.NativeProductOrgSupplierPartBarcode>();
                            nativeProduct.IsBarcoded = true;
                            nativeProductBarcode = new CW.NDM.NativeProductOrgSupplierPartBarcode();
                            nativeProductBarcode.Action = CW.NDM.Action.MERGE;
                            nativeProductBarcode.ActionSpecified = true;
                            nativeProductBarcode.Barcode = product.Barcode;
                            //nativeProductBarcode.PackType.TableName = "RefPackType";
                            CW.NDM.NativeProductOrgSupplierPartBarcodePackType productPackType = new CW.NDM.NativeProductOrgSupplierPartBarcodePackType();
                            productPackType.TableName = "RefPackType";
                            productPackType.Code = "UNT";
                            nativeProductBarcode.PackType = productPackType;
                            nativeProductBarcodeColl.Add(nativeProductBarcode);
                            nativeProduct.OrgSupplierPartBarcodeCollection = nativeProductBarcodeColl.ToArray();
                        }
                        else
                        {
                            nativeProduct.IsBarcoded = false;
                        }
                        var cwNSUniversal = new XmlSerializerNamespaces();
                        cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                        var cwNSNative = new XmlSerializerNamespaces();
                        cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
                        productData.OrgSupplierPart = nativeProduct;
                        productData.version = "2.0";
                        nativeProduct.Action = partOperation;
                        nativeBody.Any = new[] { productData.AsXmlElement(cwNSNative) };
                        nativeHeader.OwnerCode = common.IdendtityMatrix.SenderId;
                        nativeHeader.EnableCodeMapping = true;
                        nativeHeader.EnableCodeMappingSpecified = true;
                        dataContext.ActionPurpose = new CW.NDM.CodeDescriptionPair { Code = "IMP", Description = "Product Import" };
                        dataContext.EventType = new CW.NDM.CodeDescriptionPair { Code = "ADD", Description = "Record Added" };
                        native.Header.DataContext = dataContext;
                        native.version = "2.0";
                        native.Header = nativeHeader;
                        native.Body = nativeBody;
                        universalInterchange.version = "1.1";
                        universalInterchangeHeader.SenderID = common.IdendtityMatrix.SenderId;
                        universalInterchangeHeader.RecipientID = common.IdendtityMatrix.CustomerId;
                        universalInterchangeBody.Any = new[] { native.AsXmlElement(cwNSUniversal) };
                        universalInterchange.Body = universalInterchangeBody;
                        universalInterchange.Header = universalInterchangeHeader;
                        String cwXML = Path.Combine(outputPath, common.IdendtityMatrix.SenderId + "PRODUCT" + "-" + product.Code + partOperation.ToString() + ".xml");
                        int iFileCount = 0;
                        while (File.Exists(cwXML))
                        {
                            iFileCount++;
                            cwXML = Path.Combine(outputPath, common.IdendtityMatrix.SenderId + "PRODUCT" + "-" + product.Code + partOperation + iFileCount + ".xml");
                        }
                        using (Stream outputCW = File.Open(cwXML, FileMode.Create))
                        {
                            StringWriter writer = new StringWriter();
                            XmlSerializer xSer = new XmlSerializer(typeof(CW.NDM.UniversalInterchange));
                            xSer.Serialize(outputCW, universalInterchange, cwNSUniversal);
                            outputCW.Flush();
                            outputCW.Close();
                        }
                    }


                    catch (Exception ex)
                    {
                        cp = "Error Creating Product File";
                        de = "Error Processing " + product.Code;
                        ti = DateTime.Now;
                        er = "Error:" + ex.GetType().Name + " :" + ex.Message;
                        di = outputPath;
                        using (AppLogger log = new AppLogger(lo, cp, de, fu, ti, er, di))
                        {
                            log.AddLog();
                        }

                    }
                }
            }
        }

    }
}

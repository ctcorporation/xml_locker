﻿
namespace XMLLocker.Cargowise.Native
{


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11")]
    public partial class NativeRate
    {

        private string pkField;

        private string quoteNumberField;

        private string quoteDateField;

        private string quoteEndDateField;

        private string followUpDateField;

        private string acceptedField;

        private string rateTypeField;

        private short globalRateLevelField;

        private bool globalRateLevelFieldSpecified;

        private string globalRateDescriptionField;

        private decimal airCFXField;

        private bool airCFXFieldSpecified;

        private decimal seaCFXField;

        private bool seaCFXFieldSpecified;

        private decimal exportAirCFXField;

        private bool exportAirCFXFieldSpecified;

        private decimal exportSeaCFXField;

        private bool exportSeaCFXFieldSpecified;

        private string systemLastEditTimeUtcField;

        private string systemCreateTimeUtcField;

        private string quoteCancellationReasonField;

        private bool isLockedField;

        private bool isLockedFieldSpecified;

        private bool isOneOffQuoteConsumedField;

        private bool isOneOffQuoteConsumedFieldSpecified;

        private bool printRateLevelOriginChargesField;

        private bool printRateLevelOriginChargesFieldSpecified;

        private bool printRateLevelDestinationChargesField;

        private bool printRateLevelDestinationChargesFieldSpecified;

        private bool printInheritedOriginChargesField;

        private bool printInheritedOriginChargesFieldSpecified;

        private bool printInheritedDestinationChargesField;

        private bool printInheritedDestinationChargesFieldSpecified;

        private bool isCancelledField;

        private bool isCancelledFieldSpecified;

        private bool oneTimeQuoteField;

        private bool oneTimeQuoteFieldSpecified;

        private NativeRateFirstSignatory firstSignatoryField;

        private NativeRateSecondSignatory secondSignatoryField;

        private NativeRateOrgHeader orgHeaderField;

        private NativeRateGlbCompany glbCompanyField;

        private NativeRateRateEntry[] rateEntryCollectionField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public string QuoteNumber
        {
            get
            {
                return this.quoteNumberField;
            }
            set
            {
                this.quoteNumberField = value;
            }
        }

        /// <remarks/>
        public string QuoteDate
        {
            get
            {
                return this.quoteDateField;
            }
            set
            {
                this.quoteDateField = value;
            }
        }

        /// <remarks/>
        public string QuoteEndDate
        {
            get
            {
                return this.quoteEndDateField;
            }
            set
            {
                this.quoteEndDateField = value;
            }
        }

        /// <remarks/>
        public string FollowUpDate
        {
            get
            {
                return this.followUpDateField;
            }
            set
            {
                this.followUpDateField = value;
            }
        }

        /// <remarks/>
        public string Accepted
        {
            get
            {
                return this.acceptedField;
            }
            set
            {
                this.acceptedField = value;
            }
        }

        /// <remarks/>
        public string RateType
        {
            get
            {
                return this.rateTypeField;
            }
            set
            {
                this.rateTypeField = value;
            }
        }

        /// <remarks/>
        public short GlobalRateLevel
        {
            get
            {
                return this.globalRateLevelField;
            }
            set
            {
                this.globalRateLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GlobalRateLevelSpecified
        {
            get
            {
                return this.globalRateLevelFieldSpecified;
            }
            set
            {
                this.globalRateLevelFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string GlobalRateDescription
        {
            get
            {
                return this.globalRateDescriptionField;
            }
            set
            {
                this.globalRateDescriptionField = value;
            }
        }

        /// <remarks/>
        public decimal AirCFX
        {
            get
            {
                return this.airCFXField;
            }
            set
            {
                this.airCFXField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AirCFXSpecified
        {
            get
            {
                return this.airCFXFieldSpecified;
            }
            set
            {
                this.airCFXFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal SeaCFX
        {
            get
            {
                return this.seaCFXField;
            }
            set
            {
                this.seaCFXField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SeaCFXSpecified
        {
            get
            {
                return this.seaCFXFieldSpecified;
            }
            set
            {
                this.seaCFXFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal ExportAirCFX
        {
            get
            {
                return this.exportAirCFXField;
            }
            set
            {
                this.exportAirCFXField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExportAirCFXSpecified
        {
            get
            {
                return this.exportAirCFXFieldSpecified;
            }
            set
            {
                this.exportAirCFXFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal ExportSeaCFX
        {
            get
            {
                return this.exportSeaCFXField;
            }
            set
            {
                this.exportSeaCFXField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExportSeaCFXSpecified
        {
            get
            {
                return this.exportSeaCFXFieldSpecified;
            }
            set
            {
                this.exportSeaCFXFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string SystemLastEditTimeUtc
        {
            get
            {
                return this.systemLastEditTimeUtcField;
            }
            set
            {
                this.systemLastEditTimeUtcField = value;
            }
        }

        /// <remarks/>
        public string SystemCreateTimeUtc
        {
            get
            {
                return this.systemCreateTimeUtcField;
            }
            set
            {
                this.systemCreateTimeUtcField = value;
            }
        }

        /// <remarks/>
        public string QuoteCancellationReason
        {
            get
            {
                return this.quoteCancellationReasonField;
            }
            set
            {
                this.quoteCancellationReasonField = value;
            }
        }

        /// <remarks/>
        public bool IsLocked
        {
            get
            {
                return this.isLockedField;
            }
            set
            {
                this.isLockedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsLockedSpecified
        {
            get
            {
                return this.isLockedFieldSpecified;
            }
            set
            {
                this.isLockedFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsOneOffQuoteConsumed
        {
            get
            {
                return this.isOneOffQuoteConsumedField;
            }
            set
            {
                this.isOneOffQuoteConsumedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsOneOffQuoteConsumedSpecified
        {
            get
            {
                return this.isOneOffQuoteConsumedFieldSpecified;
            }
            set
            {
                this.isOneOffQuoteConsumedFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool PrintRateLevelOriginCharges
        {
            get
            {
                return this.printRateLevelOriginChargesField;
            }
            set
            {
                this.printRateLevelOriginChargesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrintRateLevelOriginChargesSpecified
        {
            get
            {
                return this.printRateLevelOriginChargesFieldSpecified;
            }
            set
            {
                this.printRateLevelOriginChargesFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool PrintRateLevelDestinationCharges
        {
            get
            {
                return this.printRateLevelDestinationChargesField;
            }
            set
            {
                this.printRateLevelDestinationChargesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrintRateLevelDestinationChargesSpecified
        {
            get
            {
                return this.printRateLevelDestinationChargesFieldSpecified;
            }
            set
            {
                this.printRateLevelDestinationChargesFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool PrintInheritedOriginCharges
        {
            get
            {
                return this.printInheritedOriginChargesField;
            }
            set
            {
                this.printInheritedOriginChargesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrintInheritedOriginChargesSpecified
        {
            get
            {
                return this.printInheritedOriginChargesFieldSpecified;
            }
            set
            {
                this.printInheritedOriginChargesFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool PrintInheritedDestinationCharges
        {
            get
            {
                return this.printInheritedDestinationChargesField;
            }
            set
            {
                this.printInheritedDestinationChargesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrintInheritedDestinationChargesSpecified
        {
            get
            {
                return this.printInheritedDestinationChargesFieldSpecified;
            }
            set
            {
                this.printInheritedDestinationChargesFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsCancelled
        {
            get
            {
                return this.isCancelledField;
            }
            set
            {
                this.isCancelledField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsCancelledSpecified
        {
            get
            {
                return this.isCancelledFieldSpecified;
            }
            set
            {
                this.isCancelledFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool OneTimeQuote
        {
            get
            {
                return this.oneTimeQuoteField;
            }
            set
            {
                this.oneTimeQuoteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OneTimeQuoteSpecified
        {
            get
            {
                return this.oneTimeQuoteFieldSpecified;
            }
            set
            {
                this.oneTimeQuoteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public NativeRateFirstSignatory FirstSignatory
        {
            get
            {
                return this.firstSignatoryField;
            }
            set
            {
                this.firstSignatoryField = value;
            }
        }

        /// <remarks/>
        public NativeRateSecondSignatory SecondSignatory
        {
            get
            {
                return this.secondSignatoryField;
            }
            set
            {
                this.secondSignatoryField = value;
            }
        }

        /// <remarks/>
        public NativeRateOrgHeader OrgHeader
        {
            get
            {
                return this.orgHeaderField;
            }
            set
            {
                this.orgHeaderField = value;
            }
        }

        /// <remarks/>
        public NativeRateGlbCompany GlbCompany
        {
            get
            {
                return this.glbCompanyField;
            }
            set
            {
                this.glbCompanyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("RateEntry", IsNullable = false)]
        public NativeRateRateEntry[] RateEntryCollection
        {
            get
            {
                return this.rateEntryCollectionField;
            }
            set
            {
                this.rateEntryCollectionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateFirstSignatory
    {

        private string codeField;

        private string loginNameField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string LoginName
        {
            get
            {
                return this.loginNameField;
            }
            set
            {
                this.loginNameField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public enum Action
    {

        /// <remarks/>
        INSERT,

        /// <remarks/>
        UPDATE,

        /// <remarks/>
        MERGE,

        /// <remarks/>
        DELETE,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateSecondSignatory
    {

        private string codeField;

        private string loginNameField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string LoginName
        {
            get
            {
                return this.loginNameField;
            }
            set
            {
                this.loginNameField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateOrgHeader
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateGlbCompany
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntry
    {

        private string pkField;

        private int lineOrderField;

        private bool lineOrderFieldSpecified;

        private string rateStartDateField;

        private string rateEndDateField;

        private int frequencyField;

        private bool frequencyFieldSpecified;

        private string cartagePickupAddressPostCodeField;

        private string cartageDeliveryAddressPostCodeField;

        private string originLRCField;

        private string destinationLRCField;

        private string viaLRCField;

        private string pageHeadingField;

        private string pageOpeningTextField;

        private string pageClosingTextField;

        private string quotePageIncoTermField;

        private string buyersConsolRateModeField;

        private string systemCreateTimeUtcField;

        private string systemLastEditTimeUtcField;

        private string contractNumberField;

        private bool isCrossTradeField;

        private bool isCrossTradeFieldSpecified;

        private bool dataCheckedField;

        private bool dataCheckedFieldSpecified;

        private bool matchContainerRateClassField;

        private bool matchContainerRateClassFieldSpecified;

        private string transitTimeField;

        private string frequencyUnitField;

        private string modeField;

        private string rateCategoryField;

        private string fromIDField;

        private string fromTableCodeField;

        private string toIdField;

        private string toTableCodeField;

        private bool isTactField;

        private bool isTactFieldSpecified;

        private NativeRateRateEntryCurrency currencyField;

        private NativeRateRateEntrySupplier supplierField;

        private NativeRateRateEntryTransportProvider transportProviderField;

        private NativeRateRateEntryConsignor consignorField;

        private NativeRateRateEntryConsignee consigneeField;

        private NativeRateRateEntryCartagePickupAddressOverride cartagePickupAddressOverrideField;

        private NativeRateRateEntryCartageDeliveryAddressOverride cartageDeliveryAddressOverrideField;

        private NativeRateRateEntryServiceLevel_NI serviceLevel_NIField;

        private NativeRateRateEntryAgentOverride agentOverrideField;

        private NativeRateRateEntryRefContainer refContainerField;

        private NativeRateRateEntryCarrierServiceLevel carrierServiceLevelField;

        private NativeRateRateEntryOriginZone originZoneField;

        private NativeRateRateEntryDestinationZone destinationZoneField;

        private NativeRateRateEntryCommodityCode commodityCodeField;

        private NativeRateRateEntryControllingCustomer controllingCustomerField;

        private NativeRateRateEntryPublisher publisherField;

        private NativeRateRateEntryRateLines[] rateLinesCollectionField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public int LineOrder
        {
            get
            {
                return this.lineOrderField;
            }
            set
            {
                this.lineOrderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LineOrderSpecified
        {
            get
            {
                return this.lineOrderFieldSpecified;
            }
            set
            {
                this.lineOrderFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string RateStartDate
        {
            get
            {
                return this.rateStartDateField;
            }
            set
            {
                this.rateStartDateField = value;
            }
        }

        /// <remarks/>
        public string RateEndDate
        {
            get
            {
                return this.rateEndDateField;
            }
            set
            {
                this.rateEndDateField = value;
            }
        }

        /// <remarks/>
        public int Frequency
        {
            get
            {
                return this.frequencyField;
            }
            set
            {
                this.frequencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FrequencySpecified
        {
            get
            {
                return this.frequencyFieldSpecified;
            }
            set
            {
                this.frequencyFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string CartagePickupAddressPostCode
        {
            get
            {
                return this.cartagePickupAddressPostCodeField;
            }
            set
            {
                this.cartagePickupAddressPostCodeField = value;
            }
        }

        /// <remarks/>
        public string CartageDeliveryAddressPostCode
        {
            get
            {
                return this.cartageDeliveryAddressPostCodeField;
            }
            set
            {
                this.cartageDeliveryAddressPostCodeField = value;
            }
        }

        /// <remarks/>
        public string OriginLRC
        {
            get
            {
                return this.originLRCField;
            }
            set
            {
                this.originLRCField = value;
            }
        }

        /// <remarks/>
        public string DestinationLRC
        {
            get
            {
                return this.destinationLRCField;
            }
            set
            {
                this.destinationLRCField = value;
            }
        }

        /// <remarks/>
        public string ViaLRC
        {
            get
            {
                return this.viaLRCField;
            }
            set
            {
                this.viaLRCField = value;
            }
        }

        /// <remarks/>
        public string PageHeading
        {
            get
            {
                return this.pageHeadingField;
            }
            set
            {
                this.pageHeadingField = value;
            }
        }

        /// <remarks/>
        public string PageOpeningText
        {
            get
            {
                return this.pageOpeningTextField;
            }
            set
            {
                this.pageOpeningTextField = value;
            }
        }

        /// <remarks/>
        public string PageClosingText
        {
            get
            {
                return this.pageClosingTextField;
            }
            set
            {
                this.pageClosingTextField = value;
            }
        }

        /// <remarks/>
        public string QuotePageIncoTerm
        {
            get
            {
                return this.quotePageIncoTermField;
            }
            set
            {
                this.quotePageIncoTermField = value;
            }
        }

        /// <remarks/>
        public string BuyersConsolRateMode
        {
            get
            {
                return this.buyersConsolRateModeField;
            }
            set
            {
                this.buyersConsolRateModeField = value;
            }
        }

        /// <remarks/>
        public string SystemCreateTimeUtc
        {
            get
            {
                return this.systemCreateTimeUtcField;
            }
            set
            {
                this.systemCreateTimeUtcField = value;
            }
        }

        /// <remarks/>
        public string SystemLastEditTimeUtc
        {
            get
            {
                return this.systemLastEditTimeUtcField;
            }
            set
            {
                this.systemLastEditTimeUtcField = value;
            }
        }

        /// <remarks/>
        public string ContractNumber
        {
            get
            {
                return this.contractNumberField;
            }
            set
            {
                this.contractNumberField = value;
            }
        }

        /// <remarks/>
        public bool IsCrossTrade
        {
            get
            {
                return this.isCrossTradeField;
            }
            set
            {
                this.isCrossTradeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsCrossTradeSpecified
        {
            get
            {
                return this.isCrossTradeFieldSpecified;
            }
            set
            {
                this.isCrossTradeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool DataChecked
        {
            get
            {
                return this.dataCheckedField;
            }
            set
            {
                this.dataCheckedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataCheckedSpecified
        {
            get
            {
                return this.dataCheckedFieldSpecified;
            }
            set
            {
                this.dataCheckedFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool MatchContainerRateClass
        {
            get
            {
                return this.matchContainerRateClassField;
            }
            set
            {
                this.matchContainerRateClassField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MatchContainerRateClassSpecified
        {
            get
            {
                return this.matchContainerRateClassFieldSpecified;
            }
            set
            {
                this.matchContainerRateClassFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TransitTime
        {
            get
            {
                return this.transitTimeField;
            }
            set
            {
                this.transitTimeField = value;
            }
        }

        /// <remarks/>
        public string FrequencyUnit
        {
            get
            {
                return this.frequencyUnitField;
            }
            set
            {
                this.frequencyUnitField = value;
            }
        }

        /// <remarks/>
        public string Mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        public string RateCategory
        {
            get
            {
                return this.rateCategoryField;
            }
            set
            {
                this.rateCategoryField = value;
            }
        }

        /// <remarks/>
        public string FromID
        {
            get
            {
                return this.fromIDField;
            }
            set
            {
                this.fromIDField = value;
            }
        }

        /// <remarks/>
        public string FromTableCode
        {
            get
            {
                return this.fromTableCodeField;
            }
            set
            {
                this.fromTableCodeField = value;
            }
        }

        /// <remarks/>
        public string ToId
        {
            get
            {
                return this.toIdField;
            }
            set
            {
                this.toIdField = value;
            }
        }

        /// <remarks/>
        public string ToTableCode
        {
            get
            {
                return this.toTableCodeField;
            }
            set
            {
                this.toTableCodeField = value;
            }
        }

        /// <remarks/>
        public bool IsTact
        {
            get
            {
                return this.isTactField;
            }
            set
            {
                this.isTactField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsTactSpecified
        {
            get
            {
                return this.isTactFieldSpecified;
            }
            set
            {
                this.isTactFieldSpecified = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCurrency Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntrySupplier Supplier
        {
            get
            {
                return this.supplierField;
            }
            set
            {
                this.supplierField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryTransportProvider TransportProvider
        {
            get
            {
                return this.transportProviderField;
            }
            set
            {
                this.transportProviderField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryConsignor Consignor
        {
            get
            {
                return this.consignorField;
            }
            set
            {
                this.consignorField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryConsignee Consignee
        {
            get
            {
                return this.consigneeField;
            }
            set
            {
                this.consigneeField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCartagePickupAddressOverride CartagePickupAddressOverride
        {
            get
            {
                return this.cartagePickupAddressOverrideField;
            }
            set
            {
                this.cartagePickupAddressOverrideField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCartageDeliveryAddressOverride CartageDeliveryAddressOverride
        {
            get
            {
                return this.cartageDeliveryAddressOverrideField;
            }
            set
            {
                this.cartageDeliveryAddressOverrideField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryServiceLevel_NI ServiceLevel_NI
        {
            get
            {
                return this.serviceLevel_NIField;
            }
            set
            {
                this.serviceLevel_NIField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryAgentOverride AgentOverride
        {
            get
            {
                return this.agentOverrideField;
            }
            set
            {
                this.agentOverrideField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRefContainer RefContainer
        {
            get
            {
                return this.refContainerField;
            }
            set
            {
                this.refContainerField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCarrierServiceLevel CarrierServiceLevel
        {
            get
            {
                return this.carrierServiceLevelField;
            }
            set
            {
                this.carrierServiceLevelField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryOriginZone OriginZone
        {
            get
            {
                return this.originZoneField;
            }
            set
            {
                this.originZoneField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryDestinationZone DestinationZone
        {
            get
            {
                return this.destinationZoneField;
            }
            set
            {
                this.destinationZoneField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCommodityCode CommodityCode
        {
            get
            {
                return this.commodityCodeField;
            }
            set
            {
                this.commodityCodeField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryControllingCustomer ControllingCustomer
        {
            get
            {
                return this.controllingCustomerField;
            }
            set
            {
                this.controllingCustomerField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryPublisher Publisher
        {
            get
            {
                return this.publisherField;
            }
            set
            {
                this.publisherField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("RateLines", IsNullable = false)]
        public NativeRateRateEntryRateLines[] RateLinesCollection
        {
            get
            {
                return this.rateLinesCollectionField;
            }
            set
            {
                this.rateLinesCollectionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCurrency
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntrySupplier
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryTransportProvider
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryConsignor
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryConsignee
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCartagePickupAddressOverride
    {

        private string codeField;

        private string pkField;

        private NativeRateRateEntryCartagePickupAddressOverrideOrgHeader orgHeaderField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCartagePickupAddressOverrideOrgHeader OrgHeader
        {
            get
            {
                return this.orgHeaderField;
            }
            set
            {
                this.orgHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCartagePickupAddressOverrideOrgHeader
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCartageDeliveryAddressOverride
    {

        private string codeField;

        private string pkField;

        private NativeRateRateEntryCartageDeliveryAddressOverrideOrgHeader orgHeaderField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCartageDeliveryAddressOverrideOrgHeader OrgHeader
        {
            get
            {
                return this.orgHeaderField;
            }
            set
            {
                this.orgHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCartageDeliveryAddressOverrideOrgHeader
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryServiceLevel_NI
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryAgentOverride
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRefContainer
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCarrierServiceLevel
    {

        private string codeField;

        private string carrierServiceLevelDescriptionField;

        private string pkField;

        private NativeRateRateEntryCarrierServiceLevelOrgMiscServ orgMiscServField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string CarrierServiceLevelDescription
        {
            get
            {
                return this.carrierServiceLevelDescriptionField;
            }
            set
            {
                this.carrierServiceLevelDescriptionField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCarrierServiceLevelOrgMiscServ OrgMiscServ
        {
            get
            {
                return this.orgMiscServField;
            }
            set
            {
                this.orgMiscServField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCarrierServiceLevelOrgMiscServ
    {

        private string pkField;

        private NativeRateRateEntryCarrierServiceLevelOrgMiscServOrgHeader orgHeaderField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryCarrierServiceLevelOrgMiscServOrgHeader OrgHeader
        {
            get
            {
                return this.orgHeaderField;
            }
            set
            {
                this.orgHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCarrierServiceLevelOrgMiscServOrgHeader
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryOriginZone
    {

        private string zoneNameField;

        private string pkField;

        private NativeRateRateEntryOriginZoneRateTransportProvider rateTransportProviderField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string ZoneName
        {
            get
            {
                return this.zoneNameField;
            }
            set
            {
                this.zoneNameField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryOriginZoneRateTransportProvider RateTransportProvider
        {
            get
            {
                return this.rateTransportProviderField;
            }
            set
            {
                this.rateTransportProviderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryOriginZoneRateTransportProvider
    {

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryDestinationZone
    {

        private string zoneNameField;

        private string pkField;

        private NativeRateRateEntryDestinationZoneRateTransportProvider rateTransportProviderField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string ZoneName
        {
            get
            {
                return this.zoneNameField;
            }
            set
            {
                this.zoneNameField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryDestinationZoneRateTransportProvider RateTransportProvider
        {
            get
            {
                return this.rateTransportProviderField;
            }
            set
            {
                this.rateTransportProviderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryDestinationZoneRateTransportProvider
    {

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryCommodityCode
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryControllingCustomer
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryPublisher
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLines
    {

        private string pkField;

        private short lineOrderField;

        private bool lineOrderFieldSpecified;

        private string rateDescField;

        private decimal conversionFactorField;

        private bool conversionFactorFieldSpecified;

        private string weightVolumeField;

        private decimal weightVolumeMultipleField;

        private bool weightVolumeMultipleFieldSpecified;

        private string rateCalculatorField;

        private short companyTariffLevelField;

        private bool companyTariffLevelFieldSpecified;

        private string roundingField;

        private decimal roundingFactorField;

        private bool roundingFactorFieldSpecified;

        private short actualPercentageField;

        private bool actualPercentageFieldSpecified;

        private bool isWhsJobLevelChargeField;

        private bool isWhsJobLevelChargeFieldSpecified;

        private bool isOnPalletsField;

        private bool isOnPalletsFieldSpecified;

        private string conditionField;

        private string rateDescLocalField;

        private string factorNumeratorField;

        private string factorDenominatorField;

        private string feeChargeLevelField;

        private string feeChargeTypeField;

        private NativeRateRateEntryRateLinesCurrency currencyField;

        private NativeRateRateEntryRateLinesAccChargeCode accChargeCodeField;

        private NativeRateRateEntryRateLinesRateLineItems[] rateLineItemsCollectionField;

        private NativeRateRateEntryRateLinesStmNote[] stmNoteCollectionField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public short LineOrder
        {
            get
            {
                return this.lineOrderField;
            }
            set
            {
                this.lineOrderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LineOrderSpecified
        {
            get
            {
                return this.lineOrderFieldSpecified;
            }
            set
            {
                this.lineOrderFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string RateDesc
        {
            get
            {
                return this.rateDescField;
            }
            set
            {
                this.rateDescField = value;
            }
        }

        /// <remarks/>
        public decimal ConversionFactor
        {
            get
            {
                return this.conversionFactorField;
            }
            set
            {
                this.conversionFactorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ConversionFactorSpecified
        {
            get
            {
                return this.conversionFactorFieldSpecified;
            }
            set
            {
                this.conversionFactorFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string WeightVolume
        {
            get
            {
                return this.weightVolumeField;
            }
            set
            {
                this.weightVolumeField = value;
            }
        }

        /// <remarks/>
        public decimal WeightVolumeMultiple
        {
            get
            {
                return this.weightVolumeMultipleField;
            }
            set
            {
                this.weightVolumeMultipleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool WeightVolumeMultipleSpecified
        {
            get
            {
                return this.weightVolumeMultipleFieldSpecified;
            }
            set
            {
                this.weightVolumeMultipleFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string RateCalculator
        {
            get
            {
                return this.rateCalculatorField;
            }
            set
            {
                this.rateCalculatorField = value;
            }
        }

        /// <remarks/>
        public short CompanyTariffLevel
        {
            get
            {
                return this.companyTariffLevelField;
            }
            set
            {
                this.companyTariffLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompanyTariffLevelSpecified
        {
            get
            {
                return this.companyTariffLevelFieldSpecified;
            }
            set
            {
                this.companyTariffLevelFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Rounding
        {
            get
            {
                return this.roundingField;
            }
            set
            {
                this.roundingField = value;
            }
        }

        /// <remarks/>
        public decimal RoundingFactor
        {
            get
            {
                return this.roundingFactorField;
            }
            set
            {
                this.roundingFactorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RoundingFactorSpecified
        {
            get
            {
                return this.roundingFactorFieldSpecified;
            }
            set
            {
                this.roundingFactorFieldSpecified = value;
            }
        }

        /// <remarks/>
        public short ActualPercentage
        {
            get
            {
                return this.actualPercentageField;
            }
            set
            {
                this.actualPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActualPercentageSpecified
        {
            get
            {
                return this.actualPercentageFieldSpecified;
            }
            set
            {
                this.actualPercentageFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsWhsJobLevelCharge
        {
            get
            {
                return this.isWhsJobLevelChargeField;
            }
            set
            {
                this.isWhsJobLevelChargeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsWhsJobLevelChargeSpecified
        {
            get
            {
                return this.isWhsJobLevelChargeFieldSpecified;
            }
            set
            {
                this.isWhsJobLevelChargeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsOnPallets
        {
            get
            {
                return this.isOnPalletsField;
            }
            set
            {
                this.isOnPalletsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsOnPalletsSpecified
        {
            get
            {
                return this.isOnPalletsFieldSpecified;
            }
            set
            {
                this.isOnPalletsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Condition
        {
            get
            {
                return this.conditionField;
            }
            set
            {
                this.conditionField = value;
            }
        }

        /// <remarks/>
        public string RateDescLocal
        {
            get
            {
                return this.rateDescLocalField;
            }
            set
            {
                this.rateDescLocalField = value;
            }
        }

        /// <remarks/>
        public string FactorNumerator
        {
            get
            {
                return this.factorNumeratorField;
            }
            set
            {
                this.factorNumeratorField = value;
            }
        }

        /// <remarks/>
        public string FactorDenominator
        {
            get
            {
                return this.factorDenominatorField;
            }
            set
            {
                this.factorDenominatorField = value;
            }
        }

        /// <remarks/>
        public string FeeChargeLevel
        {
            get
            {
                return this.feeChargeLevelField;
            }
            set
            {
                this.feeChargeLevelField = value;
            }
        }

        /// <remarks/>
        public string FeeChargeType
        {
            get
            {
                return this.feeChargeTypeField;
            }
            set
            {
                this.feeChargeTypeField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRateLinesCurrency Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRateLinesAccChargeCode AccChargeCode
        {
            get
            {
                return this.accChargeCodeField;
            }
            set
            {
                this.accChargeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("RateLineItems", IsNullable = false)]
        public NativeRateRateEntryRateLinesRateLineItems[] RateLineItemsCollection
        {
            get
            {
                return this.rateLineItemsCollectionField;
            }
            set
            {
                this.rateLineItemsCollectionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("StmNote", IsNullable = false)]
        public NativeRateRateEntryRateLinesStmNote[] StmNoteCollection
        {
            get
            {
                return this.stmNoteCollectionField;
            }
            set
            {
                this.stmNoteCollectionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesCurrency
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesAccChargeCode
    {

        private string codeField;

        private string pkField;

        private NativeRateRateEntryRateLinesAccChargeCodeGlbCompany glbCompanyField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRateLinesAccChargeCodeGlbCompany GlbCompany
        {
            get
            {
                return this.glbCompanyField;
            }
            set
            {
                this.glbCompanyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesAccChargeCodeGlbCompany
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesRateLineItems
    {

        private string pkField;

        private short lineOrderField;

        private bool lineOrderFieldSpecified;

        private string typeField;

        private decimal breakMinimumField;

        private bool breakMinimumFieldSpecified;

        private decimal breakField;

        private bool breakFieldSpecified;

        private string breakWeightVolumeField;

        private decimal valueField;

        private bool valueFieldSpecified;

        private decimal agentDeclaredRateField;

        private bool agentDeclaredRateFieldSpecified;

        private decimal flatAmountField;

        private bool flatAmountFieldSpecified;

        private string textField;

        private int unitMultipleField;

        private bool unitMultipleFieldSpecified;

        private bool callForPricingField;

        private bool callForPricingFieldSpecified;

        private NativeRateRateEntryRateLinesRateLineItemsPercentOf percentOfField;

        private NativeRateRateEntryRateLinesRateLineItemsDomesticZone domesticZoneField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public short LineOrder
        {
            get
            {
                return this.lineOrderField;
            }
            set
            {
                this.lineOrderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LineOrderSpecified
        {
            get
            {
                return this.lineOrderFieldSpecified;
            }
            set
            {
                this.lineOrderFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public decimal BreakMinimum
        {
            get
            {
                return this.breakMinimumField;
            }
            set
            {
                this.breakMinimumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BreakMinimumSpecified
        {
            get
            {
                return this.breakMinimumFieldSpecified;
            }
            set
            {
                this.breakMinimumFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Break
        {
            get
            {
                return this.breakField;
            }
            set
            {
                this.breakField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BreakSpecified
        {
            get
            {
                return this.breakFieldSpecified;
            }
            set
            {
                this.breakFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string BreakWeightVolume
        {
            get
            {
                return this.breakWeightVolumeField;
            }
            set
            {
                this.breakWeightVolumeField = value;
            }
        }

        /// <remarks/>
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValueSpecified
        {
            get
            {
                return this.valueFieldSpecified;
            }
            set
            {
                this.valueFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal AgentDeclaredRate
        {
            get
            {
                return this.agentDeclaredRateField;
            }
            set
            {
                this.agentDeclaredRateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgentDeclaredRateSpecified
        {
            get
            {
                return this.agentDeclaredRateFieldSpecified;
            }
            set
            {
                this.agentDeclaredRateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal FlatAmount
        {
            get
            {
                return this.flatAmountField;
            }
            set
            {
                this.flatAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FlatAmountSpecified
        {
            get
            {
                return this.flatAmountFieldSpecified;
            }
            set
            {
                this.flatAmountFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }

        /// <remarks/>
        public int UnitMultiple
        {
            get
            {
                return this.unitMultipleField;
            }
            set
            {
                this.unitMultipleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UnitMultipleSpecified
        {
            get
            {
                return this.unitMultipleFieldSpecified;
            }
            set
            {
                this.unitMultipleFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool CallForPricing
        {
            get
            {
                return this.callForPricingField;
            }
            set
            {
                this.callForPricingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CallForPricingSpecified
        {
            get
            {
                return this.callForPricingFieldSpecified;
            }
            set
            {
                this.callForPricingFieldSpecified = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRateLinesRateLineItemsPercentOf PercentOf
        {
            get
            {
                return this.percentOfField;
            }
            set
            {
                this.percentOfField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRateLinesRateLineItemsDomesticZone DomesticZone
        {
            get
            {
                return this.domesticZoneField;
            }
            set
            {
                this.domesticZoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesRateLineItemsPercentOf
    {

        private string codeField;

        private string pkField;

        private NativeRateRateEntryRateLinesRateLineItemsPercentOfGlbCompany glbCompanyField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRateLinesRateLineItemsPercentOfGlbCompany GlbCompany
        {
            get
            {
                return this.glbCompanyField;
            }
            set
            {
                this.glbCompanyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesRateLineItemsPercentOfGlbCompany
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesRateLineItemsDomesticZone
    {

        private string zoneNameField;

        private string pkField;

        private NativeRateRateEntryRateLinesRateLineItemsDomesticZoneRateTransportProvider rateTransportProviderField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string ZoneName
        {
            get
            {
                return this.zoneNameField;
            }
            set
            {
                this.zoneNameField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRateLinesRateLineItemsDomesticZoneRateTransportProvider RateTransportProvider
        {
            get
            {
                return this.rateTransportProviderField;
            }
            set
            {
                this.rateTransportProviderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesRateLineItemsDomesticZoneRateTransportProvider
    {

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesStmNote
    {

        private string pkField;

        private byte[] noteDataField;

        private string noteTextField;

        private string noteTypeField;

        private string noteContextField;

        private bool isCustomDescriptionField;

        private bool isCustomDescriptionFieldSpecified;

        private bool forceReadField;

        private bool forceReadFieldSpecified;

        private string descriptionField;

        private NativeRateRateEntryRateLinesStmNoteRelatedCompany relatedCompanyField;

        private Action actionField;

        private bool actionFieldSpecified;

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary")]
        public byte[] NoteData
        {
            get
            {
                return this.noteDataField;
            }
            set
            {
                this.noteDataField = value;
            }
        }

        /// <remarks/>
        public string NoteText
        {
            get
            {
                return this.noteTextField;
            }
            set
            {
                this.noteTextField = value;
            }
        }

        /// <remarks/>
        public string NoteType
        {
            get
            {
                return this.noteTypeField;
            }
            set
            {
                this.noteTypeField = value;
            }
        }

        /// <remarks/>
        public string NoteContext
        {
            get
            {
                return this.noteContextField;
            }
            set
            {
                this.noteContextField = value;
            }
        }

        /// <remarks/>
        public bool IsCustomDescription
        {
            get
            {
                return this.isCustomDescriptionField;
            }
            set
            {
                this.isCustomDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsCustomDescriptionSpecified
        {
            get
            {
                return this.isCustomDescriptionFieldSpecified;
            }
            set
            {
                this.isCustomDescriptionFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool ForceRead
        {
            get
            {
                return this.forceReadField;
            }
            set
            {
                this.forceReadField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ForceReadSpecified
        {
            get
            {
                return this.forceReadFieldSpecified;
            }
            set
            {
                this.forceReadFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public NativeRateRateEntryRateLinesStmNoteRelatedCompany RelatedCompany
        {
            get
            {
                return this.relatedCompanyField;
            }
            set
            {
                this.relatedCompanyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    public partial class NativeRateRateEntryRateLinesStmNoteRelatedCompany
    {

        private string codeField;

        private string pkField;

        private Action actionField;

        private bool actionFieldSpecified;

        private string tableNameField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string PK
        {
            get
            {
                return this.pkField;
            }
            set
            {
                this.pkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActionSpecified
        {
            get
            {
                return this.actionFieldSpecified;
            }
            set
            {
                this.actionFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableName
        {
            get
            {
                return this.tableNameField;
            }
            set
            {
                this.tableNameField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.cargowise.com/Schemas/Native/2011/11")]
    [System.Xml.Serialization.XmlRootAttribute("Rate", Namespace = "http://www.cargowise.com/Schemas/Native/2011/11", IsNullable = false)]
    public partial class RateData
    {

        private NativeRate ratingHeaderField;

        private string versionField;

        /// <remarks/>
        public NativeRate RatingHeader
        {
            get
            {
                return this.ratingHeaderField;
            }
            set
            {
                this.ratingHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "token")]
        public string version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }
}
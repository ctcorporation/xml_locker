﻿using CTCLogging;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using XMLLocker.Cargowise.XUS;

namespace XMLocker.Cargowise
{
    public class CWToCommon
    {
        #region members
        private Ivw_CustomerProfile _profile;
        private Shipment _shipment { get; set; }
        private Container[] _shipmentcontainerCollection;
        private Container[] _consolContainerCollection;

        #endregion

        #region properties
        public Shipment Consol { get; private set; }

        public string ErrorString { get; set; }
        public string LogPath { get; set; }
        public Ivw_CustomerProfile Profile
        {
            get { return _profile; }
            set { this._profile = value; }
        }
        #endregion

        #region constructors
        public CWToCommon()
        {

        }
        public CWToCommon(string path, Ivw_CustomerProfile profile)
        {
            LogPath = path;
            Profile = profile;
        }



        #endregion

        #region Methods

        public string GetKeyFromContext(DataContext context, string keyToFind)
        {
            var key = (from ds in context.DataSourceCollection
                       where ds.Type == keyToFind
                       select ds.Key).FirstOrDefault();
            return !string.IsNullOrEmpty(key) ? key : string.Empty;

        }


        public NodeFile CommonFromCW(UniversalInterchange cw)
        {
            NodeFile ctc = new NodeFile();
            NodeFileIdendtityMatrix id = new NodeFileIdendtityMatrix();
            id.SenderId = cw.Header.SenderID;
            id.CustomerId = cw.Header.RecipientID;
            id.DocumentType = "CommonXML";
            if (Profile.P_PARAMLIST != null)
            {
                var paramlist = Profile.P_PARAMLIST.Split('|').ToList();

                var doctype = GetParamFromProfile(paramlist, "DocType");
                if (doctype != null)
                {
                    id.DocumentType = doctype;
                }
            }

            {

            }

            id.FileDateTime = DateTime.Now.ToString();
            id.FileId = DateTime.Now.ToString("yyyyMMddhhmm");
           
            //NodeFileOperation op = new NodeFileOperation();
            DataContext dc = cw.Body.BodyField.Shipment.DataContext;
            List<NodeFileTrackingOrder> o = new List<NodeFileTrackingOrder>();
            List<NodeFileTrackingOrder> shipmentOrders = new List<NodeFileTrackingOrder>();
            List<NodeFileTimeSlotRequest> timeslots = new List<NodeFileTimeSlotRequest>();
            NodeFileTimeSlotRequest timeslot = new NodeFileTimeSlotRequest();
            id.EventCode = dc.EventType.Code;
            id.EventDescription = dc.EventType.Description;
            id.TriggerDate = dc.TriggerDate;

            foreach (DataSource source in dc.DataSourceCollection)
            {
                switch (source.Type)
                {
                    case "ForwardingConsol":
                        NodeFileMaster master = new NodeFileMaster();
                        master = GetMasterFromCW(cw);
                        if (master != null)
                        {

                            ctc.Master = master;
                        }
                        _consolContainerCollection = cw.Body.BodyField.Shipment.ContainerCollection != null ? cw.Body.BodyField.Shipment.ContainerCollection.Container : null;
                        break;

                    case "ForwardingShipment":
                        string consol = GetKeyFromContext(dc, "ForwardingConsol");
                        List<NodeFileShipment> shipments = new List<NodeFileShipment>();
                        if (string.IsNullOrEmpty(consol))
                        {
                            var nodeShipment = GetNodeShipmentFromCWShipment(cw.Body.BodyField.Shipment, source.Key, consol);
                            if (nodeShipment != null)
                            {
                                shipments.Add(nodeShipment);
                            }
                        }

                        if (cw.Body.BodyField.Shipment.SubShipmentCollection != null)
                        {
                            foreach (Shipment cwShipment in cw.Body.BodyField.Shipment.SubShipmentCollection)
                            {
                                //Add TimeSlot Requests here. 
                                var nodeShipment = GetNodeShipmentFromCWShipment(cwShipment, source.Key, consol);
                                if (nodeShipment != null)
                                {
                                    shipments.Add(nodeShipment);
                                }
                                _shipment = cwShipment;
                                _shipmentcontainerCollection = cwShipment.ContainerCollection != null ? cwShipment.ContainerCollection.Container : null;
                                timeslot = GetTimeSlotFromShipment(cwShipment);
                                if (timeslot != null)
                                {
                                    if (cw.Body.BodyField.Shipment.WayBillType.Code == "MWB")
                                    {
                                        timeslot.OceanBill = cw.Body.BodyField.Shipment.WayBillNumber;
                                    }
                                    var companies = timeslot.Companies.ToList();
                                    companies.Add(GetOrgFromShipment(cw.Body.BodyField.Shipment.OrganizationAddressCollection, "ReceivingForwarderAddress"));
                                    //check for DeliveryLocalCartage if same as Profile Sender and Change BillTo
                                    //Done in GetTimeSlotFromShipment
                                    //   var comp = GetOrgFromShipment(cw.Body.BodyField.Shipment.OrganizationAddressCollection, "DeliveryLocalCartage");
                                    //  if (comp != null)
                                    //   {
                                    //       if (_profile.P_RECIPIENTID == comp.CompanyOrgCode)
                                    //       {
                                    //           comp = GetOrgFromShipment(cw.Body.BodyField.Shipment.OrganizationAddressCollection, "ImporterDocumentaryAddress");
                                    //           comp.CompanyType = GetCompType("SendersLocalClient");
                                    //           companies.Add(comp);
                                    //       }
                                    //   }
                                    timeslot.Companies = companies.ToArray();
                                    timeslots.Add(timeslot);
                                }

                                //Add Tracking Orders here
                                o = GetTrackingOrdersFromShipment(cwShipment);
                                o = XMLLocker.CTC.NodeFunctions.AddtoOrderList(o, shipmentOrders);// This is to combine the orders from prev shipments into a single order.
                            }
                        }
                        ctc.TrackingOrders = o?.ToArray();
                        ctc.Shipments = shipments?.ToArray();
                        if (timeslots.Count > 0)
                        {
                            ctc.TimeSlotRequests = timeslots.ToArray();
                        }

                        break;

                    case "WarehouseOrder":
                        List<NodeFileWarehouseOrder> whOrders = new List<NodeFileWarehouseOrder>();
                        whOrders = GetWarehouseOrdersFromCW(cw.Body.BodyField.Shipment.Order, source.Key);
                        ctc.WarehouseOrders = whOrders != null ? whOrders.ToArray() : null;
                        break;
                    case "CustomsDeclaration":
                        _shipment = cw.Body.BodyField.Shipment;
                        timeslot = GetTimeSlotFromDeclaration(cw.Body.BodyField.Shipment);
                        if (timeslot != null)
                        {
                            switch (cw.Body.BodyField.Shipment.WayBillType.Code)
                            {
                                case "MWB":
                                    timeslot.OceanBill = cw.Body.BodyField.Shipment.WayBillNumber;
                                    break;

                                case "HWB":
                                    timeslot.HouseBill = cw.Body.BodyField.Shipment.WayBillNumber;
                                    break;
                            }


                            var companies = timeslot.Companies.ToList();
                            companies.Add(GetOrgFromShipment(cw.Body.BodyField.Shipment.OrganizationAddressCollection, "ReceivingForwarderAddress"));
                            timeslot.Companies = companies.ToArray();
                            timeslots.Add(timeslot);
                        }
                        ctc.TimeSlotRequests = timeslots.ToArray();
                        break;
                    case "OrderManagerOrder":
                        var order = GetOrderManagementOrderDetails(cw.Body.BodyField.Shipment, true);
                        List<NodeFileTrackingOrder> orders = new List<NodeFileTrackingOrder>();
                        orders.Add(order);
                        ctc.TrackingOrders = orders.ToArray();

                        break;

                    case "ForwardingBooking":
                        consol = GetKeyFromContext(dc, "ForwardingBooking");
                        _shipment = cw.Body.BodyField.Shipment;
                        List<NodeFileBooking> bookings = new List<NodeFileBooking>();

                        NodeFileBooking book = new NodeFileBooking();
                        List<CompanyElement> comps = new List<CompanyElement>();

                        book.BookingConfirmationReference = _shipment.BookingConfirmationReference;

                        if(_shipment.OrganizationAddressCollection != null)
                        {
                            CompanyElement comp = new CompanyElement();
                            foreach (var org in _shipment.OrganizationAddressCollection)
                            {
                                comp = new CompanyElement();
                                comp = GetOrgFromShipment(_shipment.OrganizationAddressCollection, org.AddressType);

                                comps.Add(comp);
                            }

                            book.Companies = comps.ToArray();
                        }

                        if(_shipment.LocalProcessing != null)
                        {
                            book.DeliveryRequiredBy = _shipment.LocalProcessing.DeliveryRequiredBy;
                            
                            if(_shipment.LocalProcessing.OrderNumberCollection != null)
                            {
                                foreach(var ordNum in _shipment.LocalProcessing.OrderNumberCollection)
                                {
                                    book.OrderNumber = ordNum.OrderReference;
                                }
                            }
                        }

                        List<OrderLineElement> lines = new List<OrderLineElement>();
                        OrderLineElement line = new OrderLineElement();
                        foreach(var pack in _shipment.PackingLineCollection)
                        {
                            line = new OrderLineElement();
                            line.DocEntry = _shipment.BookingConfirmationReference;
                            line.PackageQty = pack.PackQty;
                            line.Product = new ProductElement
                            {
                                Code = pack.PackedItemCollection != null ? pack.PackedItemCollection[0].Product.Code : ""
                            };

                            lines.Add(line);
                        }
                        book.PackingLines = lines.ToArray();

                        bookings.Add(book);
                        ctc.Bookings = bookings.ToArray();
                        break;
                    case "LocalTransport":
                        NodeFileLocalTransport trans = new NodeFileLocalTransport();
                        
                        trans.JobNumber = source.Key;
                        trans.WayBillNumber = cw.Body.BodyField.Shipment.WayBillNumber;
                        ctc.LocalTransport = trans;
                        break;

                }
            }

            ctc.IdendtityMatrix = id;
            return ctc;
        }
        public NodeFile ConvertData(string xmlfile, Ivw_CustomerProfile profile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            UniversalInterchange cwFile = new UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                cwFile = (UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            return CommonFromCW(cwFile);
        }

        public NodeFile ConvertDataShipment(string xmlfile, Ivw_CustomerProfile profile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            UniversalInterchange cwFile = new UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                cwFile = (UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            return CommonFromCWShipment(cwFile);
        }

        public NodeFile CommonFromCWShipment(UniversalInterchange cw)
        {

            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetShipmentFromCWShipment";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            NodeFile ctc = new NodeFile();
            NodeFileIdendtityMatrix id = new NodeFileIdendtityMatrix();
            id.SenderId = cw.Header.SenderID;
            id.CustomerId = cw.Header.RecipientID;
            id.DocumentType = "CommonXML";
            if (Profile.P_PARAMLIST != null)
            {
                var paramlist = Profile.P_PARAMLIST.Split('|').ToList();

                var doctype = GetParamFromProfile(paramlist, "DocType");
                if (doctype != null)
                {
                    id.DocumentType = doctype;
                }
            }

            {

            }

            id.FileDateTime = DateTime.Now.ToString();
            id.FileId = DateTime.Now.ToString("yyyyMMddhhmm");
            //NodeFileOperation op = new NodeFileOperation();
            DataContext dc = cw.Body.BodyField.Shipment.DataContext;
            List<NodeFileTrackingOrder> o = new List<NodeFileTrackingOrder>();
            List<NodeFileTrackingOrder> shipmentOrders = new List<NodeFileTrackingOrder>();
            List<NodeFileTimeSlotRequest> timeslots = new List<NodeFileTimeSlotRequest>();
            NodeFileTimeSlotRequest timeslot = new NodeFileTimeSlotRequest();

            id.EventCode = dc.EventType.Code;
            id.EventDescription = dc.EventType.Description;
            id.TriggerDate = dc.TriggerDate;

            try
            {

                foreach (DataSource source in dc.DataSourceCollection)
                {
                    switch (source.Type)
                    {
                        case "ForwardingConsol":
                            string consol = GetKeyFromContext(dc, "ForwardingShipment");
                            List<NodeFileShipment> shipments = new List<NodeFileShipment>();

                            if (cw.Body.BodyField.Shipment.SubShipmentCollection != null)
                            {
                                foreach (Shipment cwShipment in cw.Body.BodyField.Shipment.SubShipmentCollection)
                                {
                                    if (string.IsNullOrEmpty(consol))
                                    {
                                        var nodeShipment = GetNodeShipmentFromCWShipment(cwShipment, source.Key, consol);
                                        if (nodeShipment.Routes == null)
                                        {
                                            nodeShipment.Routes = GetLocationsFromShipment(cw.Body.BodyField.Shipment.TransportLegCollection).ToArray();
                                        }
                                        if (nodeShipment != null)
                                        {
                                            shipments.Add(nodeShipment);
                                        }
                                    }

                                }
                            }
                            ctc.Shipments = shipments.ToArray();
                            break;
                        case "ForwardingShipment":
                            consol = GetKeyFromContext(dc, "ForwardingConsol");
                            shipments = new List<NodeFileShipment>();
                            if (string.IsNullOrEmpty(consol))
                            {
                                var nodeShipment = GetNodeShipmentFromCWShipment(cw.Body.BodyField.Shipment, source.Key, consol);
                                if (nodeShipment != null)
                                {
                                    shipments.Add(nodeShipment);
                                }
                            }

                            if (cw.Body.BodyField.Shipment.SubShipmentCollection != null)
                            {
                                foreach (Shipment cwShipment in cw.Body.BodyField.Shipment.SubShipmentCollection)
                                {
                                    //Add TimeSlot Requests here. 
                                    var nodeShipment = GetNodeShipmentFromCWShipment(cwShipment, source.Key, consol);
                                    if (nodeShipment != null)
                                    {
                                        shipments.Add(nodeShipment);
                                    }
                                    _shipment = cwShipment;
                                    _shipmentcontainerCollection = cwShipment.ContainerCollection != null ? cwShipment.ContainerCollection.Container : null;
                                    timeslot = GetTimeSlotFromShipment(cwShipment);
                                    if (timeslot != null)
                                    {
                                        if (cw.Body.BodyField.Shipment.WayBillType.Code == "MWB")
                                        {
                                            timeslot.OceanBill = cw.Body.BodyField.Shipment.WayBillNumber;
                                        }
                                        var companies = timeslot.Companies.ToList();
                                        companies.Add(GetOrgFromShipment(cw.Body.BodyField.Shipment.OrganizationAddressCollection, "ReceivingForwarderAddress"));
                                        //check for DeliveryLocalCartage if same as Profile Sender and Change BillTo
                                        //Done in GetTimeSlotFromShipment
                                        //   var comp = GetOrgFromShipment(cw.Body.BodyField.Shipment.OrganizationAddressCollection, "DeliveryLocalCartage");
                                        //  if (comp != null)
                                        //   {
                                        //       if (_profile.P_RECIPIENTID == comp.CompanyOrgCode)
                                        //       {
                                        //           comp = GetOrgFromShipment(cw.Body.BodyField.Shipment.OrganizationAddressCollection, "ImporterDocumentaryAddress");
                                        //           comp.CompanyType = GetCompType("SendersLocalClient");
                                        //           companies.Add(comp);
                                        //       }
                                        //   }
                                        timeslot.Companies = companies.ToArray();
                                        timeslots.Add(timeslot);
                                    }

                                    //Add Tracking Orders here
                                    
                                    o = GetTrackingOrdersFromShipment(cwShipment);
                                    
                                    foreach(var ord in o)
                                    {
                                        ord.ShipmentNo = source.Key;
                                        
                                    }
                                    o = XMLLocker.CTC.NodeFunctions.AddtoOrderList(o, shipmentOrders);// This is to combine the orders from prev shipments into a single order.
                                }
                            }
                            ctc.TrackingOrders = o?.ToArray();
                            ctc.Shipments = shipments?.ToArray();
                            if (timeslots.Count > 0)
                            {
                                ctc.TimeSlotRequests = timeslots.ToArray();
                            }

                            break;

                        case "WarehouseOrder":
                            List<NodeFileWarehouseOrder> whOrders = new List<NodeFileWarehouseOrder>();
                            whOrders = GetWarehouseOrdersFromCW(cw.Body.BodyField.Shipment.Order, source.Key);
                            ctc.WarehouseOrders = whOrders != null ? whOrders.ToArray() : null;
                            break;
                        case "CustomsDeclaration":
                            _shipment = cw.Body.BodyField.Shipment;
                            timeslot = GetTimeSlotFromDeclaration(cw.Body.BodyField.Shipment);
                            if (timeslot != null)
                            {
                                switch (cw.Body.BodyField.Shipment.WayBillType.Code)
                                {
                                    case "MWB":
                                        timeslot.OceanBill = cw.Body.BodyField.Shipment.WayBillNumber;
                                        break;

                                    case "HWB":
                                        timeslot.HouseBill = cw.Body.BodyField.Shipment.WayBillNumber;
                                        break;
                                }


                                var companies = timeslot.Companies.ToList();
                                companies.Add(GetOrgFromShipment(cw.Body.BodyField.Shipment.OrganizationAddressCollection, "ReceivingForwarderAddress"));
                                timeslot.Companies = companies.ToArray();
                                timeslots.Add(timeslot);
                            }
                            ctc.TimeSlotRequests = timeslots.ToArray();
                            break;
                        case "OrderManagerOrder":
                            var order = GetOrderManagementOrderDetails(cw.Body.BodyField.Shipment, true);
                            List<NodeFileTrackingOrder> orders = new List<NodeFileTrackingOrder>();
                            orders.Add(order);
                            ctc.TrackingOrders = orders.ToArray();

                            break;
                    }
                }
            }

            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Shipment Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }

                return null;
            }


            ctc.IdendtityMatrix = id;
            return ctc;
        }

        private NodeFileMaster GetMasterFromCWShipment(UniversalShipmentData cw)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetMasterFromCW";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;

            NodeFileMaster master = new NodeFileMaster();
            try
            {
                Consol = cw.Shipment;
                master.ConsolNo = GetKeyFromContext(cw.Shipment.DataContext, "ForwardingConsol");
                master.Dates = GetDatesFromShipment(Consol.DateCollection);
                master.Companies = GetCompaniesFromShipment(Consol.OrganizationAddressCollection);
                master.Containers = GetContainersFromShipment(Consol.ContainerCollection, Consol.LocalProcessing);
                master.FlightVoyage = Consol.VoyageFlightNo;
                NodeFileMasterMasterType contMode;
                if (Enum.TryParse(Consol.ContainerMode.Code, out contMode))
                {
                    master.MasterType = contMode;
                }

                List<LocationElement> locations = new List<LocationElement>();
                LocationElement loc = new LocationElement
                {
                    Code = Consol.PortOfDischarge.Code,
                    Name = Consol.PortOfDischarge.Name,
                    Type = "PortOfDischarge"
                };
                locations.Add(loc);
                loc = new LocationElement
                {
                    Code = Consol.PortOfLoading.Code,
                    Name = Consol.PortOfLoading.Name,
                    Type = "PortOfLoading"
                };
                locations.Add(loc);
                master.Locations = locations.ToArray();
                master.Vessel = Consol.VesselName;
                switch (Consol.TransportMode.Code.ToUpper())
                {
                    case "SEA":
                        master.TransportMode = NodeFileMasterTransportMode.SEA;
                        break;
                    case "AIR":
                        master.TransportMode = NodeFileMasterTransportMode.AIR;
                        break;
                }
                master.TransportModeSpecified = true;
                master.PrePaid = Consol.PaymentMethod.Code;

                if (Consol.WayBillType.Code == "MWB")
                {
                    master.MasterBillNo = Consol.WayBillNumber;
                }
                master.Milestones = Consol.MilestoneCollection == null ? null : GetMileStonesFromCW(Consol.MilestoneCollection, master.ConsolNo);
            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Master Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }

            return master;
        }

        public NodeFileTimeSlotRequest GetTimeSlotFromDeclaration(Shipment shipment)
        {
            string lo = LogPath;
            string cp = string.Empty;
            MethodBase m = MethodBase.GetCurrentMethod();
            string fu = m.Name;
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;

            try
            {
                //if (shipment.LocalProcessing != null)
                //{
                    NodeFileTimeSlotRequest ts = new NodeFileTimeSlotRequest();
                    List<CompanyElement> companies = new List<CompanyElement>();
                    ts.ContainerMode = shipment.CustomsContainerMode != null ? shipment.CustomsContainerMode.Code : string.Empty;

                    if (shipment.PortOfDischarge.Code.Substring(0, 2) == "AU")
                    {
                        ts.Direction = NodeFileTimeSlotRequestDirection.I;
                    }
                    else
                    {
                        ts.Direction = NodeFileTimeSlotRequestDirection.E;
                    }
                    ts.DirectionSpecified = true;
                    ts.DischargePort = !string.IsNullOrEmpty(shipment.PortOfDischarge.Code) ? shipment.PortOfDischarge.Code : null;
                    if (shipment.WayBillType != null)
                    {
                        if (shipment.WayBillType.Code == "HWB")
                        {
                            ts.HouseBill = shipment.WayBillNumber;
                        }

                    if (shipment.WayBillType.Code == "MWB")
                    {
                        ts.MasterBill = shipment.WayBillNumber;
                    }
                }
                    ts.Vessel = shipment.VesselName;
                    ts.VesselNo = shipment.LloydsIMO;
                    ts.Voyage = shipment.VoyageFlightNo;
                    ts.ShipmentTypeSpecified = true;
                    ts.TotalWeight = shipment.TotalWeight;
                    ts.WeightUnit = shipment.TotalWeightUnit.Code.ToString();
                    ts.TotalVolume = shipment.TotalVolume;
                    ts.VolumeUnit = shipment.TotalVolumeUnit.Code.ToString();


                ts.TransportType = shipment.TransportMode.Code;
                    ts.ShipmentNo = GetKeyValue(shipment.DataContext.DataSourceCollection, "CustomsDeclaration");
                    ts.Routes = GetLocationsFromShipment(shipment.TransportLegCollection).ToArray();
                    ts.ModeOfTransport = GetModeOfTransport(shipment.TransportMode.Code);
                    var reference = shipment.CustomizedFieldCollection != null ? GetCustomFieldsFromShipments(shipment.CustomizedFieldCollection, "Ref").FirstOrDefault() : null;
                    if (reference == null)
                    {
                        ts.CustomerReference = new CustomerReferenceElement
                        {
                            RefValue = ts.ShipmentNo
                        };
                    }
                    else
                    {
                        ts.CustomerReference = reference;
                    }

                    switch (ts.ContainerMode)
                    {
                        case "FCL":
                            ts.ShipmentType = NodeFileTimeSlotRequestShipmentType.FCL;
                            break;
                        case "LCL":
                            ts.ShipmentType = NodeFileTimeSlotRequestShipmentType.LCL;
                            break;

                    }

                if(shipment.SubShipmentCollection != null)
                {
                    companies.Add(GetOrgFromShipment(shipment.SubShipmentCollection[0].OrganizationAddressCollection, "ImporterPickupDeliveryAddress"));//Delivery
                    companies.Add(GetOrgFromShipment(shipment.SubShipmentCollection[0].OrganizationAddressCollection, "DeliveryLocalCartage"));//Cartage
                    companies.Add(GetOrgFromShipment(shipment.SubShipmentCollection[0].OrganizationAddressCollection, "ImporterDocumentaryAddress"));//Consignee/Importer
                    companies.Add(GetOrgFromShipment(shipment.SubShipmentCollection[0].OrganizationAddressCollection, "SupplierDocumentaryAddress"));//Supplier

                    if (shipment.SubShipmentCollection[0].ShipmentIncoTerm != null)
                    {
                        ts.IncoTerms = shipment.SubShipmentCollection[0].ShipmentIncoTerm.Code;
                    }
                }
                else
                {
                    companies.Add(GetOrgFromShipment(shipment.OrganizationAddressCollection, "ImporterPickupDeliveryAddress"));//Delivery
                    companies.Add(GetOrgFromShipment(shipment.OrganizationAddressCollection, "DeliveryLocalCartage"));//Cartage
                    companies.Add(GetOrgFromShipment(shipment.OrganizationAddressCollection, "ImporterDocumentaryAddress"));//Consignee/Importer
                    companies.Add(GetOrgFromShipment(shipment.OrganizationAddressCollection, "SupplierDocumentaryAddress"));//Supplier
                }

                    
                                                                                                                        //check for DeliveryLocalCartage and ImportBroker ( broker does not work hardcode PAUSUSSYD) if same as Profile Sender and Change BillTo
                                                                                                                        // need to add on profile to check FT bitto is needed
                var comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "DeliveryLocalCartage");
                        if (comp != null)
                        {
                            if (_profile.P_RECIPIENTID == comp.CompanyOrgCode && _profile.P_RECIPIENTID == "PAUCUSSYD")
                            {
                                comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "ImporterDocumentaryAddress");
                                if (comp is null)
                                {
                                    comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "SendersLocalClient");
                                }
                                if (comp != null)
                                {
                                    comp.CompanyType = GetCompType("SendersLocalClient");
                                    companies.Add(comp);
                                }
                            }
                        }

                    ts.Companies = companies.ToArray();
                    if (shipment.LocalProcessing != null)
                    {
                        if(shipment.LocalProcessing.FCLDeliveryEquipmentNeeded != null)
                        {
                            List<DateElement> dates = new List<DateElement>();
                            dates.Add(new DateElement { DateType = DateElementDateType.DeliverBy, EstimateDate = shipment.LocalProcessing.DeliveryRequiredBy });
                            ts.Dates = dates.ToArray();
                        }
                        
                    }
                    if (shipment.ContainerCollection == null)
                    {
                        ErrorString += "Unable to create TimeSlot. There are no containers in the Shipment.";
                        return null;
                    }
                    ts.Containers = shipment.ContainerCollection != null ? GetContainersFromShipment(shipment.ContainerCollection, shipment.LocalProcessing) : null;
                    if (shipment.PackingLineCollection != null)
                    {
                        var packDetails = (from l in shipment.PackingLineCollection
                                           group l by new { l.Link, l.PackType.Code } into packingLineLink
                                           select new
                                           {
                                               Link = packingLineLink.Key.Link,
                                               PackType = packingLineLink.Key.Code,
                                               Pieces = packingLineLink.Sum(x => x.PackQty),
                                               Weight = packingLineLink.Sum(x => x.Weight)
                                           }).FirstOrDefault();
                        if (packDetails != null)
                        {
                            ts.Pieces = packDetails.Pieces;
                            ts.PiecesSpecified = true;
                            ts.Weight = packDetails.Weight;
                            ts.WeightSpecified = true;
                            ts.UnitOfMeasure = packDetails.PackType;
                        }
                    }
                    return ts;
                //}
            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Shipment Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }

            return null;
        }

        public NodeFileTimeSlotRequest GetTimeSlotFromShipment(Shipment shipment)
        {
            string lo = LogPath;
            string cp = string.Empty;
            MethodBase m = MethodBase.GetCurrentMethod();
            string fu = m.Name;
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            try
            {
                if (shipment.LocalProcessing != null)
                {
                    NodeFileTimeSlotRequest ts = new NodeFileTimeSlotRequest();
                    List<CompanyElement> companies = new List<CompanyElement>();
                    ts.ContainerMode = shipment.ContainerMode.Code;
                    ts.ModeOfTransport = GetModeOfTransport(shipment.TransportMode.Code);


                    if (shipment.PortOfDischarge.Code.Substring(0, 2) == "AU")
                    {
                        ts.Direction = NodeFileTimeSlotRequestDirection.I;
                        var comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "ConsigneePickupDeliveryAddress");
                        if (comp != null)
                            companies.Add(comp);
                        comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "DeliveryLocalCartage");
                        if (comp != null)
                            companies.Add(comp);
                        comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "ConsigneeDocumentaryAddress");
                        if (comp != null)
                            companies.Add(comp);
                        //check for DeliveryLocalCartage and ImportBroker ( broker does not work hardcode PAUSUSSYD) if same as Profile Sender and Change BillTo
                        // need to add on profile to check FT bitto is needed
                        comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "DeliveryLocalCartage");
                        //var comp2 = GetOrgFromShipment(shipment.OrganizationAddressCollection, "ImportBroker");
                        if (comp != null)
                        {
                            if (_profile.P_RECIPIENTID == comp.CompanyOrgCode && _profile.P_RECIPIENTID == "PAUCUSSYD")
                            {
                                comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "ImporterDocumentaryAddress");
                                if (comp is null)
                                {
                                    comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "SendersLocalClient");
                                }
                                if (comp != null)
                                {
                                    comp.CompanyType = GetCompType("SendersLocalClient");
                                    companies.Add(comp);
                                }
                            }
                        }
                    }
                    else
                    {
                        ts.Direction = NodeFileTimeSlotRequestDirection.E;
                        var comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "CustomsContainerTerminalOperatorAddress");
                        if (comp != null)
                            companies.Add(comp);
                        comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "ConsignorDocumentaryAddress");
                        if (comp != null)
                            companies.Add(comp); //Consignor
                        comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "ConsignorPickupDeliveryAddress");
                        if (comp != null)
                            companies.Add(comp);//Pickup
                        comp = GetOrgFromShipment(shipment.OrganizationAddressCollection, "PickupLocalCartage");
                        if (comp != null)
                            companies.Add(comp);//Cartage
                    }
                    ts.DirectionSpecified = true;
                    ts.DischargePort = !string.IsNullOrEmpty(shipment.PortOfDischarge.Code) ? shipment.PortOfDischarge.Code : null;
                    if (shipment.WayBillType.Code == "HWB")
                    {
                        ts.HouseBill = shipment.WayBillNumber;
                    }
                    ts.Vessel = string.IsNullOrEmpty(shipment.VesselName) ? string.Empty : shipment.VesselName;
                    ts.VesselNo = string.IsNullOrEmpty(shipment.LloydsIMO) ? string.Empty : shipment.LloydsIMO;
                    ts.Voyage = shipment.VoyageFlightNo;
                    ts.ShipmentTypeSpecified = true;
                    ts.ShipmentNo = GetKeyValue(shipment.DataContext.DataSourceCollection, "ForwardingShipment");
                    var reference = GetCustomFieldsFromShipments(shipment.CustomizedFieldCollection, "Ref").FirstOrDefault();
                    if (reference == null)
                    {
                        ts.CustomerReference = new CustomerReferenceElement
                        {
                            RefValue = ts.ShipmentNo
                        };
                    }
                    else
                    {
                        ts.CustomerReference = reference;
                    }
                    switch (shipment.ContainerMode.Code)
                    {
                        case "FCL":
                            ts.ShipmentType = NodeFileTimeSlotRequestShipmentType.FCL;

                            break;
                        case "LCL":
                            ts.ShipmentType = NodeFileTimeSlotRequestShipmentType.LCL;
                            break;
                        case "LSE":
                            ts.ShipmentType = NodeFileTimeSlotRequestShipmentType.LSE;
                            break;
                    }


                    ts.Companies = companies.ToArray();
                    if (shipment.LocalProcessing.FCLDeliveryEquipmentNeeded != null)
                    {
                        List<DateElement> dates = new List<DateElement>();
                        dates.Add(new DateElement { DateType = DateElementDateType.DeliverBy, EstimateDate = shipment.LocalProcessing.DeliveryRequiredBy });
                        ts.Dates = dates.ToArray();
                    }
                    if (ts.ModeOfTransport != NodeFileTimeSlotRequestModeOfTransport.A)
                    {
                        ts.Containers = shipment.ContainerCollection != null ? GetContainersFromShipment(shipment.ContainerCollection, shipment.LocalProcessing) : null;
                    }

                    if (shipment.PackingLineCollection != null)
                    {
                        var packDetails = (from l in shipment.PackingLineCollection
                                           group l by new { l.Link, l.PackType.Code } into packingLineLink
                                           select new
                                           {
                                               Link = packingLineLink.Key.Link,
                                               PackType = packingLineLink.Key.Code,
                                               Pieces = packingLineLink.Sum(x => x.PackQty),
                                               Weight = packingLineLink.Sum(x => x.Weight)
                                           }).FirstOrDefault();
                        if (packDetails != null)
                        {
                            ts.Pieces = packDetails.Pieces;
                            ts.PiecesSpecified = true;
                            ts.Weight = packDetails.Weight;
                            ts.WeightSpecified = true;
                            ts.UnitOfMeasure = packDetails.PackType;
                        }
                    }
                    return ts;
                }
            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get TimeSlot Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }

            return null;



        }

        private NodeFileTimeSlotRequestModeOfTransport GetModeOfTransport(string code)
        {
            NodeFileTimeSlotRequestModeOfTransport result = new NodeFileTimeSlotRequestModeOfTransport();
            switch (code)
            {
                case "SEA":
                    result = NodeFileTimeSlotRequestModeOfTransport.S;
                    break;
                case "AIR":
                    result = NodeFileTimeSlotRequestModeOfTransport.A;

                    break;
                case "ROA":
                    result = NodeFileTimeSlotRequestModeOfTransport.R;

                    break;
                case "RAI":
                    result = NodeFileTimeSlotRequestModeOfTransport.T;
                    break;

            }
            return result;
        }

        private ContainerElementEquipmentNeeded GetFCLEquipFromLocalProcessing(ShipmentLocalProcessing localProcessing)
        {
            if (localProcessing != null)
            {
                if (localProcessing.FCLDeliveryEquipmentNeeded != null)
                {

                    return Extensions.ToEnum<ContainerElementEquipmentNeeded>(localProcessing.FCLDeliveryEquipmentNeeded.Code, ContainerElementEquipmentNeeded.ASK);
                }
            }
            return ContainerElementEquipmentNeeded.ASK;
        }

        private CompanyElement GetOrgFromShipment(OrganizationAddress[] org, string orgType)
        {
            var comp = (from x in org
                        where x.AddressType == orgType
                        select new CompanyElement
                        {
                            CompanyName = x.CompanyName,
                            CompanyOrgCode = x.OrganizationCode,
                            CompanyCode = x.AddressShortCode,
                            Address1 = x.Address1,
                            Address2 = x.Address2,
                            City = x.City,
                            State = x.State,
                            PostCode = x.Postcode,
                            Country = x.Country != null ? x.Country.Name : string.Empty,
                            CountryCode = x.Country != null ? x.Country.Code : string.Empty,
                            EmailAddress = x.Email,
                            PhoneNo = x.Phone,
                            CompanyType = GetCompType(orgType)
                        }).FirstOrDefault();
            return comp;
        }

        private CompanyElementCompanyType GetCompType(string orgType)
        {
            CompanyElementCompanyType result = new CompanyElementCompanyType();
            switch (orgType)
            {
                case "ConsigneeDocumentaryAddress":
                    result = CompanyElementCompanyType.Consignee;
                    break;
                case "ImporterDocumentaryAddress":
                    result = CompanyElementCompanyType.Buyer;
                    break;
                case "ConsignorDocumentaryAddress":
                    result = CompanyElementCompanyType.Consignor;
                    break;
                case "SupplierDocumentaryAddress":
                    result = CompanyElementCompanyType.Supplier;
                    break;
                case "ConsigneePickupDeliveryAddress":
                    result = CompanyElementCompanyType.DeliveryAddress;
                    break;
                case "ImporterPickupDeliveryAddress":
                    result = CompanyElementCompanyType.DeliveryAddress;
                    break;
                case "DeliveryLocalCartage":
                    result = CompanyElementCompanyType.TransportCompany;
                    break;
                case "PickupLocalCartage":
                    result = CompanyElementCompanyType.TransportCompany;
                    break;
                case "ConsignorPickupDeliveryAddress":
                    result = CompanyElementCompanyType.PickupAddress;
                    break;
                case "SendingForwarderAddress":
                    result = CompanyElementCompanyType.ForwardingAgent;
                    break;
                case "ImportBroker":
                    result = CompanyElementCompanyType.ImportBroker;
                    break;
                case "ShippingLineAddress":
                    result = CompanyElementCompanyType.ShippingLine;
                    break;
                case "ShippingLine":
                    result = CompanyElementCompanyType.ShippingLine;
                    break;
                case "ReceivingForwarderAddress":
                    result = CompanyElementCompanyType.ReceivingAgent;
                    break;
                case "SendersLocalClient":
                    result = CompanyElementCompanyType.BillToAddress;
                    break;
                case "CustomsContainerTerminalOperatorAddress:":
                    result = CompanyElementCompanyType.CTO;
                    break;
            }
            return result;
        }

        private List<CustomerReferenceElement> GetCustomFieldsFromShipments(CustomizedField[] customizedFieldCollection, string value)
        {
            List<CustomerReferenceElement> res = new List<CustomerReferenceElement>();

            if (customizedFieldCollection != null)
            {

                if (string.IsNullOrEmpty(value))
                {
                    var result = (from x in customizedFieldCollection
                                  select x).ToList();
                    if (result.Count > 0)
                    {

                        foreach (var c in result)
                        {
                            CustomerReferenceElement custRef = new CustomerReferenceElement
                            {
                                RefType = c.Key,
                                RefValue = c.Value
                            };
                            res.Add(custRef);
                        }
                    }
                }
                else
                {
                    var result = (from x in customizedFieldCollection
                                  where x.Key.Contains(value)
                                  select x).ToList();
                    if (result.Count > 0)
                    {

                        foreach (var c in result)
                        {
                            CustomerReferenceElement custRef = new CustomerReferenceElement
                            {
                                RefType = c.Key,
                                RefValue = c.Value
                            };
                            res.Add(custRef);
                        }
                    }
                }


            }
            return res;
        }



        private NodeFileTrackingOrder GetOrderManagementOrderDetails(Shipment rs, bool parentLevel)
        {
            // Parent Level is used when the Order is sent from the Order Management Screen. 
            // When true we will use the Order Manager Order as the parent Shipment. 
            // When false this is being called from within the Related Shipments and therefore will need to get the Parent Shipment Values. 
            if (parentLevel)
            {
                _shipment = rs;
            }

            NodeFileTrackingOrder order = new NodeFileTrackingOrder
            {
                OrderNo = rs.Order.OrderNumber,
                OrderStatus = rs.Order.Status == null ? null : rs.Order.Status.Code,
                ShipmentNo = GetKeyFromContext(rs.DataContext, "ForwardingShipment"),
                Companies = rs.OrganizationAddressCollection == null ? null : GetCompaniesFromShipment(rs.OrganizationAddressCollection),
                OrderLines = rs.Order.OrderLineCollection == null ? null : GetOrderLinesFromOrders(rs.Order.OrderLineCollection),
                Containers = rs.ContainerCollection == null ? null : GetContainersFromShipment(rs.ContainerCollection, rs.LocalProcessing),
                TotalVolume = rs.TotalVolume,
                VolumeUnit = rs.TotalVolumeUnit == null ? null : rs.TotalVolumeUnit.Code,
                TotalWeight = rs.TotalWeight,
                WeightUnit = rs.TotalWeightUnit == null ? null : rs.TotalWeightUnit.Code,
                HouseBill = rs.WayBillType != null ? rs.WayBillType.Code == "HWB" ? rs.WayBillNumber != null ? rs.WayBillNumber : null : null : null,
                PiecesUnit = rs.OuterPacksPackageType == null ? null : rs.OuterPacksPackageType.Code,
                TransportMode = rs.TransportMode == null ? null : rs.TransportMode.Code,
                ContainerMode = rs.ContainerMode == null ? null : rs.ContainerMode.Code,
                Currency = rs.FreightRateCurrency == null ? null : rs.FreightRateCurrency.Code,
                IncoTerms = rs.ShipmentIncoTerm == null ? null : rs.ShipmentIncoTerm.Code,
                TotalPieces = rs.OuterPacks,
                Milestones = rs.MilestoneCollection == null ? null : GetMileStonesFromCW(rs.MilestoneCollection, rs.Order.OrderNumber)


            };

            var dates = rs.Order.DateCollection == null ? null : GetDatesFromShipment(rs.Order.DateCollection).ToList();
            
            
            List<DateElement> orderDates = new List<DateElement>();
            if (dates != null)
            {
                orderDates.AddRange(dates);
            }

            if (_shipment != null)
            {
                var shipdates = _shipment.DateCollection == null ? null : GetDatesFromShipment(_shipment.DateCollection).ToList();
                if (shipdates != null)
                {
                    orderDates.AddRange(shipdates);
                }
            }

            //    orderDates.Add(GetMileStone(rs.MilestoneCollection, "ARV", DateElementDateType.Arrival));
            order.Dates = orderDates.ToArray();


            if (rs.AdditionalBillCollection != null)
            {
                var mwb = (from m in rs.AdditionalBillCollection
                           where m.BillType.Code == "MWB"
                           select m.BillNumber).FirstOrDefault();
                order.MasterBillNo = mwb;
            }

            if (rs.Order.ClientReference != null)
            {

                List<CustomerReferenceElement> custref = new List<CustomerReferenceElement>();
                custref.Add(new CustomerReferenceElement { RefType = "Customer Reference", RefValue = rs.Order.ClientReference });
                order.OrderReferences = custref.ToArray();
            }
            if (rs.CustomizedFieldCollection != null)
            {
                // setting the Reference to find as null will pull all customised fields into the file.
                order.CustomerReferences = GetCustomFieldsFromShipments(rs.CustomizedFieldCollection, string.Empty).ToArray();
            }
            if (rs.CommercialInfo != null)
            {
                if (rs.CommercialInfo.CommercialInvoiceCollection != null)
                {
                    var invno = (from c in rs.CommercialInfo.CommercialInvoiceCollection
                                 select c.InvoiceNumber).FirstOrDefault();
                    order.InvoiceNo = invno == null ? null : invno;
                }
            }
            return order;
        }

        private MilestoneElement[] GetMileStonesFromCW(ShipmentMilestone[] milestoneCollection, string context)
        {
            List<MilestoneElement> milestones = new List<MilestoneElement>();
            foreach (var ms in milestoneCollection)
            {
                MilestoneElement milestone = new MilestoneElement
                {
                    Sequence = ms.Sequence,
                    Code = ms.EventCode,
                    EventDescription = ms.Description,
                    MilestoneDate = new DateElement
                    {
                        DateType = DateElementDateType.Triggered,
                        ActualDate = ms.ActualDate,
                        EstimateDate = ms.EstimatedDate
                    },
                    MilestoneType = ms.ConditionType,
                    MilestoneData = ms.ConditionReference,
                    MilestoneContext = context

                };
                milestones.Add(milestone);
            }
            return milestones.ToArray();
        }

        private DateElement GetMileStone(ShipmentMilestone[] milestoneCollection, string milestone, DateElementDateType dateType)
        {
            var msFind = (from x in milestoneCollection
                          where x.EventCode == milestone
                          select x).FirstOrDefault();
            if (msFind != null)
            {
                if (msFind.ActualDate != null)
                {
                    return new DateElement
                    {
                        ActualDate = msFind.ActualDate,
                        DateType = dateType
                    };
                }
                if (msFind.EstimatedDate != null)
                {
                    return new DateElement
                    {
                        EstimateDate = msFind.ActualDate,
                        DateType = dateType
                    };
                }
            }
            return null;
        }

        private List<NodeFileTrackingOrder> GetTrackingOrdersFromShipment(Shipment shipment)
        {
            List<NodeFileTrackingOrder> orders = new List<NodeFileTrackingOrder>();
            if (shipment.RelatedShipmentCollection != null)
            {
                foreach (Shipment rs in shipment.RelatedShipmentCollection)
                {
                    NodeFileTrackingOrder order = new NodeFileTrackingOrder();
                    _shipmentcontainerCollection = shipment.ContainerCollection != null ? shipment.ContainerCollection.Container : null;
                    if (rs.DataContext.DataSourceCollection != null)
                    {
                        var ds = (from d in rs.DataContext.DataSourceCollection
                                  where d.Type == "OrderManagerOrder"
                                  select d.Key).FirstOrDefault();
                        if (ds != null)
                        {
                            order = GetOrderManagementOrderDetails(rs, false);
                            var sno = (from source in shipment.DataContext.DataSourceCollection
                                       where (source.Type == "ForwardingShipment")
                                       select source.Key).FirstOrDefault();

                            order.ShipmentNo = sno != null ? sno : string.Empty;
                            orders.Add(order);
                        }
                    }
                }
            }

            return orders;

        }
        private OrderLineElement[] GetOrderLinesFromOrders(ShipmentOrderOrderLineCollection orderLineCollection)
        {
            List<OrderLineElement> lines = new List<OrderLineElement>();
            foreach (ShipmentOrderOrderLineCollectionOrderLine ol in orderLineCollection.OrderLine)
            {
                if (ol.OrderedQty > 0)
                {
                    OrderLineElement line = new OrderLineElement
                    {
                        LineNo = ol.LineNumber,
                        OrderQty = ol.OrderedQty,
                        OrderQtySpecified = ol.OrderedQtySpecified,
                        PackageQty = (int)ol.PackageQty,
                        PackageUnit = ol.PackageQtyUnit.Code,
                        Product = new ProductElement { Code = ol.Product.Code, Description = ol.Product.Description, Barcode = ol.ConfirmationNumber },
                        Weight = ol.Weight,
                        WeightSpecified = ol.WeightSpecified,
                        Volume = ol.Volume,
                        VolumeSpecified = ol.VolumeSpecified,
                        WeightUnit = Extensions.ToEnum<OrderLineElementWeightUnit>(ol.WeightUnit.Code, OrderLineElementWeightUnit.KG),
                        WeightUnitSpecified = true,
                        VolumeUnit = Extensions.ToEnum<OrderLineElementVolumeUnit>(ol.VolumeUnit.Code, OrderLineElementVolumeUnit.M3)

                    };

                    lines.Add(line);

                }

            }
            return lines.ToArray();

        }

        private List<NodeFileWarehouseOrder> GetWarehouseOrdersFromCW(object cW, string orderNo)
        {
            string lo = LogPath;
            string cp = string.Empty;
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            return null;
        }

        private NodeFileShipment GetNodeShipmentFromCWShipment(Shipment cwShipment, string shipmentNo, string consolNo)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetShipmentsFromShipment";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            string key = string.Empty;

            foreach(var ds in cwShipment.DataContext.DataSourceCollection)
            {
                if(ds.Type == "ForwardingShipment")
                {
                    key = ds.Key;
                }
            }

            NodeFileShipment shipment = new NodeFileShipment();
            try
            {
                NodeFileShipment ctcShipment = new NodeFileShipment
                {
                    Companies = GetCompaniesFromShipment(cwShipment.OrganizationAddressCollection),
                    Containers = GetContainersFromShipment(cwShipment.ContainerCollection, cwShipment.LocalProcessing),
                    Dates = GetDatesFromShipment(cwShipment.DateCollection),
                    Routes = cwShipment.TransportLegCollection !=  null ? GetLocationsFromShipment(cwShipment.TransportLegCollection).ToArray() : null,
                    FlightVoyage = cwShipment.VoyageFlightNo,
                    Vessel = cwShipment.VesselName,
                    GoodsDescription = shipment.GoodsDescription,
                    HouseBillNo = cwShipment.WayBillType.Code == "HouseBill" ? cwShipment.WayBillNumber : null,
                    ShipmentNo = shipmentNo,
                    Key = key,
                    ConsolNo = consolNo,
                    IncoTerms = GetIncoTerm(cwShipment),
                    TotalPcs = cwShipment.TotalNoOfPieces,
                    TotalPcsUnit = cwShipment.TotalNoOfPacksPackageType.Code,
                    ChargeableWeight = cwShipment.ActualChargeable,
                    GoodsCurrency = cwShipment.GoodsValueSpecified ? cwShipment.GoodsValueCurrency.Code : null,
                    TotalVolume = cwShipment.TotalVolume,
                    TotalVolumeSpecified = cwShipment.TotalVolume > 0,
                    TotalWeight = cwShipment.TotalWeight,
                    TotalWeightSpecified = cwShipment.TotalWeight > 0,
                    ChargeableWeightSpecified = cwShipment.ActualChargeable > 0,
                    Milestones = cwShipment.MilestoneCollection == null ? null : GetMileStonesFromCW(cwShipment.MilestoneCollection, shipmentNo)

                };

                er = "NoteCollection";

                if (cwShipment.RelatedShipmentCollection != null)
                {
                    if (cwShipment.RelatedShipmentCollection[0].NoteCollection != null && cwShipment.RelatedShipmentCollection[0].NoteCollection.Note[0].NoteText != "")
                    {
                        ctcShipment.Vendor = GetVendor(cwShipment.RelatedShipmentCollection[0].NoteCollection.Note[0].NoteText);
                    }
                }


                List<LocationElement> locations = new List<LocationElement>();
                locations = GetLocationsFromShipment(cwShipment);
                ctcShipment.Locations = locations != null ? locations.ToArray() : null;
                List<ShipmentLineElement> shipmentline = new List<ShipmentLineElement>();
                shipmentline = GetShipmentLinesFromShipment(cwShipment, ctcShipment);
                ctcShipment.ShipmentLines = shipmentline != null ? shipmentline.ToArray() : null;
                NodeFileShipmentTransportMode tm;
                if (Enum.TryParse(cwShipment.TransportMode.Code, out tm))
                {
                    ctcShipment.TransportMode = tm;
                    ctcShipment.TransportModeSpecified = true;
                }

                NodeFileShipmentTotalVolumeUnit vu;

                if (Enum.TryParse(cwShipment.TotalVolumeUnit.Code, out vu))
                {
                    ctcShipment.TotalVolumeUnit = vu;
                    ctcShipment.TotalVolumeUnitSpecified = true;
                }

                NodeFileShipmentTotalWeightUnit wu;
                if (Enum.TryParse(cwShipment.TotalWeightUnit.Code, out wu))
                {
                    ctcShipment.TotalWeightUnit = wu;
                    ctcShipment.TotalWeightUnitSpecified = true;
                }

                NodeFileShipmentContainerMode cMode;
                if (Enum.TryParse(cwShipment.ContainerMode.Code, out cMode))
                {
                    ctcShipment.ContainerMode = cMode;
                    ctcShipment.ContainerModeSpecified = true;
                }

                shipment = ctcShipment;
            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Shipment Details " + er, fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
                return null;
            }

            return shipment;
        }

        public string GetIncoTerm(Shipment cwShipment)
        {
            string incoterm = null;

            if(cwShipment.ShipmentIncoTerm != null)
            {
                incoterm = cwShipment.ShipmentIncoTerm.Code;
            }
            else if(cwShipment.SubShipmentCollection != null)
            {
                if(cwShipment.SubShipmentCollection[0].ShipmentIncoTerm != null)
                {
                    incoterm = cwShipment.SubShipmentCollection[0].ShipmentIncoTerm.Code;
                }
            }

            return incoterm;
        }

        public string GetVendor(string text)
        {
            string final = "";

            int i1 = text.IndexOf("Name: ") + "Name: ".Length;

            string temp = text.Substring(i1);

            int i2 = temp.IndexOf("\n");

            final = temp.Substring(0, i2);
            return final;
        }

        private List<LocationElement> GetLocationsFromShipment(Shipment shipment)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetLocationsFromShipment";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            List<LocationElement> locations = new List<LocationElement>();

            try
            {
                LocationElement location = shipment.PortFirstForeign != null ? new LocationElement
                {
                    Code = shipment.PortFirstForeign.Code,
                    Name = shipment.PortFirstForeign.Name,
                    Type = "PortFirstForeign"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PortLastForeign != null ? new LocationElement
                {
                    Code = shipment.PortLastForeign.Code,
                    Name = shipment.PortLastForeign.Name,
                    Type = "PortLastForeign"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PortOfDestination != null ? new LocationElement
                {
                    Code = shipment.PortOfDestination.Code,
                    Name = shipment.PortOfDestination.Name,
                    Type = "PortOfDestination"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PortOfDischarge != null ? new LocationElement
                {
                    Code = shipment.PortOfDischarge.Code,
                    Name = shipment.PortOfDischarge.Name,
                    Type = "PortOfDischarge"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PortOfFirstArrival != null ? new LocationElement
                {
                    Code = shipment.PortOfFirstArrival.Code,
                    Name = shipment.PortOfFirstArrival.Name,
                    Type = "PortFirstArrival"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PortOfLoading != null ? new LocationElement
                {
                    Code = shipment.PortOfLoading.Code,
                    Name = shipment.PortOfLoading.Name,
                    Type = "PortOfLoading"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PortOfOrigin != null ? new LocationElement
                {
                    Code = shipment.PortOfOrigin.Code,
                    Name = shipment.PortOfOrigin.Name,
                    Type = "PortOfOrigin"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PlaceOfDelivery != null ? new LocationElement
                {
                    Code = shipment.PlaceOfDelivery.Code,
                    Name = shipment.PlaceOfDelivery.Name,
                    Type = "PortOfDelivery"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PlaceOfIssue != null ? new LocationElement
                {
                    Code = shipment.PlaceOfIssue.Code,
                    Name = shipment.PlaceOfIssue.Name,
                    Type = "PlaceOfIssue"

                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
                location = shipment.PortFirstForeign != null ? new LocationElement
                {
                    Code = shipment.PortFirstForeign.Code,
                    Name = shipment.PortFirstForeign.Name,
                    Type = "PortFirstForeign"
                } : null;
                if (location != null)
                {
                    locations.Add(location);
                }
            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Location Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }


            return locations;
        }

        private List<RouteElement> GetLocationsFromShipment(XMLLocker.Cargowise.XUS.TransportLeg[] legs)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetLocationsFromShipment";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            List<RouteElement> routes = new List<RouteElement>();
            List<LocationElement> locations = new List<LocationElement>();

            foreach(var leg in legs)
            {
                RouteElement route = new RouteElement();
                try
                {
                    LocationElement location = leg.PortOfDischarge != null ? new LocationElement
                    {
                        Code = leg.PortOfDischarge.Code,
                        Name = leg.PortOfDischarge.Name,
                        Type = "PortOfDischarge",
                        Estimated = leg.EstimatedArrival,
                        Actual = leg.ActualArrival

                    } : null;
                    if (location != null)
                    {
                        locations.Add(location);
                    }

                    location = leg.PortOfLoading != null ? new LocationElement
                    {
                        Code = leg.PortOfLoading.Code,
                        Name = leg.PortOfLoading.Name,
                        Type = "PortOfLoading",
                        Estimated = leg.EstimatedDeparture,
                        Actual = leg.ActualDeparture
                    } : null;
                    if (location != null)
                    {
                        locations.Add(location);
                    }

                    route.TransportMode = leg.TransportMode.ToString();
                    route.Vessel = leg.VesselName;
                    route.VoyageNo = leg.VoyageFlightNo;
                    route.LegOrder = leg.LegOrder;
                    route.Locations = locations.ToArray();

                    routes.Add(route);
                    
                }
                catch (Exception ex)
                {
                    using (AppLogger log = new AppLogger(lo, cp, "Failed to get Location Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                    {
                        log.AddLog();
                    }
                }
            }
            return routes;
        }

        private List<ShipmentLineElement> GetShipmentLinesFromShipment(Shipment shipment, NodeFileShipment ctcShipment)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetShipmentLinesFromShipment";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            List<ShipmentLineElement> shipmentlines = new List<ShipmentLineElement>();

            try
            {
                string containerNo = "";
                string containerType = "";
                //container
                if (shipment.ContainerCollection != null)
                {
                    if (shipment.ContainerCollection.Container != null)
                    {
                        var container = shipment.ContainerCollection.Container.FirstOrDefault();
                        if (container != null)
                        {
                            containerNo = container.ContainerNumber;
                            containerType = container.ContainerType.Code;
                        }
                    }

                }

                if (shipment.RelatedShipmentCollection != null)
                {
                    foreach (var rsc in shipment.RelatedShipmentCollection.ToList())
                    {
                        if (rsc.Order.OrderLineCollection != null)
                        {
                            foreach (var item in rsc.Order.OrderLineCollection.OrderLine.ToList())
                            {
                                var sline = new ShipmentLineElement()
                                {
                                    ContainerNumber = containerNo,
                                    ContainerType = containerType,
                                    Currency = ctcShipment.GoodsCurrency,
                                    Description = item.Product.Description,
                                    DocEntry = item.ConfirmationNumber,
                                    ItemPrice = item.UnitPriceRecommended,
                                    ItemPriceSpecified = true,
                                    LineNo = item.SubLineNumber.ToString(),
                                    OrderNumber = rsc.Order.OrderNumber,
                                    ProductCode = item.Product.Code,
                                    QtyOrdered = item.OrderedQty,
                                    QtyOrderedSpecified = true,
                                    QtyPacked = item.PackageQty,
                                    QtyPackedSpecified = true,
                                    Status = item.Status.Code,
                                    UnitOfQuantity = item.OrderedQtyUnit.Code

                                };

                                sline.TotalPrice = sline.QtyOrdered * sline.ItemPrice;
                                sline.TotalPriceSpecified = true;

                                shipmentlines.Add(sline);

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Shipment Lines Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }


            return shipmentlines;
        }
        private NodeFileMaster GetMasterFromCW(UniversalInterchange cw)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetMasterFromCW";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;

            NodeFileMaster master = new NodeFileMaster();
            try
            {
                Consol = cw.Body.BodyField.Shipment;
                master.ConsolNo = GetKeyFromContext(cw.Body.BodyField.Shipment.DataContext, "ForwardingConsol");
                master.Dates = GetDatesFromShipment(Consol.DateCollection);
                master.Companies = GetCompaniesFromShipment(Consol.OrganizationAddressCollection);
                master.Containers = GetContainersFromShipment(Consol.ContainerCollection, Consol.LocalProcessing);
                master.FlightVoyage = Consol.VoyageFlightNo;
                NodeFileMasterMasterType contMode;
                if (Enum.TryParse(Consol.ContainerMode.Code, out contMode))
                {
                    master.MasterType = contMode;
                }

                List<LocationElement> locations = new List<LocationElement>();
                LocationElement loc = new LocationElement
                {
                    Code = Consol.PortOfDischarge.Code,
                    Name = Consol.PortOfDischarge.Name,
                    Type = "PortOfDischarge"
                };
                locations.Add(loc);
                loc = new LocationElement
                {
                    Code = Consol.PortOfLoading.Code,
                    Name = Consol.PortOfLoading.Name,
                    Type = "PortOfLoading"
                };
                locations.Add(loc);
                master.Locations = locations.ToArray();
                master.Vessel = Consol.VesselName;
                switch (Consol.TransportMode.Code.ToUpper())
                {
                    case "SEA":
                        master.TransportMode = NodeFileMasterTransportMode.SEA;
                        break;
                    case "AIR":
                        master.TransportMode = NodeFileMasterTransportMode.AIR;
                        break;
                }
                master.TransportModeSpecified = true;
                master.PrePaid = Consol.PaymentMethod.Code;

                if (Consol.WayBillType.Code == "MWB")
                {
                    master.MasterBillNo = Consol.WayBillNumber;
                }
                master.Milestones = Consol.MilestoneCollection == null ? null : GetMileStonesFromCW(Consol.MilestoneCollection, master.ConsolNo);
            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Master Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }

            return master;
        }

        private ContainerElement[] GetContainersFromShipment(ShipmentContainerCollection containerCollection, ShipmentLocalProcessing localProcessing)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetContainersFromShipment";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;

            List<ContainerElement> containers = new List<ContainerElement>();
            try
            {
                if (containerCollection.Container == null)
                {
                    return containers.ToArray();
                }
                foreach (Container cn in containerCollection.Container)
                {
                    ContainerElement ctcCont = new ContainerElement
                    {
                        ContainerNo = cn.ContainerNumber,
                        ContainerCode = cn.ContainerType.Code,
                        ISOCode = cn.ContainerType.ISOCode,
                        GoodsWeight = cn.GoodsWeight,
                        GoodsWeightSpecified = cn.GoodsWeight > 0,
                        GrossWeight = cn.GrossWeight,
                        GrossWeightSpecified = cn.GrossWeight > 0,
                        SealNo = cn.Seal,
                        Volume = cn.VolumeCapacity,
                        VolumeSpecified = cn.VolumeCapacity > 0,
                        VolumeUnit = ContainerElementVolumeUnit.M3,
                        VolumeUnitSpecified = true,
                        WeightUnit = ContainerElementWeightUnit.KG,
                        WeightUnitSpecified = true,
                        EquipmentNeeded = GetFCLEquipFromLocalProcessing(localProcessing)
                    };
                    if (cn.Commodity != null)
                    {

                        ctcCont.Commodity = cn.Commodity.Code.ToEnum<ContainerElementCommodity>(ContainerElementCommodity.GEN);
                        ctcCont.CommoditySpecified = true;
                    }
                    if (_shipment != null)
                    {
                        if (_shipment.LocalProcessing != null)
                        {
                            if (_shipment.LocalProcessing.FCLDeliveryEquipmentNeeded != null)
                            {
                                switch (_shipment.LocalProcessing.FCLDeliveryEquipmentNeeded.Code)
                                {
                                    case "SID":
                                        ctcCont.EquipmentNeeded = ContainerElementEquipmentNeeded.SDL;
                                        break;
                                    case "SDL":
                                        ctcCont.EquipmentNeeded = ContainerElementEquipmentNeeded.SDL;
                                        break;
                                    case "TRL":
                                        ctcCont.EquipmentNeeded = ContainerElementEquipmentNeeded.DRP;
                                        break;
                                    default:
                                        ctcCont.EquipmentNeeded = ContainerElementEquipmentNeeded.STD;
                                        break;
                                }
                            }
                        }
                        var contDates = GetContainerDates(_shipmentcontainerCollection, cn.ContainerNumber);


                    }
                    else
                    {
                        List<DateElement> contDates = new List<DateElement>();
                        if (localProcessing != null)
                        {
                            if (!string.IsNullOrEmpty(localProcessing.EstimatedDelivery))
                            {
                                contDates.Add(new DateElement
                                {
                                    DateType = DateElementDateType.DeliverBy,
                                    EstimateDate = localProcessing.EstimatedDelivery
                                });
                                ctcCont.Dates = contDates.ToArray();
                            }
                            //else
                            //{
                            //    contDates.Add(new DateElement
                            //    {
                            //        DateType = DateElementDateType.DeliverBy,
                            //        EstimateDate = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz")
                            //    });
                            //}
                        }


                    }

                    containers.Add(ctcCont);
                }
            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Containers Details", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }
            return containers.ToArray();
        }

        private List<DateElement> GetContainerDates(Container[] containers, string containerNumber)
        {
            List<DateElement> dates = new List<DateElement>();
            if (containers != null)
            {

                var container = (from c in containers
                                 where c.ContainerNumber == containerNumber
                                 select c).FirstOrDefault();
                if (container == null)
                {
                    return dates;
                }
                DateElement newDate = ReturnDateFromString(container.ArrivalCartageAdvised, DateElementDateType.CartageAdvised, true);
                if (newDate != null)
                {
                    dates.Add(newDate);

                }
                newDate = ReturnDateFromString(container.ArrivalCartageComplete, DateElementDateType.Delivered, true);
                if (newDate != null)
                {
                    dates.Add(newDate);

                }
                newDate = ReturnDateFromString(container.ArrivalEstimatedDelivery, DateElementDateType.Delivered, false);
                if (newDate != null)
                {
                    dates.Add(newDate);

                }
                newDate = ReturnDateFromString(container.ArrivalDeliveryRequiredBy, DateElementDateType.Required, true);
                if (newDate != null)
                {
                    dates.Add(newDate);

                }

            }
            return dates;
        }



        private CompanyElement[] GetCompaniesFromShipment(OrganizationAddress[] organizationAddressCollection)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetCompaniesFromShipment";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            List<CompanyElement> ctcCompanys = new List<CompanyElement>();
            CompanyElement company;
            try
            {
                foreach (OrganizationAddress oa in organizationAddressCollection)
                {
                    if (oa.Country == null)
                    {
                        company = new CompanyElement
                        {
                            ContactName = oa.CompanyName,
                            CompanyOrgCode = oa.OrganizationCode,
                            Address1 = oa.Address1,
                            Address2 = oa.Address2,
                            City = oa.City,
                            State = oa.State,
                            PostCode = oa.Postcode,
                            PhoneNo = oa.Phone,
                            EmailAddress = oa.Email,
                            CompanyCode = oa.AddressShortCode,
                            CompanyType = GetCompType(oa.AddressType),
                            CompanyName = oa.CompanyName
                        };
                    }
                    else
                    {
                        company = new CompanyElement
                        {
                            ContactName = oa.CompanyName,
                            CompanyOrgCode = oa.OrganizationCode,
                            Address1 = oa.Address1,
                            Address2 = oa.Address2,
                            City = oa.City,
                            State = oa.State,
                            PostCode = oa.Postcode,
                            Country = oa.Country.Name,
                            CountryCode = oa.Country.Code,
                            PhoneNo = oa.Phone,
                            EmailAddress = oa.Email,
                            CompanyCode = oa.AddressShortCode,
                            CompanyType = GetCompType(oa.AddressType),
                            CompanyName = oa.CompanyName
                        };
                    }

                    if (oa.Port != null)
                    {
                        company.Address3 = oa.Port.Code;
                    }
                    ctcCompanys.Add(company);
                }
            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Companies ", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }
            return ctcCompanys.ToArray();
        }

        private DateElement[] GetDatesFromShipment(Date[] dateCollection)
        {
            string lo = LogPath;
            string cp = string.Empty;
            string fu = "GetCompaniesFromShipment";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            List<DateElement> ctcDates = new List<DateElement>();
            try
            {
                if (dateCollection != null)
                {
                    foreach (Date date in dateCollection)
                    {
                        if (!string.IsNullOrEmpty(date.Value))
                        {
                            DateElement ctcDate = new DateElement();
                            if (date.IsEstimate)
                            {
                                ctcDate.EstimateDate = date.Value;
                            }
                            if (!date.IsEstimate)
                            {
                                ctcDate.ActualDate = date.Value;
                            }
                            ctcDate.DateType = MapDateType(date.Type);
                            if (ctcDate.DateType != DateElementDateType.None)
                            {
                                ctcDates.Add(ctcDate);
                            }

                        }



                    }
                }

            }
            catch (Exception ex)
            {
                using (AppLogger log = new AppLogger(lo, cp, "Failed to get Dates ", fu, ti, "Error " + ex.GetType().Name + ": " + ex.Message, di))
                {
                    log.AddLog();
                }
            }

            return ctcDates.ToArray();
        }

        #endregion

        #region helpers
        private DateElementDateType MapDateType(DateType dateType)
        {
            DateElementDateType retVal;
            switch (dateType)
            {
                case DateType.Arrival:
                    retVal = DateElementDateType.Arrival;

                    break;
                case DateType.FirstArrivalInCountry:
                    retVal = DateElementDateType.FirstArrival;
                    break;
                case DateType.BookingConfirmed:
                    retVal = DateElementDateType.Booked;

                    break;
                case DateType.CutOffDate:
                    retVal = DateElementDateType.Closing;

                    break;
                case DateType.EntryDate:
                    retVal = DateElementDateType.CustomsCleared;

                    break;
                case DateType.ClientRequestedETA:
                    retVal = DateElementDateType.DeliverBy;

                    break;
                case DateType.Delivery:
                    retVal = DateElementDateType.Delivered;

                    break;
                case DateType.Departure:
                    retVal = DateElementDateType.Departure;

                    break;
                case DateType.AvailableExFactory:
                    retVal = DateElementDateType.ExFactory;

                    break;
                case DateType.DeliveryRequiredBy:
                    retVal = DateElementDateType.InStoreDateReq;

                    break;
                case DateType.LoadingDate:
                    retVal = DateElementDateType.Loading;

                    break;
                case DateType.ShippedOnBoard:
                    retVal = DateElementDateType.Loading;

                    break;
                case DateType.OrderDate:
                    retVal = DateElementDateType.Raised;

                    break;
                case DateType.Unpack:
                    retVal = DateElementDateType.Unloading;

                    break;
                case DateType.DepartureVesselCutoffDate:
                    retVal = DateElementDateType.CutOff;
                    break;
                case DateType.ExWorksRequiredBy:
                    retVal = DateElementDateType.ExFactory;
                    break;
                case DateType.DischargeDate:
                    retVal = DateElementDateType.DischargeDate;
                    break;
                case DateType.EntrySubmitted:
                    retVal = DateElementDateType.EntryDate;
                    break;

                default:
                    retVal = DateElementDateType.None;
                    break;

            }
            return retVal;
        }
        private DateElement ReturnDateFromString(string stringDate, DateElementDateType dateType, bool actual)
        {
            DateTime d;
            if (string.IsNullOrEmpty(stringDate))
            {
                return null;
            }
            if (!DateTime.TryParse(stringDate, out d))
            {
                return null;
            }
            DateElement date = new DateElement();
            if (actual)
            {
                date.ActualDate = d.ToString("dd/MM/yyyyThh:mm:ss");
            }
            else
            {
                date.EstimateDate = d.ToString("dd/MM/yyyyThh:mm:ss");
            }
            date.DateType = dateType;
            return date;
        }
        private string GetParamFromProfile(List<string> param, string accountType)
        {
            var pair = (from p in param
                        where p.Contains(accountType)
                        select p).FirstOrDefault();
            if (pair != null)
            {
                string[] code = pair.Split(':');

                return !string.IsNullOrEmpty(code[1]) ? code[1] : null;
            }
            ErrorString += "ERROR: Other Account Code Not Found: " + accountType + Environment.NewLine;
            return null;
        }
        private string GetKeyValue(DataSource[] collection, string type)
        {
            var result = (from x in collection
                          where x.Type == type
                          select x.Key).FirstOrDefault();
            return result;
        }
        #endregion




















    }
}

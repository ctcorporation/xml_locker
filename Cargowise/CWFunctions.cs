﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using XMLLocker.Cargowise.XUS;

namespace XMLLocker.Cargowise
{

    public static class CWFunctions
    {

        public static string GetRef(List<AdditionalReference> additionalReferenceCollection, string v)
        {
            string result = string.Empty;
            var ar = (from r in additionalReferenceCollection
                      where (r.Type.Code == v)
                      select r.ReferenceNumber).FirstOrDefault();
            result = ar != null ? ar : result;
            return result;

        }

        public static bool IsCargowiseFile(string fileName)
        {
            bool cwFile = false;
            try
            {
                XDocument cwXML = XDocument.Load(fileName);
                var ns = cwXML.Root.Name.Namespace;
                if (ns.NamespaceName.Contains("www.cargowise.com") || ns.NamespaceName.Contains("www.edi.com"))
                {
                    cwFile = true;
                }

            }
            catch (IOException ex)
            {

            }
            catch (Exception)
            {

            }
            return cwFile;
        }

        public static XNamespace GetNameSpace(string fileName)
        {
            try
            {
                XDocument doc = XDocument.Load(fileName);
                var ns = doc.Root.Name.Namespace;
                if (ns != null)
                {
                    return ns;
                }
            }
            catch (IOException ex)
            {
                return "InUse";
            }

            return null;
        }
        public static string GetCodefromCW(string fileName, string codeTofind)
        {
            string val = string.Empty;
            try
            {
                XDocument cwXML = XDocument.Load(fileName);
                var ns = cwXML.Root.Name.Namespace;
                string n = GetApplicationCode(ns.NamespaceName);


                switch (n)
                {
                    case "UDM":
                        //universal xml files XUS - XUE - XUT - XUA - XUD
                        var code = from c in cwXML.Descendants()
                                   where c.Name.LocalName == codeTofind
                                   select c;
                        val = (from v in code.Elements()
                               where v.Name.LocalName == "Code"
                               select v.Value).FirstOrDefault();
                        break;
                    case "NDM":
                        //native XML data - products / Orgs / Rates

                        break;
                    case "XMS":
                        //legacy xml files
                        val = (from cwelement in cwXML.Descendants()
                                    where cwelement.Name.LocalName == codeTofind
                                    select cwelement.Value).FirstOrDefault();

                        break;
                    default:

                        break;
                }
            }
            catch (FileNotFoundException ex)
            {
                val = "NotFound";
            }


            return val;
        }


        public static string GetXMLType(string messageFilePath)
        {
            string result = string.Empty;
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Load(messageFilePath);
            }
            catch (IOException ex)
            {
                return "InUse";
            }
           
            //using (var fileStream = GetFileAsStream(messageFilePath))
            //{
            //    using (var reader = XmlReader.Create(fileStream))
            //    {
            //        xDoc = XDocument.Load(reader);
            //    }
            //}
            if (IsCargowiseFile(messageFilePath))
            {
                var nsList = xDoc.Descendants().Select(n => n.Name.NamespaceName).Distinct().ToList();
                XElement typeXML = null;
                foreach (XNamespace ns in nsList)
                {
                    typeXML = xDoc.Descendants().Where(n => n.Name == ns + "Body").FirstOrDefault();
                    if (typeXML != null)
                    {
                        break;
                    }
                    else
                    {
                        typeXML = xDoc.Descendants().Where(n => n.Name == ns + "InterchangeInfo").FirstOrDefault();
                    }

                }

                var name = (XElement)typeXML.FirstNode;

                switch (name.Name.LocalName)

                {
                    case "UniversalEvent":
                        result = "UDM";
                        break;
                    case "UniversalShipment":
                        result = "UDM";
                        break;
                    case "Native":
                        result = "NDM";
                        break;
                    case "UniversalTransaction":
                        result = "UDM";
                        break;
                    default:
                        result = name.Document.Root.Name.LocalName == "XmlInterchange" ? "XMS" : "UNK";
                        break;
                }
                return result;
            }
            else
            {
                return "N/A";
            }



        }
        public static string GetApplicationCode(string messageNamespace)
        {
            string appCode = string.Empty;
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    appCode = "UDM";
                    break;

                case "http://www.cargowise.com/Schemas/Native":
                    appCode = "NDM";
                    break;

                case "http://www.edi.com.au/EnterpriseService/":
                    appCode = "XMS";
                    break;

                default:
                    appCode = "";
                    break;
            }

            return appCode;


        }

        public static void CreateNativeOrgFile(XMLLocker.Cargowise.NDM.Organization.Native nativeOrg, string outputPath)
        {
            var cwNSUniversal = new XmlSerializerNamespaces();
            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            var cwNSNative = new XmlSerializerNamespaces();
            cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
            Cargowise.NDM.Organization.UniversalInterchange universalInterchange = new Cargowise.NDM.Organization.UniversalInterchange();
            Cargowise.NDM.Organization.UniversalInterchangeHeader universalInterchangeHeader = new Cargowise.NDM.Organization.UniversalInterchangeHeader();
            Cargowise.NDM.Organization.UniversalInterchangeBody universalInterchangeBody = new Cargowise.NDM.Organization.UniversalInterchangeBody();

            nativeOrg.version = "2.0";
            nativeOrg.Header.DataContext = new NDM.Organization.DataContext();
            nativeOrg.Header.DataContext.ActionPurpose = new Cargowise.NDM.CodeDescriptionPair { Code = "IMP", Description = "Org Import" };
            nativeOrg.Header.DataContext.EventType = new Cargowise.NDM.CodeDescriptionPair { Code = "ADD", Description = "Record Added" };
            universalInterchange.version = "1.1";
            universalInterchangeHeader.SenderID = "CTCORGIMP";
            universalInterchangeHeader.RecipientID = "PCFSYDSYD";
            universalInterchangeBody.Any = new[] { nativeOrg.AsXmlElement(cwNSUniversal) };
            universalInterchange.Body = universalInterchangeBody;
            universalInterchange.Header = universalInterchangeHeader;
            String cwXML = Path.Combine(outputPath, "PCFSYDSYD-ORGIMPORT" + ".xml");
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(outputPath, "PCFSYDSYD-ORGIMPORT-" + iFileCount + ".xml");
            }
            using (Stream outputCW = File.Open(cwXML, FileMode.Create))
            {
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.NDM.Organization.UniversalInterchange));
                xSer.Serialize(outputCW, universalInterchange, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();
            }
        }






    }
}

﻿using System;
using System.Data;
using System.Linq;
using System.Text;

public static class Extensions
{

    public static T ToEnum<T>(this string value, T defaultValue)
    {
        if (string.IsNullOrEmpty(value))
        {
            return defaultValue;
        }
        try
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        catch (Exception ex)
        {
            return defaultValue;
        }




    }

    public static string ToCWDate(this string value)
    {
        try
        {
            return DateTime.FromOADate(double.Parse(value)).ToString("yyyy-MM-ddTHH\\:mm\\:ss");
        }
        catch (Exception)
        {
            return value;
        }
    }


}



public static class StringEx
{
    public static string Truncate(this string value, int maxLength)
    {
        if (string.IsNullOrEmpty(value))
        {
            return value;
        }
        value = value.Trim();
        return value.Length <= maxLength ? value : value.Substring(0, maxLength);
    }
}

public static class TableEx
{
    public static bool ContainsColumn(this DataRow tbl, string columnName)
    {
        if (tbl.Table.Columns.Contains(columnName))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static string RemoveChars(this string s, params char[] removeChars)
    {
        //Contract.Requires<ArgumentNullException>(s != null);
        //Contract.Requires<ArgumentNullException>(removeChars != null);
        var sb = new StringBuilder(s.Length);
        foreach (char c in s)

        {
            if (!removeChars.Contains(c))
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }
}


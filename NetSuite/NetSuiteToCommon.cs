﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace XML_Locker.NetSuite
{
    public class NetSuiteToCommon
    {
        #region members
        private string _senderID;
        private string _recipientId;
        private string _errMsg;
        #endregion

        #region Properties
        public string CommonType { get; set; }
        public string ErrMsg
        {
            get { return _errMsg; }
            set { _errMsg = value; }
        }
        #endregion

        #region Constructors
        public NetSuiteToCommon(string senderId, string recipientId)
        {
            _senderID = senderId;
            _recipientId = recipientId;
        }
        #endregion

        #region Methods

        public List<NodeFile> DoConvert(string xmlFile)
        {
            var xmlInfo = new FileInfo(xmlFile);
            var netsuite = ReadOrdersXml(xmlFile);
            if (netsuite != null)
            {
                var common = ConvertToCommon(netsuite);
                return common;
            }
            return null;
        }

        public Orders ReadOrdersXml(string xmlFile)
        {
            Orders netSuiteOrder = new Orders();
            try
            {
                using (FileStream fStream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    XmlSerializer netSuiteImport = new XmlSerializer(typeof(Orders), "http://www.spscommerce.com/RSX");
                    netSuiteOrder = (Orders)netSuiteImport.Deserialize(fStream);
                    return netSuiteOrder;
                }
            }
            catch (Exception ex)
            {
                return null;
                _errMsg += ex.GetType().Name + " error found processing " + xmlFile;
            }

        }

        public List<NodeFile> ConvertToCommon(Orders orders)
        {
            List<NodeFile> commonList = new List<NodeFile>();
            foreach (var order in orders.Order)
            {
                NodeFile common = CreateCommonFile();

                common.WarehouseOrders = CreateWharehouseOrder(order).ToArray();
                if (common.WarehouseOrders == null)
                {
                    throw new Exception("There were errors while processing the Order.");
                }
                commonList.Add(common);
            }
            return commonList;

        }

        private List<NodeFileWarehouseOrder> CreateWharehouseOrder(OrderType order)
        {
            List<NodeFileWarehouseOrder> whOrders = new List<NodeFileWarehouseOrder>();
            NodeFileWarehouseOrder whOrder = new NodeFileWarehouseOrder();
            whOrder.OrderNumber = order.Header.OrderHeader.PurchaseOrderNumber;
            whOrder.Dates = GetOrderDates(order).ToArray();
            try
            {
                whOrder.Companies = GetCompaniesFromOrder(order).ToArray();
            }
            catch (Exception ex)
            {
                _errMsg = ex.Message;
                return null;
            }

            whOrder.OrderNumber = order.Header.OrderHeader.PurchaseOrderNumber;
            whOrder.OrderLines = GetOrderLinesFromOrder(order.LineItem).ToArray();
            return whOrders;
        }
        private NodeFile CreateCommonFile()
        {
            NodeFile common = new NodeFile
            {
                IdendtityMatrix = new NodeFileIdendtityMatrix
                {
                    SenderId = _senderID,
                    CustomerId = _recipientId,
                    DocumentType = CommonType,
                    FileDateTime = DateTime.Now.ToString("dd/MM/yyyy:hh:mm:sss")
                }
            };
            return common;

        }
        #endregion


        #region Helper
        private List<OrderLineElement> GetOrderLinesFromOrder(OrderTypeLineItem[] lineItem)
        {
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            foreach (var line in lineItem)
            {
                OrderLineElement orderLine = new OrderLineElement
                {
                    LineNo = int.Parse(line.OrderLine.LineSequenceNumber),
                    OrderQty = line.OrderLine.OrderQty,
                    OrderQtySpecified = true
                };
                ProductElement prod = new ProductElement
                {
                    Barcode = line.OrderLine.EAN,
                    Code = line.OrderLine.VendorPartNumber,

                };
                foreach (var detail in line.ProductOrItemDescription)
                {
                    if (detail.ProductCharacteristicCode == OrderTypeLineItemProductOrItemDescriptionProductCharacteristicCode.Item08)
                    {
                        prod.Description = detail.ProductDescription;
                    }
                }
                orderLine.Product = prod;
                orderLines.Add(orderLine);
            }
            return orderLines;
        }
        private List<CompanyElement> GetCompaniesFromOrder(OrderType order)
        {
            List<CompanyElement> companies = new List<CompanyElement>();
            foreach (var company in order.Header.Address)
            {
                switch (company.AddressTypeCode)
                {
                    case OrderTypeHeaderAddressAddressTypeCode.ST:
                        CompanyElement newCompany = new CompanyElement();
                        newCompany.CompanyName = company.AddressName;
                        if (!string.IsNullOrEmpty(company.Address1))
                        {
                            newCompany.Address1 = company.Address1;
                            newCompany.Address2 = company.Address2;
                            if (!string.IsNullOrEmpty(company.PostalCode))
                            {
                                newCompany.PostCode = company.PostalCode;
                            }
                            else
                            {
                                throw new Exception();
                            }
                            if (!string.IsNullOrEmpty(company.City))
                            {
                                newCompany.City = company.City;
                            }
                            else
                            {
                                throw new Exception();
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(order.Header.Notes[0].Note))
                            {
                                throw new Exception("Address Cannot be Blank");
                            }
                            else
                            {
                                if (order.Header.Notes[0].Note.Contains("CASH SALE"))
                                {
                                    throw new Exception("Invalid Address: " + order.Header.Notes[0].Note);

                                }
                                string[] address = order.Header.Notes[0].Note.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);
                                string suburb = string.Empty;
                                string[] substate = null;

                                companies.Add(GetCompanyFromAddress(address));
                            }
                        }
                        break;
                }
            }

            return companies;


        }

        private CompanyElement GetCompanyFromAddress(string[] address)
        {

            CompanyElement company = new CompanyElement();
            string[] substate = null;
            string suburb = string.Empty;
            switch (address.Length)
            {
                case 3:
                    company.CompanyName = address[0];
                    company.Address1 = address[1];
                    substate = GetAddressFromText(address[2]);
                    break;
                case 4:
                    company.CompanyName = address[0];
                    company.Address1 = address[1];
                    if (!address[3].Replace(" ", string.Empty).All(Char.IsDigit))
                    {
                        if (address[3].ToUpper().Contains("PH:") || address[3].ToUpper().Contains("MOB:"))
                        {
                            company.PhoneNo = address[3];
                            substate = GetAddressFromText(address[2]);
                        }
                        else
                        {
                            company.Address2 = address[2];
                            substate = GetAddressFromText(address[3]);
                        }
                    }
                    else
                    {
                        company.Address2 = address[2];
                        substate = GetAddressFromText(address[3]);
                    }

                    break;
                case 5:
                    company.ContactName = address[0];
                    company.ContactName = address[1];
                    company.Address1 = address[2];
                    company.Address2 = string.Empty;
                    if (address[4].Replace(" ", string.Empty).All(Char.IsDigit))
                    {
                        company.PhoneNo = address[4];
                    }
                    else
                    {
                        substate = GetAddressFromText(address[4]);
                        if (substate[substate.Length - 1].All(Char.IsDigit))
                        {
                            company.City = address[3];
                        }
                    }

                    break;
                case 6:
                    company.ContactName = address[0];
                    company.CompanyName = address[1];
                    company.Address1 = address[2];
                    company.City = address[3];
                    string[] substate6 = address[4].Split(',');
                    company.PhoneNo = address[5];
                    break;
            }
            if (substate.Length == 2)
            {
                company.PostCode = substate[1];
                company.State = substate[0];

            }
            if (substate.Length == 3)
            {
                company.PostCode = substate[2];
                company.State = substate[1];
                company.City = substate[0];
            }
            if (substate.Length > 3)
            {
                company.PostCode = substate[substate.Length - 1];
                company.State = substate[substate.Length - 2];

                for (int i = 0; i < substate.Length - 2; i++)
                {
                    suburb += substate[i] + " ";
                }
                company.State = suburb;
            }
            return company;
        }

        private string[] GetAddressFromText(string address)
        {
            address.Replace(',', ' ');
            string[] add = address.Replace(',', ' ').Split(' ');
            add = add.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (add.Length == 1)
            {
                add = address.Split(',');
            }
            if (add.Length == 2)
            {
                if (!add[1].All(Char.IsDigit))
                {
                    add = GetAddressFromText(address.Replace(',', ' '));

                }
            }

            return add;
        }

        private string[] RemoveBlanks(string[] val)
        {
            List<string> valList = val.ToList();
            try
            {
                for (int i = 0; i < valList.Count; i++)
                {

                    if (string.IsNullOrEmpty(valList[i]))
                    {
                        valList.RemoveAt(i);
                    }

                }
            }
            catch (Exception)
            {

            }
            return valList.ToArray();
        }
        private string[] GetAddressFromText(string address, ref int counter)
        {
            if (counter > 10)
            {
                return null;
            }

            var addTemp = address.Replace(',', ' ');

            string[] add = RemoveBlanks(addTemp.Split(' '));
            counter++;

            try
            {
                //  add = address.Replace(',', ' ').Split(' ');
                //add = add.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                if (add.Length == 1)
                {
                    add = address.Split(',');
                }
                if (add.Length == 2)
                {
                    if (!add[1].All(Char.IsDigit))
                    {
                        add = GetAddressFromText(address.Replace(',', ' '), ref counter);
                    }


                }
            }
            catch (StackOverflowException)
            {
                add = null;
            }
            return add;
        }
        private List<DateElement> GetOrderDates(OrderType order)
        {
            List<DateElement> dates = new List<DateElement>();
            foreach (var date in order.Header.Dates)
            {
                switch (date.DateTimeQualifier)
                {
                    case OrderTypeHeaderDatesDateTimeQualifier.Item002:
                        if (date.Date >= DateTime.MinValue)
                        {
                            DateElement newDate = new DateElement
                            {
                                DateType = DateElementDateType.DeliverBy,
                                ActualDate = date.Date.ToString("s")
                            };
                            dates.Add(newDate);
                        }

                        break;

                }
            }
            dates.Add(new DateElement
            {
                DateType = DateElementDateType.Ordered,
                ActualDate = order.Header.OrderHeader.PurchaseOrderDate.ToString("s")
            });
            return dates;

        }
        #endregion




    }
}

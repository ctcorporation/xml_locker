﻿using NodeData.Models;
using System;
using System.IO;
using System.Xml.Serialization;

namespace XML_Locker
{
    public class FileBuilder : IDisposable
    {
        #region members
        string _outPath;

        #endregion

        #region properties
        string OutPath
        {
            get
            {
                return _outPath;
            }
            set
            {
                _outPath = value;
            }
        }
        #endregion

        #region constructor
        public FileBuilder(string outPath)
        {
            OutPath = outPath;
        }
        #endregion

        #region methods

     
        public string BuildXmlFile<TypeOfXml>(XmlSerializerNamespaces xmlNS, string ns, TypeOfXml xmlObject,
        string senderID, string filename, string fileType)
        {
            string cwXML = Path.Combine(_outPath, senderID + filename + fileType);
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(_outPath, senderID + filename + "-" + iFileCount + fileType);
            }
            Stream outputFile = File.Open(cwXML, FileMode.Create);
            using (StringWriter writer = new StringWriter())
            {
                XmlSerializer xSer = new XmlSerializer(typeof(TypeOfXml));
                xmlNS.Add("", ns);
                xSer.Serialize(outputFile, xmlObject, xmlNS);
                outputFile.Flush();
                outputFile.Close();
            }
            return cwXML;
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~FileBuilder()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            if (!disposedValue)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #endregion



    }
}

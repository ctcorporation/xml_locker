﻿using CTCLogging;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace XMLLocker.TMC
{
    public class TMCToCommon
    {

        private string logpath;

        public string LogPath
        {
            get { return this.logpath; }
            set { this.logpath = value; }
        }

        public TMCToCommon()
        {


        }
        public TMCToCommon(string path)
        {
            LogPath = path;
        }

        public NodeFile ConvertTMC(string fileName, Ivw_CustomerProfile profile)
        {

            NodeFile nodeFile = new NodeFile();
            FileInfo processingFile = new FileInfo(fileName);
            XDocument tmcFile = XDocument.Load(fileName);

            var oElem = tmcFile.Root.Element("Shipment");
            if (oElem.HasElements)
            {
                NodeFileTransportJob _order = new NodeFileTransportJob();
                //_order.Dates = GetDatesFromTMCOrder(oElem).ToArray();
                var companyList = GetCompanyListFromTMC(oElem);
                _order.Companies = companyList.ToArray();
                //Shipment Details
                var sdElem = oElem.Element("ShipmentDetails");
                var refs = GetReferencesTMCOrder(sdElem, _order);
                _order.CustomerReferences = refs;
                _order.ShipmentNo = sdElem.NodeExists("ShipmentID");
                _order.OrderNo = sdElem.NodeExists("ShipmentID");
                _order.CustomerName = sdElem.NodeExists("CustomerName");
                var mode = sdElem.NodeExists("ShipmentMode");
                if (mode != null)
                    if (mode.Length > 3)
                        mode = mode.Substring(0, 3);
                _order.TransportMode = mode;
                _order.OrderStatus = "INC";

                var carriers = GeCarriersFromTMC(oElem);
                _order.Carriers = carriers.ToArray();
                List<OrderLineElement> ordLines = new List<OrderLineElement>();
                ordLines = GetOrderLinesFromTMCOrder(oElem, _order);
                if (ordLines == null)
                {
                    return null;
                }
                else
                {
                    _order.OrderLines = ordLines.ToArray();
                }


                nodeFile.TransportJob = new List<NodeFileTransportJob> { _order }.ToArray();
                NodeFileIdendtityMatrix identityMatrix = new NodeFileIdendtityMatrix();
                identityMatrix.CustomerId = profile.C_CODE;
                identityMatrix.DocumentIdentifier = _order.OrderNo;
                identityMatrix.SenderId = profile.P_SENDERID;
                identityMatrix.DocumentType = "TransportJob";
                identityMatrix.FileDateTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                identityMatrix.OriginalFileName = fileName;
                nodeFile.IdendtityMatrix = identityMatrix;
            }
            return nodeFile;
        }

        private string GetElemValueString(XElement oElem, string elemName)
        {
            try
            {
                return oElem.Element(elemName).Value ?? string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
        private List<OrderLineElement> GetOrderLinesFromTMCOrder(XElement poElem, NodeFileTransportJob _order)
        {
            List<OrderLineElement> lines = new List<OrderLineElement>();
            var ordLines = poElem.Element("Commodities").Elements("Commodity");
            OrderLineElement ordLine = new OrderLineElement();
            int lineNo = 0;
            if (ordLines != null)
            {
                foreach (var line in ordLines)
                {
                    try
                    {
                        decimal q;
                        decimal u;
                        decimal v;
                        decimal w;
                        decimal h;
                        decimal l;
                        ordLine = new OrderLineElement
                        {
                            UnitPriceSpecified = true,
                            OrderQtySpecified = true,
                            LineTotalSpecified = false,
                            Product = new ProductElement
                            {
                                ItemId = line.NodeExists("ItemID"),
                                Description = line.NodeExists("CommodityDescription"),
                                Code = line.NodeExists("SKUNumber"),
                                Barcode = line.NodeExists("PONumber")
                            },
                            VolumeSpecified = true,
                            VolumeUnitSpecified = false,
                            WeightSpecified = true,
                            WeightUnitSpecified = false
                        };
                        var idData = line.NodeExists("ItemID");
                        String[] ids = idData.Split('-');
                        if (ids.Count() > 0)
                        {
                            ordLine.LineNo = int.Parse(ids[0]);
                            lineNo = ordLine.LineNo;
                        }
                        decimal.TryParse(line.NodeExists("NumberPieces", "0"), out q);
                        ordLine.OrderQty = q;
                        decimal.TryParse(line.NodeExists("CommodityValue", "0"), out u);
                        ordLine.UnitPrice = u;
                        decimal.TryParse(line.NodeExists("Volume", "0"), out v);
                        ordLine.Volume = v;
                        decimal.TryParse(line.NodeExists("MaximumWeight", "0"), out w);
                        ordLine.Weight = w;
                        decimal.TryParse(line.NodeExists("PalletHeight", "0"), out h);
                        ordLine.Height = h;
                        decimal.TryParse(line.NodeExists("PalletLength", "0"), out l);
                        ordLine.PackingSize = l;
                        ordLine.PackageUnit = line.NodeExists("Packaging"); //PackageUnit = "CTN" => CAS
                        lines.Add(ordLine);
                    }
                    catch (Exception ex)
                    {
                        var st = new StackTrace();
                        var sf = st.GetFrame(0);

                        var currentMethodName = sf.GetMethod().Name;
                        using (AppLogger log = new AppLogger(logpath, "", "Error Converting Order Lines. Order No:" + _order.OrderNo + " Line No:" + lineNo + " ",
                            currentMethodName, DateTime.Now, ex.GetType().Name + ":" + ex.Message, string.Empty))
                        {
                            log.AddLog();
                        }
                        return null;
                    }
                }
            }
            return lines;
        }

        private string GetNodeValue(XDocument arcFile, string parent, string nodeToFind)
        {
            try
            {
                var node = arcFile.Descendants(parent).Elements("row").Single();
                var returnValue = node.Element(nodeToFind) == null ? null : node.Element(nodeToFind).Value;
                return returnValue;
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                using (AppLogger log = new AppLogger(logpath, "", "Error returning value: Parent='" + parent + "' NodeToFind='" + nodeToFind + "'",
                    currentMethodName, DateTime.Now, ex.GetType().Name + ":" + ex.Message, string.Empty))
                {
                    log.AddLog();
                }
                return string.Empty;
            }


        }

        private IEnumerable<DateElement> GetDatesFromTMCOrder(XElement poElem)
        {
            var createDate = poElem.Element("Header");
            List<DateElement> dates = new List<DateElement>();
            DateElement date = new DateElement();
            string tempDate = string.Empty;

            var details = poElem.Element("ShipmentDetails");

            date = new DateElement();
            tempDate = details.NodeExists("TenderedDateTime");
            date.DateType = DateElementDateType.Tendered;
            date.ActualDate = tempDate != null ? Convert.ToDateTime(tempDate).ToString("dd/MM/yyyy hh:mm") : string.Empty;
            dates.Add(date);

            date = new DateElement();
            tempDate = details.NodeExists("ExpiredDateTime");
            date.DateType = DateElementDateType.Tendered;
            date.ActualDate = tempDate != null ? Convert.ToDateTime(tempDate).ToString("dd/MM/yyyy hh:mm") : string.Empty;
            dates.Add(date);

            date = new DateElement();
            tempDate = details.NodeExists("ActivityDate");
            date.DateType = DateElementDateType.Tendered;
            date.ActualDate = tempDate != null ? Convert.ToDateTime(tempDate).ToString("dd/MM/yyyy hh:mm") : string.Empty;
            dates.Add(date);

            return dates;
        }

        private IEnumerable<CompanyElement> GetCompanyListFromTMC(XElement poElem)
        {
            var ordNo = poElem.NodeExists("OrderNumber");
            var addressLines = poElem.Element("Stops").Elements("Stop");
            List<CompanyElement> _companies = new List<CompanyElement>();
            List<string> reqs = new List<string>();
            foreach (XElement oElem in addressLines)
            {
                reqs = new List<string>();
                var seqNo = oElem.NodeExists("StopSeqNum");
                var locId = oElem.NodeExists("LocationID");
                var name = oElem.NodeExists("Name");
                var note = oElem.NodeExists("StopNote");
                var node = oElem.Elements("StopRequirements");
                foreach (XElement rElem in node)
                {
                    reqs.Add(rElem.NodeExists("StopRequirement"));
                }

                var mainPhone = oElem.NodeExists("MainPhone") == null ? string.Empty : oElem.NodeExists("MainPhone");
                var stopContact = oElem.NodeExists("StopContact") == null ? string.Empty : oElem.NodeExists("StopContact");
                var address1 = oElem.NodeExists("Address1") == null ? string.Empty : oElem.NodeExists("Address1");
                var address2 = oElem.NodeExists("Address2") == null ? string.Empty : oElem.NodeExists("Address2");
                var city = oElem.NodeExists("City") == null ? string.Empty : oElem.NodeExists("City");
                var state = oElem.NodeExists("State") == null ? string.Empty : oElem.NodeExists("State");
                var country = oElem.NodeExists("Country") == null ? string.Empty : oElem.NodeExists("Country");
                var zipCode = oElem.NodeExists("ZipCode") == null ? string.Empty : oElem.NodeExists("ZipCode");
                var refNum = oElem.NodeExists("RefNum") == null ? string.Empty : oElem.NodeExists("RefNum");
                if (oElem.NodeExists("StopType") == "P")
                {

                    CompanyElement pickupTo = new CompanyElement();
                    pickupTo.Address1 = address1;
                    pickupTo.Address2 = address2;
                    pickupTo.City = city;
                    pickupTo.PostCode = zipCode;
                    pickupTo.State = state;
                    pickupTo.CountryCode = country;
                    pickupTo.CompanyType = CompanyElementCompanyType.PickupAddress;
                    pickupTo.ContactName = stopContact;
                    pickupTo.CompanyName = name;
                    pickupTo.PhoneNo = mainPhone;
                    pickupTo.CompanyCode = locId;
                    pickupTo.Note = note;
                    pickupTo.Requirements = reqs.ToArray();
                    _companies.Add(pickupTo);
                }
                else
                {

                    CompanyElement deliverTo = new CompanyElement();
                    deliverTo.Address1 = address1;
                    deliverTo.Address2 = address2;
                    deliverTo.City = city;
                    deliverTo.PostCode = zipCode;
                    deliverTo.State = state;
                    deliverTo.CountryCode = country;
                    deliverTo.CompanyType = CompanyElementCompanyType.DeliveryAddress;
                    deliverTo.ContactName = stopContact;
                    deliverTo.CompanyName = name;
                    deliverTo.PhoneNo = mainPhone;
                    deliverTo.CompanyCode = locId;
                    deliverTo.Note = note;
                    deliverTo.Requirements = reqs.ToArray();
                    _companies.Add(deliverTo);
                }
            }
            return _companies;
        }


        private IEnumerable<CarrierElement> GeCarriersFromTMC(XElement poElem)
        {

            var carLines = poElem.Element("Carriers").Elements("Carrier");
            List<CarrierElement> _carriers = new List<CarrierElement>();
            foreach (XElement oElem in carLines)
            {
                CarrierElement car = new CarrierElement();

                car.CarrierCode = oElem.NodeExists("CarrierCode");
                car.EquipmentType = oElem.NodeExists("EquipmentType");
                car.CustomEquipmentType = oElem.NodeExists("CustomEquipmentType");
                car.TrailerLength = oElem.NodeExists("TrailerLength");
                _carriers.Add(car);

            }
            return _carriers;
        }

        private string GetCompanycode(string v)
        {
            string code = string.Empty;
            switch (v.ToUpper())
            {
                case "CHINA":
                    code = "CN";
                    break;
                case "JAPAN":
                    code = "JP";
                    break;
                case "INDIA":
                    code = "IN";
                    break;
                case "VIETNAM":
                    code = "VN";
                    break;
                case "AUSTRALIA":
                    code = "AU";
                    break;
            }

            return code;

        }
        private CustomerReferenceElement[] GetReferencesTMCOrder(XElement poElem, NodeFileTransportJob _order)
        {
            List<CustomerReferenceElement> refs = new List<CustomerReferenceElement>();

            CustomerReferenceElement custRef;

            var refLines = poElem.Element("ShipmentRefNums").Elements("ReferenceNumber");
            if (refLines != null)
            {
                foreach (var line in refLines)
                {
                    try
                    {
                        custRef = new CustomerReferenceElement();

                        custRef.RefType = line.NodeExists("ReferenceNumType");
                        custRef.RefValue = line.NodeExists("ReferenceNum");

                        refs.Add(custRef);
                        var type = line.NodeExists("ReferenceNumType");
                        if (type == "Currency")
                            _order.Currency = line.NodeExists("ReferenceNum");
                    }
                    catch (Exception ex)
                    {
                        var st = new StackTrace();
                        var sf = st.GetFrame(0);

                        var currentMethodName = sf.GetMethod().Name;
                        using (AppLogger log = new AppLogger(logpath, "", "Error Converting Shipment References. Order No:" + _order.OrderNo + " ",
                            currentMethodName, DateTime.Now, ex.GetType().Name + ":" + ex.Message, string.Empty))
                        {
                            log.AddLog();
                        }
                    }
                }
            }

            return refs.ToArray();
        }
    }
}

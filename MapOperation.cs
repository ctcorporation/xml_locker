﻿namespace XMLLocker
{
    public class MapOperation
    {
        public string MD_MapDescription { get; set; }
        public string MD_Type { get; set; }
        public string MD_Operation { get; set; }
        public string MD_FromField { get; set; }
        public string MD_ToField { get; set; }
        public string MD_DataType { get; set; }
    }
}



﻿using CTCLogging;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using XMLLocker;

namespace XML_Locker.Airco
{
    public class AircoToCommon
    {
        //Convert Airco SAP order to Cargowise Order Manager
        private string logpath;

        public string LogPath
        {
            get { return this.logpath; }
            set { this.logpath = value; }
        }

        public AircoToCommon()
        {


        }
        public AircoToCommon(string path)
        {
            LogPath = path;
        }

        public NodeFile ConvertAirco(string fileName, Ivw_CustomerProfile profile)
        {
            NodeFile nodeFile = new NodeFile();
            FileInfo processingFile = new FileInfo(fileName);
            XDocument aircoFile = XDocument.Load(fileName);
            List<NodeFileTrackingOrder> orders = new List<NodeFileTrackingOrder>();
            var posElem = aircoFile.Root.Elements("PurchaseOrder");

            if (posElem.Count() > 0)
            {
                foreach (XElement oElem in posElem)
                {
                    var companyList = GetCompanyListFromAirco(oElem);

                    NodeFileTrackingOrder _order = new NodeFileTrackingOrder();
                    _order.Companies = companyList.ToArray();
                    _order.OrderNo = oElem.NodeExists("OrderNumber");
                    _order.Dates = GetDatesFromAircoOrder(oElem).ToArray();
                    _order.OrderStatus = "INC";
                    List<OrderLineElement> ordLines = new List<OrderLineElement>();
                    ordLines = GetOrderLinesFromAircoOrder(oElem, _order);
                    if (ordLines == null)
                    {
                        return null;
                    }
                    else
                    {
                        _order.OrderLines = ordLines.ToArray();
                    }
                    _order.TransportMode = "SEA";
                    _order.ContainerMode = "FCL";

                    NodeFileIdendtityMatrix identityMatrix = new NodeFileIdendtityMatrix();
                    identityMatrix.CustomerId = profile.C_CODE;
                    identityMatrix.DocumentIdentifier = _order.OrderNo;
                    identityMatrix.SenderId = profile.P_SENDERID;
                    identityMatrix.DocumentType = "TrackingOrder";
                    identityMatrix.FileDateTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                    identityMatrix.OriginalFileName = fileName;
                    _order.IdentityMatrix = identityMatrix;

                    orders.Add(_order);
                }

                nodeFile.TrackingOrders = orders.ToArray();
            }
            else
            {
                return null;
            }

            return nodeFile;
        }

        private string GetElemValueString(XElement oElem, string elemName)
        {
            try
            {
                return oElem.Element(elemName).Value ?? string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
        private List<OrderLineElement> GetOrderLinesFromAircoOrder(XElement poElem, NodeFileTrackingOrder _order)
        {
            List<OrderLineElement> lines = new List<OrderLineElement>();
            var ordLines = poElem.Element("OrderLines").Elements("OrderLine");
            var ordNo = poElem.NodeExists("OrderNumber");
            OrderLineElement ordLine = new OrderLineElement();
            if (ordLines != null)
            {
                foreach (var line in ordLines)
                {
                    try
                    {
                        decimal d;
                        decimal up;
                        ordLine = new OrderLineElement
                        {
                            LineNo = int.Parse(line.NodeExists("LineNum__")) + 1,
                            subLineNo = int.Parse(line.NodeExists("LineNum__")),
                            PackageUnit = line.NodeExists("UomCode"),
                            UnitPriceSpecified = true,
                            OrderQtySpecified = true,
                            LineTotalSpecified = false,
                            Product = new ProductElement
                            {
                                Description = line.NodeExists("ItemCode"),
                                Code = line.NodeExists("ItemCode"),
                                Barcode = line.NodeExists("DocEntry__")
                            },
                            VolumeSpecified = false,
                            VolumeUnitSpecified = false,
                            WeightSpecified = false,
                            WeightUnitSpecified = false,
                            DestinationState = line.NodeExists("State")
                        };
                        
                        string status = line.NodeExists("Status");

                        switch (status)
                        {
                            case "O":
                                ordLine.StatusCode = "PLC";
                                ordLine.Status = "Placed";
                                break;
                            case "C":
                                ordLine.StatusCode = "CAN";
                                ordLine.Status = "Cancelled";
                                break;
                        }
                        
                        decimal.TryParse(line.NodeExists("Quantity", "0"), out d);
                        ordLine.OrderQty = d;
                        decimal.TryParse(line.NodeExists("ItemPrice", "0"), out up);
                        ordLine.UnitPrice = up;
                        _order.Currency = line.NodeExists("Currency");
                        string testDate = string.Empty;
                        testDate = line.NodeExists("ETA") == null ? string.Empty : line.NodeExists("ETA");
                        if (!string.IsNullOrEmpty(testDate))
                        {
                            DateTime compdate;
                            List<DateElement> dates = new List<DateElement>();
                            DateElement inStoreDate = new DateElement();
                            inStoreDate.DateType = DateElementDateType.InStoreDateReq;
                            if (DateTime.TryParse(testDate, out compdate))
                            {
                                inStoreDate.ActualDate = compdate.ToString("yyyy-MM-ddThh:mm:ss");
                                dates.Add(inStoreDate);
                            }
                            if (dates.Count() > 0)
                            {
                                var container = new ContainerElement();
                                container.Dates = dates.ToArray();
                                ordLine.ContainerField = container;
                            }

                        }
                        lines.Add(ordLine);
                    }
                    catch (Exception ex)
                    {
                        var st = new StackTrace();
                        var sf = st.GetFrame(0);

                        var currentMethodName = sf.GetMethod().Name;
                        using (AppLogger log = new AppLogger(logpath, "", "Error Converting Order Lines. Order No:" + ordNo + " Line No:" + line.Element("LineNum").Value + " ",
                            currentMethodName, DateTime.Now, ex.GetType().Name + ":" + ex.Message, string.Empty))
                        {
                            log.AddLog();
                        }
                        return null;
                    }
                }
            }
            return lines;
        }

        private string GetNodeValue(XDocument arcFile, string parent, string nodeToFind)
        {
            try
            {
                var node = arcFile.Descendants(parent).Elements("row").Single();
                var returnValue = node.Element(nodeToFind) == null ? null : node.Element(nodeToFind).Value;
                return returnValue;
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                using (AppLogger log = new AppLogger(logpath, "", "Error returning value: Parent='" + parent + "' NodeToFind='" + nodeToFind + "'",
                    currentMethodName, DateTime.Now, ex.GetType().Name + ":" + ex.Message, string.Empty))
                {
                    log.AddLog();
                }
                return string.Empty;
            }


        }

        private IEnumerable<DateElement> GetDatesFromAircoOrder(XElement poElem)
        {
            List<DateElement> dates = new List<DateElement>();
            DateElement raiseDate = new DateElement();
            DateTime compdate;
            string testDate = string.Empty;
            raiseDate.DateType = DateElementDateType.Raised;
            testDate = poElem.NodeExists("OrderDate") == null ? string.Empty : poElem.NodeExists("OrderDate");
            if (DateTime.TryParse(testDate, out compdate))
            {
                raiseDate.ActualDate = compdate.ToString("dd/MM/yyyy hh:mm");
                dates.Add(raiseDate);
            }

            DateElement inStoreDate = new DateElement();
            inStoreDate.DateType = DateElementDateType.InStoreDateReq;
            testDate = poElem.NodeExists("RequiredIntoStore") == null ? string.Empty : poElem.NodeExists("RequiredIntoStore");
            if (DateTime.TryParse(testDate, out compdate))
            {
                inStoreDate.ActualDate = compdate.ToString("dd/MM/yyyy hh:mm");
                dates.Add(inStoreDate);
            }
            return dates;
        }

        private IEnumerable<CompanyElement> GetCompanyListFromAirco(XElement poElem)
        {
            //changed to only use AddressType and OrganizationCode rather than fill address and CW will map
            var deliveryAddress = poElem.Element("DeliveryAddress");
            var pickupAddress = poElem.Element("PickupAddress");
            List<CompanyElement> _companies = new List<CompanyElement>();
            CompanyElement supplier = new CompanyElement();
            //supplier.CompanyName = poElem.NodeExists("Supplier");
            supplier.CompanyOrgCode = poElem.NodeExists("Supplier");
            supplier.CompanyType = CompanyElementCompanyType.Supplier;
            _companies.Add(supplier);
            CompanyElement buyer = new CompanyElement();
            buyer.CompanyOrgCode = poElem.NodeExists("Buyer");
            buyer.CompanyType = CompanyElementCompanyType.Consignee;
            _companies.Add(buyer);
            CompanyElement deliverTo = new CompanyElement();
            //deliverTo.CompanyName = poElem.NodeExists("Buyer");
            //deliverTo.Address1 = deliveryAddress.NodeExists("StreetS") == null ? string.Empty : deliveryAddress.NodeExists("StreetS");
            //deliverTo.City = deliveryAddress.NodeExists("CityS") == null ? string.Empty : deliveryAddress.NodeExists("CityS");
            //deliverTo.PostCode = deliveryAddress.NodeExists("ZipCodeS") == null ? string.Empty : deliveryAddress.NodeExists("ZipCodeS");
            //deliverTo.State = deliveryAddress.NodeExists("StateS") == null ? string.Empty : deliveryAddress.NodeExists("StateS");
            //deliverTo.CountryCode = deliveryAddress.NodeExists("CountryS") == null ? string.Empty : deliveryAddress.NodeExists("CountryS");
            deliverTo.CompanyOrgCode = poElem.NodeExists("Buyer");
            deliverTo.CompanyType = CompanyElementCompanyType.DeliveryAddress;
            _companies.Add(deliverTo);
            CompanyElement pickupTo = new CompanyElement();
            //pickupTo.Address1 = pickupAddress.NodeExists("Street") == null ? string.Empty : pickupAddress.NodeExists("Street");
            pickupTo.City = pickupAddress.NodeExists("City") == null ? string.Empty : pickupAddress.NodeExists("City");
            //pickupTo.PostCode = pickupAddress.NodeExists("ZipCode") == null ? string.Empty : pickupAddress.NodeExists("ZipCode");
            //pickupTo.State = pickupAddress.NodeExists("State") == null ? string.Empty : pickupAddress.NodeExists("State");
            //pickupTo.CountryCode = pickupAddress.NodeExists("Country") == null ? string.Empty : pickupAddress.NodeExists("Country");
            pickupTo.CompanyOrgCode = poElem.NodeExists("Supplier");
            pickupTo.CompanyType = CompanyElementCompanyType.PickupAddress;

            if(pickupTo.City == "" || pickupTo.City == null)
            {
                //pickupTo.Address1 = pickupAddress.NodeExists("StreetB") == null ? string.Empty : pickupAddress.NodeExists("StreetB");
                //pickupTo.City = pickupAddress.NodeExists("CityB") == null ? string.Empty : pickupAddress.NodeExists("CityB");
                //pickupTo.PostCode = pickupAddress.NodeExists("ZipCodeB") == null ? string.Empty : pickupAddress.NodeExists("ZipCodeB");
                //pickupTo.State = pickupAddress.NodeExists("StateB") == null ? string.Empty : pickupAddress.NodeExists("StateB");
                //pickupTo.CountryCode = pickupAddress.NodeExists("CountryB") == null ? string.Empty : pickupAddress.NodeExists("CountryB");
                pickupTo.CompanyOrgCode = poElem.NodeExists("Supplier");
                pickupTo.CompanyType = CompanyElementCompanyType.PickupAddress;
            }
            _companies.Add(pickupTo);
            return _companies;

        }

        private string GetCompanycode(string v)
        {
            string code = string.Empty;
            switch (v.ToUpper())
            {
                case "CHINA":
                    code = "CN";
                    break;
                case "JAPAN":
                    code = "JP";
                    break;
                case "INDIA":
                    code = "IN";
                    break;
                case "VIETNAM":
                    code = "VN";
                    break;
                case "AUSTRALIA":
                    code = "AU";
                    break;
            }

            return code;

        }
    }
}

﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


// 
// This source code was auto-generated by xsd, Version=4.8.3928.0.
// 


/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class Shipments
{

    private ShipmentsShipment[] itemsField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Shipment", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ShipmentsShipment[] Items
    {
        get
        {
            return this.itemsField;
        }
        set
        {
            this.itemsField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ShipmentsShipment
{

    private string actualDepartureField;

    private decimal actualVolumeField;

    private decimal actualWeightField;

    private string consigneeField;

    private string containerModeField;

    private string departurePortField;

    private string destinationPortField;

    private string docEntry__Field;

    private string estimatedArrivalField;

    private string estimatedDepartureField;

    private string goodsDescriptionField;

    private string incoTermField;

    private string shipmentDateField;

    private string shipmentNumberField;

    private string shipmentStatusField;

    private string transportModeField;

    private string vendorField;

    private ShipmentsShipmentDeliveryAddress deliveryAddressField;

    private ShipmentsShipmentPickupAddress pickupAddressField;

    private ShipmentsShipmentShipmentLinesShipmentLine[] shipmentLinesField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string ActualDeparture
    {
        get
        {
            return this.actualDepartureField;
        }
        set
        {
            this.actualDepartureField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Vendor
    {
        get
        {
            return this.vendorField;
        }
        set
        {
            this.vendorField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public decimal ActualVolume
    {
        get
        {
            return this.actualVolumeField;
        }
        set
        {
            this.actualVolumeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public decimal ActualWeight
    {
        get
        {
            return this.actualWeightField;
        }
        set
        {
            this.actualWeightField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Consignee
    {
        get
        {
            return this.consigneeField;
        }
        set
        {
            this.consigneeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string ContainerMode
    {
        get
        {
            return this.containerModeField;
        }
        set
        {
            this.containerModeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string DeparturePort
    {
        get
        {
            return this.departurePortField;
        }
        set
        {
            this.departurePortField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string DestinationPort
    {
        get
        {
            return this.destinationPortField;
        }
        set
        {
            this.destinationPortField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string DocEntry__
    {
        get
        {
            return this.docEntry__Field;
        }
        set
        {
            this.docEntry__Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string EstimatedArrival
    {
        get
        {
            return this.estimatedArrivalField;
        }
        set
        {
            this.estimatedArrivalField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string EstimatedDeparture
    {
        get
        {
            return this.estimatedDepartureField;
        }
        set
        {
            this.estimatedDepartureField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string GoodsDescription
    {
        get
        {
            return this.goodsDescriptionField;
        }
        set
        {
            this.goodsDescriptionField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string IncoTerm
    {
        get
        {
            return this.incoTermField;
        }
        set
        {
            this.incoTermField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string ShipmentDate
    {
        get
        {
            return this.shipmentDateField;
        }
        set
        {
            this.shipmentDateField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string ShipmentNumber
    {
        get
        {
            return this.shipmentNumberField;
        }
        set
        {
            this.shipmentNumberField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string ShipmentStatus
    {
        get
        {
            return this.shipmentStatusField;
        }
        set
        {
            this.shipmentStatusField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string TransportMode
    {
        get
        {
            return this.transportModeField;
        }
        set
        {
            this.transportModeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("DeliveryAddress", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ShipmentsShipmentDeliveryAddress DeliveryAddress
    {
        get
        {
            return this.deliveryAddressField;
        }
        set
        {
            this.deliveryAddressField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("PickupAddress", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ShipmentsShipmentPickupAddress PickupAddress
    {
        get
        {
            return this.pickupAddressField;
        }
        set
        {
            this.pickupAddressField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Xml.Serialization.XmlArrayItemAttribute("ShipmentLine", typeof(ShipmentsShipmentShipmentLinesShipmentLine), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
    public ShipmentsShipmentShipmentLinesShipmentLine[] ShipmentLines
    {
        get
        {
            return this.shipmentLinesField;
        }
        set
        {
            this.shipmentLinesField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ShipmentsShipmentDeliveryAddress
{

    private string address1Field;

    private string address2Field;

    private string countryField;

    private string localityField;

    private string portField;

    private string postCodeField;

    private string regionField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Address1
    {
        get
        {
            return this.address1Field;
        }
        set
        {
            this.address1Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Address2
    {
        get
        {
            return this.address2Field;
        }
        set
        {
            this.address2Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Country
    {
        get
        {
            return this.countryField;
        }
        set
        {
            this.countryField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Locality
    {
        get
        {
            return this.localityField;
        }
        set
        {
            this.localityField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Port
    {
        get
        {
            return this.portField;
        }
        set
        {
            this.portField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string PostCode
    {
        get
        {
            return this.postCodeField;
        }
        set
        {
            this.postCodeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Region
    {
        get
        {
            return this.regionField;
        }
        set
        {
            this.regionField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ShipmentsShipmentPickupAddress
{

    private string address1Field;

    private string address2Field;

    private string countryField;

    private string localityField;

    private string portField;

    private string postCodeField;

    private string regionField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Address1
    {
        get
        {
            return this.address1Field;
        }
        set
        {
            this.address1Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Address2
    {
        get
        {
            return this.address2Field;
        }
        set
        {
            this.address2Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Country
    {
        get
        {
            return this.countryField;
        }
        set
        {
            this.countryField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Locality
    {
        get
        {
            return this.localityField;
        }
        set
        {
            this.localityField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Port
    {
        get
        {
            return this.portField;
        }
        set
        {
            this.portField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string PostCode
    {
        get
        {
            return this.postCodeField;
        }
        set
        {
            this.postCodeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Region
    {
        get
        {
            return this.regionField;
        }
        set
        {
            this.regionField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ShipmentsShipmentShipmentLinesShipmentLine
{

    private string containerNumberField;

    private string containerTypeField;

    private string currencyField;

    private string descriptionField;

    private string docEntry__Field;

    private decimal itemPriceField;

    private string lineNum__Field;

    private string orderNumberField;

    private string productCodeField;

    private decimal qtyOrderedField;

    private decimal qtyPackedField;

    private string statusField;

    private decimal totalPriceField;

    private string unitOfQuantityField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string ContainerNumber
    {
        get
        {
            return this.containerNumberField;
        }
        set
        {
            this.containerNumberField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string ContainerType
    {
        get
        {
            return this.containerTypeField;
        }
        set
        {
            this.containerTypeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Currency
    {
        get
        {
            return this.currencyField;
        }
        set
        {
            this.currencyField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Description
    {
        get
        {
            return this.descriptionField;
        }
        set
        {
            this.descriptionField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string DocEntry__
    {
        get
        {
            return this.docEntry__Field;
        }
        set
        {
            this.docEntry__Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public decimal ItemPrice
    {
        get
        {
            return this.itemPriceField;
        }
        set
        {
            this.itemPriceField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string LineNum__
    {
        get
        {
            return this.lineNum__Field;
        }
        set
        {
            this.lineNum__Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string OrderNumber
    {
        get
        {
            return this.orderNumberField;
        }
        set
        {
            this.orderNumberField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string ProductCode
    {
        get
        {
            return this.productCodeField;
        }
        set
        {
            this.productCodeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public decimal QtyOrdered
    {
        get
        {
            return this.qtyOrderedField;
        }
        set
        {
            this.qtyOrderedField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public decimal QtyPacked
    {
        get
        {
            return this.qtyPackedField;
        }
        set
        {
            this.qtyPackedField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string Status
    {
        get
        {
            return this.statusField;
        }
        set
        {
            this.statusField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public decimal TotalPrice
    {
        get
        {
            return this.totalPriceField;
        }
        set
        {
            this.totalPriceField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string UnitOfQuantity
    {
        get
        {
            return this.unitOfQuantityField;
        }
        set
        {
            this.unitOfQuantityField = value;
        }
    }
}

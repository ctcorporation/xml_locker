﻿using System.Collections.Generic;
using System.Linq;

namespace XML_Locker.Airco
{
    public class CommonToAirco
    {
        #region members
        const string arcTimeConst = "yyyy-MM-ddThh:mm:ss.mmm";
        string _fileName;
        #endregion


        #region properties
        public string CWXmlFile
        {
            get
            { return _fileName; }
            set
            { _fileName = value; }
        }
        public string OrderNo { get; private set; }

        #endregion

        #region constructors
        public CommonToAirco()
        {

        }
        #endregion
        public Shipments ConvertData(NodeFile commonXML)
        {
            var aircoXML = new Shipments();

            var shipments = new List<ShipmentsShipment>();
            foreach (var order in commonXML.Shipments)
            {
                var shipment = GetShipment(order);
                shipments.Add(shipment);
            }
            aircoXML.Items = shipments.ToArray();
            return aircoXML;
        }

        private ShipmentsShipment GetShipment(NodeFileShipment order)
        {
            var shipment = new ShipmentsShipment();
            shipment.Vendor = order.Vendor;
            shipment.ActualDeparture = GetCommonDate(order.Dates, DateElementDateType.Departure);
            shipment.ActualVolume = order.TotalVolume;
            shipment.ActualWeight = order.TotalWeight;
            shipment.Consignee = "AIRCO";
            shipment.ContainerMode = order.ContainerMode.ToString();
            shipment.DeliveryAddress = GetDeliveryAddress(order.Companies, CompanyElementCompanyType.DeliveryAddress);
            //shipment.DocEntry__ = ""; add in shipment
            shipment.EstimatedArrival = GetCommonDate(order.Dates, DateElementDateType.Arrival);
            shipment.EstimatedDeparture = GetCommonDate(order.Dates, DateElementDateType.Departure);
            shipment.GoodsDescription = order.GoodsDescription;
            shipment.IncoTerm = order.IncoTerms;
            shipment.PickupAddress = GetPickAddress(order.Companies, CompanyElementCompanyType.PickupAddress);
            shipment.ShipmentDate = GetCommonDate(order.Dates, DateElementDateType.Complete);
            shipment.ShipmentLines = GetShipmentLines(order, shipment);
            shipment.ShipmentNumber = order.ShipmentNo;
            shipment.ShipmentStatus = "SHIPPED";
            shipment.TransportMode = order.TransportMode.ToString();

            shipment.DeparturePort = shipment.PickupAddress != null ? shipment.PickupAddress.Port : "";
            shipment.DestinationPort = shipment.DeliveryAddress != null ? shipment.DeliveryAddress.Port : "";
            return shipment;
        }

        private string GetCommonDate(DateElement[] dates, DateElementDateType dateType)
        {
            var dateElement = (from d in dates
                               where (d.DateType == dateType)
                               select d).FirstOrDefault();
            if (dateElement == null)
            {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(dateElement.EstimateDate))
            {
                if (string.IsNullOrEmpty(dateElement.ActualDate))
                {
                    return string.Empty;
                }
                return dateElement.ActualDate;
            }
            return dateElement.EstimateDate;
        }
        private ShipmentsShipmentDeliveryAddress GetDeliveryAddress(CompanyElement[] companies, CompanyElementCompanyType companyType)
        {
            var companyElement = (from c in companies
                                  where (c.CompanyType == companyType)
                                  select c).FirstOrDefault();
            if (companyElement == null)
            {
                return null;
            }
            var company = new ShipmentsShipmentDeliveryAddress();
            company.Address1 = companyElement.Address1;
            company.Address2 = companyElement.Address2;
            company.Country = companyElement.CountryCode;
            company.Locality = companyElement.City;
            company.Port = companyElement.Address3;
            company.PostCode = companyElement.PostCode;
            company.Region = companyElement.State;
            return company;
        }
        private ShipmentsShipmentPickupAddress GetPickAddress(CompanyElement[] companies, CompanyElementCompanyType companyType)
        {
            var companyElement = (from c in companies
                                  where (c.CompanyType == companyType)
                                  select c).FirstOrDefault();
            if (companyElement == null)
            {
                return null;
            }
            var company = new ShipmentsShipmentPickupAddress();
            company.Address1 = companyElement.Address1;
            company.Address2 = companyElement.Address2;
            company.Country = companyElement.CountryCode;
            company.Locality = companyElement.City;
            company.Port = companyElement.Address3;
            company.PostCode = companyElement.PostCode;
            company.Region = companyElement.State;
            return company;
        }
        private ShipmentsShipmentShipmentLinesShipmentLine[] GetShipmentLines(NodeFileShipment ordshipment, ShipmentsShipment aircoshipment)
        {
            var shipmentsArray = new List<ShipmentsShipmentShipmentLinesShipmentLine>();
            if (ordshipment.ShipmentLines != null)
            {
                if (ordshipment.ShipmentLines.Count() > 0)
                {
                    foreach (var item in ordshipment.ShipmentLines.ToList())
                    {
                        var shipment = new ShipmentsShipmentShipmentLinesShipmentLine()
                        {
                            ContainerNumber = item.ContainerNumber,
                            ContainerType = item.ContainerType,
                            Currency = item.Currency,
                            Description = item.Description,
                            DocEntry__ = item.DocEntry,
                            ItemPrice = item.ItemPrice,
                            LineNum__ = item.LineNo,
                            OrderNumber = item.OrderNumber,
                            ProductCode = item.ProductCode,
                            QtyOrdered = item.QtyOrdered,
                            QtyPacked = item.QtyOrdered,
                            Status = item.Status,
                            TotalPrice = item.TotalPrice,
                            UnitOfQuantity = item.UnitOfQuantity
                        };

                        aircoshipment.DocEntry__ = shipment.DocEntry__;
                        shipmentsArray.Add(shipment);
                    }
                }
            }

            return shipmentsArray.ToArray();
        }
    }
}

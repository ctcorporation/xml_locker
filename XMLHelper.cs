﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace XMLLocker
{


    public static class XMLHelper
    {
        public static string NodeExists(this XElement parent, string elementName, string retVal = null)
        {
            var foundEl = parent.Element(elementName);
            if (foundEl != null)
            {
                return foundEl.Value;
            }
            return retVal;
        }

        public static XmlElement AsXmlElement<T>(this T o, XmlSerializerNamespaces ns = null, XmlSerializer serializer = null)
        {
            return o.AsXmlDocument(ns, serializer).DocumentElement;
        }

        public static XmlDocument AsXmlDocument<T>(this T o, XmlSerializerNamespaces ns = null, XmlSerializer serializer = null)
        {
            XmlDocument doc = new XmlDocument();
            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
            {
                new XmlSerializer(o.GetType()).Serialize(writer, o, ns ?? NoStandardXmlNamespaces());
            }

            //    doc = RemoveNameSpace(doc,ns);

            return doc;
        }

        public static XmlSerializerNamespaces NoStandardXmlNamespaces()
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", ""); // Disable the xmlns:xsi and xmlns:xsd lines.
            return ns;
        }

        public static string AsString(this XmlDocument xmlDoc)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter tx = new XmlTextWriter(sw))
                {
                    xmlDoc.WriteTo(tx);
                    string strXmlText = sw.ToString();
                    return strXmlText;
                }
            }
        }

        public static string GetNodeValue(XDocument file, string parent, string nodeToFind)
        {
            try
            {
                var node = file.Descendants(parent).Elements(nodeToFind).Single();
                if (node.Name == nodeToFind)
                {
                    return node.Value;
                }
                return string.Empty;
            }
            catch (Exception)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                Console.WriteLine("Error returning value: Parent='" + parent + "' NodeToFind='" + nodeToFind + "'");
                return string.Empty;
            }


        }

    }


}

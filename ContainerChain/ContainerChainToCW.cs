﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using XUE;

namespace XMLLocker.ContainerChain
{
    public class ContainerChainToCW
    {

        public class ConversionException : System.Exception
        {
            public ConversionException()
            {

            }
            public ConversionException(String message)
                : base(message)
            {

            }
            public ConversionException(String message, Exception innerException)
                : base(message, innerException)
            { }


        }
        #region Members
        public string OutputPath { get; set; }
        public ActivityUpdate Activity { get; set; }
        public vw_CustomerProfile Profile { get; set; }
        private string _errString;
        #endregion

        #region Constructors
        public ContainerChainToCW()
        {

        }

        public ContainerChainToCW(string outPath, ActivityUpdate activity)
        {
            Activity = activity;
            OutputPath = outPath;

        }
        #endregion

        #region Methods

        public ActivityUpdate GetActivityFromFile(string fileName)
        {
            ActivityUpdate result = new ActivityUpdate();

            if (File.Exists(fileName))
            {
                XmlSerializer xmlSer = new XmlSerializer(typeof(ActivityUpdate));
                using (StreamReader sr = new StreamReader(fileName))
                {
                    try
                    {
                        var activity = (ActivityUpdate)xmlSer.Deserialize(sr);
                        return activity;
                    }
                    catch (InvalidOperationException ex)
                    {
                        _errString = ex.GetType().Name + " found: " + ex.InnerException;
                        result = null;
                    }

                }

            }
            return null;
        }

        public void CreateEventFromActivity()
        {
            foreach (var _activity in Activity.ContainerActivity)
            {
                Event cwEvent = new Event();
                cwEvent.ContextCollection = CreateContextCollection(_activity).ToArray();
            }
            var xmlInterchange = CreateNewInterchange();
        }

        private List<Context> CreateContextCollection(ActivityUpdateContainerActivity activity)
        {
            List<DataTarget> dtColl = new List<DataTarget>();
            DataTarget dt = new DataTarget();
            if (!string.IsNullOrEmpty(activity.CustomerRef))
            {
                switch (activity.CustomerRef.Substring(0, 1))
                {
                    case "B":
                        dt.Type = "CustomsDeclaration";
                        break;
                    case "S":
                        dt.Type = "ForwardingShipment";
                        break;
                    case "C":
                        dt.Type = "ForwardingConsol";
                        break;
                }
                dt.Key = activity.CustomerRef;
            }
        }

        private object CreateNewInterchange()
        {
            Event xue = new Event();


        }

        private UniversalInterchange GetHeaderFromProfile()
        {
            UniversalInterchange xueInt = new UniversalInterchange();
            if (Profile != null)
            {
                XUE.UniversalInterchangeHeader header = new UniversalInterchangeHeader
                {
                    RecipientID = Profile.C_CODE,
                    SenderID = Profile.P_SENDERID
                };
                xueInt.Header = header;
                xueInt.version = "1.1";
                return xueInt;

            }
            return null;
        }

        #endregion




        public string DoConvert(String fileName, String senderid, String recipientid, String outputpath)
        {
            ActivityUpdate swiftActivity = new ActivityUpdate();
            string result = string.Empty;
            string xmlPath = Path.GetDirectoryName(fileName);
            if (Directory.Exists(xmlPath))
            {
                XmlSerializer xmlSwift = new XmlSerializer(typeof(ActivityUpdate));
                //ContainerEvents silkFile;
                StreamReader sr = new StreamReader(fileName);
                String xmlSource = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();
                try
                {
                    using (StringReader reader = new StringReader(xmlSource))
                    {
                        swiftActivity = (ActivityUpdate)xmlSwift.Deserialize(reader);
                        reader.Close();
                        reader.Dispose();
                    }

                }
                catch (Exception)
                {
                    result = "Operation|Invalid XSD. Checking Next";
                    return result;
                }
                foreach (ActivityUpdateContainerActivity contActivity in swiftActivity.ContainerActivity)
                {
                    try
                    {
                        UniversalInterchange interchange = new UniversalInterchange();
                        UniversalInterchangeHeader header = new UniversalInterchangeHeader();
                        header.SenderID = senderid;
                        header.RecipientID = recipientid;
                        interchange.Header = header;
                        interchange.version = "1.1";
                        UniversalInterchangeBody body = new UniversalInterchangeBody();
                        UniversalEventData bodydata = new UniversalEventData();
                        List<UniversalEventData> silkEventColl = new List<UniversalEventData>();
                        UniversalEventData silkEvent = new UniversalEventData();
                        silkEvent.version = "1.1";
                        silkEvent.xmlnsa = "http://www.cargowise.com/Schemas/Universal/2011/11";
                        Event silkevent = new Event();
                        DataContext dc = new DataContext();
                        List<DataTarget> dtColl = new List<DataTarget>();
                        DataTarget dt = new DataTarget();
                        dt.Type = "ForwardingShipment";
                        List<ActivityUpdateContainerActivity> updateContainerActivityColl = new List<ActivityUpdateContainerActivity>();
                        ActivityUpdateContainerActivity updateContainerActivity = new ActivityUpdateContainerActivity();
                        updateContainerActivity.ContainerNo = contActivity.ContainerNo;
                        switch (contActivity.CustomerRef.Substring(0, 1))
                        {
                            case "B":
                                dt.Type = "CustomsDeclaration";
                                break;
                            case "S":
                                dt.Type = "ForwardingShipment";
                                break;
                        }
                        if (contActivity.CustomerRef.Contains("/I"))
                        {
                            dt.Type = "LocalTransport";
                        }
                        dt.Key = contActivity.CustomerRef;

                        dtColl.Add(dt);
                        dc.DataTargetCollection = dtColl.ToArray();
                        silkevent.DataContext = dc;
                        silkevent.EventTime = contActivity.RequiredDate + "T" + contActivity.RequiredTime;
                        silkevent.IsEstimateSpecified = true;
                        silkevent.IsEstimate = false;
                        switch (contActivity.ActivityCode)
                        {
                            case "3310":
                                silkevent.EventType = "SLC";

                                break;
                            case "HL013":
                                silkevent.EventType = "DCF";
                                break;
                            case "HL009":
                                silkevent.EventType = "DHR";
                                break;
                            case "HL002":
                                silkevent.EventType = "GOU";
                                break;
                            case "HL011":
                                silkevent.EventType = "DCF";
                                break;
                            case "HL012":
                                silkevent.EventType = "GIN";
                                break;
                            case "HL016":
                                silkevent.EventType = "Z03";
                                break;
                            default:
                                return result = "Operation|Event Type not mapped";
                                break;
                        }
                        CodeDescriptionPair cpEventType = new CodeDescriptionPair();
                        cpEventType.Code = "DEX";
                        dc.EventType = cpEventType;
                        List<Context> ctColl = new List<Context>();
                        Context ct = new Context();
                        ContextType ctype = new ContextType();
                        ctype.Value = "ContainerNumber";
                        ct.Type = ctype;
                        //ct.Type = (ContextType)"ContainerNumber";
                        ct.Value = contActivity.ContainerNo.Replace(" ", string.Empty);
                        result = "Container|" + ct.Value;
                        result += "*Event|" + silkevent.EventType;
                        ctColl.Add(ct);
                        silkevent.ContextCollection = ctColl.ToArray();
                        silkEvent.Event = silkevent;
                        silkEventColl.Add(silkEvent);
                        bodydata.Event = silkevent;
                        body.BodyField = bodydata;
                        bodydata.version = "1.1";
                        interchange.Body = body;
                        String cwXML = Path.Combine(outputpath, "SWIFT" + contActivity.ContainerNo + DateTime.Now.ToString("yyyyMMddhhmm") + ".xml");
                        int iFileCount = 0;
                        while (File.Exists(cwXML))
                        {
                            iFileCount++;
                            cwXML = Path.Combine(outputpath, "SWIFT" + contActivity.ContainerNo + DateTime.Now.ToString("yyyyMMddhhmm") + iFileCount + ".xml");
                        }
                        Stream outputSilk = File.Open(cwXML, FileMode.Create);
                        StringWriter writer = new StringWriter();
                        XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                        xSer.Serialize(outputSilk, interchange);
                        outputSilk.Flush();
                        outputSilk.Close();
                    }
                    catch (Exception ex)
                    {
                        result = "Operation|Error: " + ex.GetType().Name + " " + ex.Message;
                        return result;
                    }
                    sr.Close();
                }
                //for (int i = 0; i < swiftActivity.ContainerActivity.Count(); i++)
                //{
                //    try
                //    {
                //        UniversalInterchange interchange = new UniversalInterchange();
                //        UniversalInterchangeHeader header = new UniversalInterchangeHeader();
                //        header.SenderID = senderid;
                //        header.RecipientID = recipientid;
                //        interchange.Header = header;
                //        interchange.version = "1.1";
                //        UniversalInterchangeBody body = new UniversalInterchangeBody();
                //        UniversalEventData bodydata = new UniversalEventData();
                //        List<UniversalEventData> silkEventColl = new List<UniversalEventData>();
                //        UniversalEventData silkEvent = new UniversalEventData();
                //        silkEvent.version = "1.1";
                //        silkEvent.xmlnsa = "http://www.cargowise.com/Schemas/Universal/2011/11";
                //        Event silkevent = new Event();
                //        DataContext dc = new DataContext();
                //        List<DataTarget> dtColl = new List<DataTarget>();
                //        DataTarget dt = new DataTarget();
                //        dt.Type = "ForwardingShipment";
                //        List<ActivityUpdateContainerActivity> updateContainerActivityColl = new List<ActivityUpdateContainerActivity>();
                //        ActivityUpdateContainerActivity updateContainerActivity = new ActivityUpdateContainerActivity();
                //        updateContainerActivity.ContainerNo = swiftActivity.ContainerActivity[i].ContainerNo;
                //        switch (swiftActivity.ContainerActivity[i].CustomerRef.Substring(0, 1))
                //        {
                //            case "B":
                //                dt.Type = "CustomsDeclaration";
                //                break;
                //            case "S":
                //                dt.Type = "ForwardingShipment";
                //                break;
                //        }
                //        dt.Key = swiftActivity.ContainerActivity[i].CustomerRef;

                //        dtColl.Add(dt);
                //        dc.DataTargetCollection = dtColl.ToArray();
                //        silkevent.DataContext = dc;
                //        silkevent.EventTime = swiftActivity.ContainerActivity[i].RequiredDate + "T" + swiftActivity.ContainerActivity[i].RequiredTime;
                //        silkevent.IsEstimateSpecified = true;
                //        silkevent.IsEstimate = false;
                //        switch (swiftActivity.ContainerActivity[i].ActivityCode)
                //        {
                //            case "3310":
                //                silkevent.EventType = "ESC";

                //                break;
                //            case "HL013":
                //                silkevent.EventType = "DCF";
                //                break;
                //            case "HL009":
                //                silkevent.EventType = "DHR";
                //                break;
                //            case "HL002":
                //                silkevent.EventType = "GOU";
                //                break;
                //            case "HL011":
                //                silkevent.EventType = "DCF";
                //                break;
                //            case "HL012":
                //                silkevent.EventType = "CPO";
                //                break;
                //            case "HL016":
                //                silkevent.EventType = "Z03";
                //                break;
                //            default:
                //                throw new ConversionException("Event Type not mapped");
                //        }
                //        CodeDescriptionPair cpEventType = new CodeDescriptionPair();
                //        cpEventType.Code = "DEX";
                //        dc.EventType = cpEventType;
                //        List<Context> ctColl = new List<Context>();
                //        Context ct = new Context();
                //        ContextType ctype = new ContextType();
                //        ctype.Value = "ContainerNumber";
                //        ct.Type = ctype;
                //        //ct.Type = (ContextType)"ContainerNumber";
                //        ct.Value = swiftActivity.ContainerActivity[i].ContainerNo;
                //        ctColl.Add(ct);
                //        silkevent.ContextCollection = ctColl.ToArray();
                //        silkEvent.Event = silkevent;
                //        silkEventColl.Add(silkEvent);
                //        bodydata.Event = silkevent;
                //        body.BodyField = bodydata;
                //        bodydata.version = "1.1";
                //        interchange.Body = body;
                //        String cwXML = Path.Combine(outputpath, "SWIFT" + swiftActivity.ContainerActivity[i].ContainerNo + swiftActivity.ContainerActivity[i].CustomerRef + ".xml");
                //        int iFileCount = 0;
                //        while (File.Exists(cwXML))
                //        {
                //            iFileCount++;
                //            cwXML = Path.Combine(outputpath, "SWIFT" + swiftActivity.ContainerActivity[i].ContainerNo + swiftActivity.ContainerActivity[i].CustomerRef + iFileCount + ".xml");
                //        }
                //        Stream outputSilk = File.Open(cwXML, FileMode.Create);
                //        StringWriter writer = new StringWriter();
                //        XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                //        xSer.Serialize(outputSilk, interchange);
                //        outputSilk.Flush();
                //        outputSilk.Close();
                //    }
                //    catch (Exception ex)
                //    {

                //        throw new ConversionException(ex.Message);                      
                //    }
                //    sr.Close();
                //}
                result += "*Operation|Conversion Complete";
                return result;
            }
            else
            {
                result = "Operation|XML Path not found";
                return result;
            }

        }
    }
}

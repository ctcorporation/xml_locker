﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using XML_Locker;
using XMLLocker.Cargowise.XUE;

namespace CNodeBE.Classes
{
    public class MaximasConversion
    {
        #region members
        string _errMsg;
        string _connString;
        const string dateFormat = "yyyy-MM-ddTHH:mm:ss";
        #endregion

        #region properties

        public string XmlFile { get; set; }
        public string ErrMsg
        {
            get
            { return _errMsg; }
            set
            {
                _errMsg = value;
            }
        }
        public vw_CustomerProfile Profile { get; set; }
        public string Schema { get; set; }
        public string OutPath { get; set; }
        public string ConnString
        {
            get
            {
                return _connString;
            }
            set
            {
                _connString = value;
            }
        }
        #endregion

        #region constructors
        public MaximasConversion(string connstring, string xmlFile, vw_CustomerProfile cust)
        {
            ConnString = connstring;
            XmlFile = xmlFile;
            Profile = cust;
        }
        #endregion

        #region methods
        public string CreateEvents()
        {
            var conversion = GetConversion(Profile.P_PARAMLIST);
            if (string.IsNullOrEmpty(conversion))
            {
                _errMsg += "Error: Unable to identify Conversion type Maximas/Container Chain. Add 'Conversion' Parameter to Profile";
                return _errMsg;
            }
            string result = string.Empty;
            using (FileStream fStream = new FileStream(XmlFile, FileMode.Open))
            {
                XmlSerializer ccReader = new XmlSerializer(typeof(XMLLocker.ContainerChain.ActivityUpdate));

                switch (conversion)
                {
                    case "C":
                        result = CCActivity(fStream);

                        break;
                    case "M":
                        result = MaxActivity(fStream);
                        break;


                }
                fStream.Close();

            }
            return result;
        }

        private string MaxActivity(FileStream fs)
        {
            var ccReader = new XmlSerializer(typeof(XMLLocker.Maximas.ActivityUpdate));
            var activity = (XMLLocker.Maximas.ActivityUpdate)ccReader.Deserialize(fs);
            if (activity != null)
            {
                foreach (var containerActivity in activity.ContainerActivity)
                {
                    var eventData = CreateEventFromActivity(containerActivity);
                    if (eventData == null)
                    {
                        return _errMsg;
                    }
                    var eventHeader = CreateHeader();
                    var eventFile = new UniversalInterchange
                    {
                        Header = eventHeader,
                        Body = new UniversalInterchangeBody { BodyField = eventData },
                        version = "1.1"
                    };
                    FileBuilder fileBuilder = new FileBuilder(OutPath);
                    try
                    {
                        fileBuilder.BuildXmlFile<UniversalInterchange>(new XmlSerializerNamespaces(), "http://www.cargowise.com/Schemas/Universal/2011/11", eventFile, Profile.P_SENDERID, "-ACTIVITY-" + containerActivity.CustomerRef, ".xml");
                        return "Event " + containerActivity.ActivityDesc + "(" + containerActivity.CustomerRef + "/" + containerActivity.ContainerNo + "). File created Ok.";
                    }
                    catch (Exception ex)
                    {
                        return _errMsg += "Error: " + ex.GetType().Name + ": " + ex.InnerException + Environment.NewLine;
                    }
                }
            }
            return null;
        }

        private string CCActivity(FileStream fs)
        {
            var ccReader = new XmlSerializer(typeof(XMLLocker.ContainerChain.ActivityUpdate));
            var activity = (XMLLocker.ContainerChain.ActivityUpdate)ccReader.Deserialize(fs);
            if (activity != null)
            {
                foreach (var containerActivity in activity.ContainerActivity)
                {
                    var eventData = CreateEventFromActivity(containerActivity);
                    if (eventData == null)
                    {
                        return _errMsg;
                    }
                    var eventHeader = CreateHeader();
                    var eventFile = new UniversalInterchange
                    {
                        Header = eventHeader,
                        Body = new UniversalInterchangeBody { BodyField = eventData },
                        version = "1.1"
                    };
                    FileBuilder fileBuilder = new FileBuilder(OutPath);
                    try
                    {
                        fileBuilder.BuildXmlFile<UniversalInterchange>(new XmlSerializerNamespaces(), "http://www.cargowise.com/Schemas/Universal/2011/11", eventFile, Profile.P_SENDERID ,"-ACTIVITY-" + containerActivity.CustomerRef, ".xml");
                        return "Event " + containerActivity.ActivityDesc + "(" + containerActivity.CustomerRef + "/" + containerActivity.ContainerNo + "). File created Ok.";
                    }
                    catch (Exception ex)
                    {
                        return _errMsg += "Error: " + ex.GetType().Name + ": " + ex.InnerException + Environment.NewLine;
                    }


                }
            }
            return null;

        }
        private string GetConversion(string param)
        {
            if (string.IsNullOrEmpty(param))
            {
                return string.Empty;
            }
            if (param.StartsWith("Conversion"))
            {
                var pL = param.Split('|');
                // Each Parameter is separated by '|'
                var p = pL[0].Split(':');

                return string.IsNullOrEmpty(p[1]) ? string.Empty : p[1];
            }
            return string.Empty;
        }

        private UniversalInterchangeHeader CreateHeader()
        {
            return new UniversalInterchangeHeader
            {
                RecipientID = Profile.P_RECIPIENTID,
                SenderID = Profile.P_SENDERID
            };
        }

        private UniversalEventData CreateEventFromActivity(XMLLocker.Maximas.ActivityUpdateContainerActivity containerActivity)
        {
            UniversalEventData ud = new UniversalEventData();
            Event cwEvent = new Event();
            cwEvent.DataContext = new DataContext
            {
                DataProvider = Profile.P_SENDERID
            };
            cwEvent.DataContext.DataTargetCollection = GetTargetCollection(containerActivity.CustomerRef).ToArray();
            IEnumDTO enumDTO = new EnumDTO(ConnString);
            try
            {
                var eventCode = enumDTO.GetEnum("CCACTIVITY", containerActivity.ActivityCode);
                if (string.IsNullOrEmpty(eventCode))
                {
                    _errMsg += "Error: Code " + containerActivity.ActivityCode + " is not Mapped. Rejecting message";
                    return null;
                }
                cwEvent.EventType = !string.IsNullOrEmpty(eventCode) ? eventCode : string.Empty;

                if (!string.IsNullOrEmpty(containerActivity.EDIDateTime))
                {
                    DateTime dt = new DateTime();
                    if (DateTime.TryParse(containerActivity.EDIDateTime, out dt))
                    {
                        cwEvent.EventTime = dt.ToString(dateFormat);
                    }
                }
                cwEvent.IsEstimateSpecified = true;
                cwEvent.IsEstimate = false;
                List<Context> contextColl = new List<Context>();
                contextColl.Add(new Context
                {
                    Type = new ContextType { Value = "ContainerNumber" },
                    Value = containerActivity.ContainerNo

                });
                cwEvent.ContextCollection = contextColl.ToArray();
                ud.Event = cwEvent;
                return ud;
            }
            catch (Exception ex)
            {
                _errMsg += "Error" + ex.GetType().Name + ": " + ex.InnerException + Environment.NewLine;
                return null;
            }
        }
        private UniversalEventData CreateEventFromActivity(XMLLocker.ContainerChain.ActivityUpdateContainerActivity containerActivity)
        {
            UniversalEventData ud = new UniversalEventData();
            Event cwEvent = new Event();
            cwEvent.DataContext = new DataContext
            {
                DataProvider = Profile.P_SENDERID
            };
            cwEvent.DataContext.DataTargetCollection = GetTargetCollection(containerActivity.CustomerRef).ToArray();
            IEnumDTO enumDTO = new EnumDTO(ConnString);
            try
            {
                var eventCode = enumDTO.GetEnum("CCACTIVITY", containerActivity.ActivityCode);
                if (string.IsNullOrEmpty(eventCode))
                {
                    _errMsg += "Error: Code " + containerActivity.ActivityCode + " is not Mapped. Rejecting message";
                    return null;
                }
                cwEvent.EventType = !string.IsNullOrEmpty(eventCode) ? eventCode : string.Empty;

                if (!string.IsNullOrEmpty(containerActivity.EventDateTime))
                {
                    DateTime dt = new DateTime();
                    if (DateTime.TryParse(containerActivity.EventDateTime, out dt))
                    {
                        cwEvent.EventTime = dt.ToString(dateFormat);
                    }
                }
                cwEvent.IsEstimateSpecified = true;
                cwEvent.IsEstimate = false;
                List<Context> contextColl = new List<Context>();
                contextColl.Add(new Context
                {
                    Type = new ContextType { Value = "ContainerNumber" },
                    Value = containerActivity.ContainerNo

                });
                cwEvent.ContextCollection = contextColl.ToArray();
                ud.Event = cwEvent;
                return ud;
            }
            catch (Exception ex)
            {
                _errMsg += "Error" + ex.GetType().Name + ": " + ex.InnerException + Environment.NewLine;
                return null;
            }
        }

        private List<DataTarget> GetTargetCollection(string customerRef)
        {
            List<DataTarget> dcColl = new List<DataTarget>();
            DataTarget dt = new DataTarget();
            switch (customerRef.Substring(0, 1))
            {
                case "B":
                    dt.Type = "CustomsDeclaration";
                    break;

                case "S":
                    dt.Type = "ForwardingShipment";
                    break;

                case "C":
                    dt.Type = "ForwardingConsol";
                    break;
            }
            if (customerRef.Contains("/I") || customerRef.Contains("/E"))
            {
                dt.Type = "LocalTransport";
            }
            dt.Key = customerRef;
            dcColl.Add(dt);
            return dcColl;


        }
        #endregion

        #region helpers

        #endregion
    }
}

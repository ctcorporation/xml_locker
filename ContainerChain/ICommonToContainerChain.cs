﻿using XMLLocker.CTC;

namespace XMLLocker.ContainerChain
{
    interface ICommonToContainerChain
    {
        NodeFile CommonXML { get; set; }
        string BuildCCFile();
        Root ConvertFromCommon(NodeFileTimeSlotRequest common);
    }
}
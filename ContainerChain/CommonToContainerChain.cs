﻿
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace XMLLocker.ContainerChain
{
    public class CommonToContainerChain : ICommonToContainerChain
    {
        public NodeFile CommonXML
        {
            get
            {
                return _commonXml;
            }
            set
            {
                this._commonXml = value;
            }
        }

        public string ErrorString
        {
            get; set;

        }

        public Ivw_CustomerProfile Profile
        {
            get { return this._profile; }
            set { this._profile = value; }
        }

        private string _outputLocation;
        private string _dehireDepotcode;
        private Ivw_CustomerProfile _profile;

        private NodeFile _commonXml { get; set; }

        public CommonToContainerChain(string outLocation)
        {
            _outputLocation = outLocation;
        }
        public CommonToContainerChain(string outLocation, NodeFile commonxml)
        {
            _outputLocation = outLocation;
            _commonXml = commonxml;
        }

        public string BuildCCFile()
        {
            string result = string.Empty;

            var ccNS = new XmlSerializerNamespaces();
            ccNS.Add("ns0", "http://cNode.ctc.net.au/09/2019");
            foreach (var timeSlot in _commonXml.TimeSlotRequests)
            {
                var ccFile = ConvertFromCommon(timeSlot);
                if (ccFile == null)
                {
                    return ErrorString;
                }
                var pathresult = Path.Combine(_outputLocation, _commonXml.IdendtityMatrix.SenderId + "CCMAX" + "-" + timeSlot.ShipmentNo + ".xml");
                int iFileCount = 0;
                while (File.Exists(pathresult))
                {
                    iFileCount++;
                    pathresult = Path.Combine(_outputLocation, _commonXml.IdendtityMatrix.SenderId + "CCMAX" + "-" + timeSlot.ShipmentNo + "-" + iFileCount + ".xml");
                }
                using (Stream outputCW = File.Open(pathresult, FileMode.Create))
                {
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(Root));
                    xSer.Serialize(outputCW, ccFile, ccNS);
                    outputCW.Flush();
                    outputCW.Close();
                }
                result = pathresult;

            }

            return result;
        }

        public Root ConvertFromCommon(NodeFileTimeSlotRequest common)
        {
            Root result = new Root();
            RootOrderHeader orderHeader = new RootOrderHeader();
            orderHeader.CustomerRef = common.ShipmentNo;
            orderHeader.OceanBLNo = common.OceanBill;
            switch (common.ShipmentType)
            {
                case NodeFileTimeSlotRequestShipmentType.FCL:
                    orderHeader.ShipmentType = RootOrderHeaderShipmentType.FCL;
                    break;
                case NodeFileTimeSlotRequestShipmentType.LCL:
                    orderHeader.ShipmentType = RootOrderHeaderShipmentType.LCL;
                    break;
                case NodeFileTimeSlotRequestShipmentType.EMP:
                    orderHeader.ShipmentType = RootOrderHeaderShipmentType.Empty;
                    break;
            }
            switch (common.Direction)
            {
                case NodeFileTimeSlotRequestDirection.I:
                    orderHeader.JobType = RootOrderHeaderJobType.I;
                    break;
                case NodeFileTimeSlotRequestDirection.E:
                    orderHeader.JobType = RootOrderHeaderJobType.E;
                    break;
            }
            orderHeader.VesselId = common.VesselNo;
            orderHeader.VoyageNo = common.Voyage;
            orderHeader.EDIdatetime = string.Format("{0:yyyy-MM-dd hh:mm:ss}", DateTime.Now);
            orderHeader.DischargePort = common.DischargePort;
            orderHeader.JobCategory = "WF-YD-CU-MT";
            switch (common.TransportType)
            {
                case "SDL":
                    orderHeader.TransportType = RootOrderHeaderTransportType.SIDELOADER;
                    orderHeader.JobCategory = "WF-YD-CU-YD-MT";
                    break;
                case "TRL":
                    orderHeader.TransportType = RootOrderHeaderTransportType.DROPOFF;

                    break;
                case "LOF":
                    orderHeader.TransportType = RootOrderHeaderTransportType.DROPOFF;

                    break;
                case "WUP":
                    orderHeader.TransportType = RootOrderHeaderTransportType.STANDARD;

                    break;
                default:
                    orderHeader.TransportType = RootOrderHeaderTransportType.STANDARD;

                    break;
            }
            string senderid = string.Empty;
            string deliveryAddressCode = string.Empty;
            string deliveryAddressName = string.Empty;
            string companycode = string.Empty;
            string branchcode = string.Empty;
            string emptyCode = string.Empty;
            if (_profile.P_PARAMLIST != null)
            {
                var pList = _profile.P_PARAMLIST.Split('|').ToList();
                if (pList.Count > 0)
                {
                    senderid = GetOtherAccountCodeFromParamList(pList, "SenderId");
                    deliveryAddressCode = GetOtherAccountCodeFromParamList(pList, "DeliveryAddressCode");

                    companycode = GetOtherAccountCodeFromParamList(pList, "CompanyCode");
                    branchcode = GetOtherAccountCodeFromParamList(pList, "BranchCode");
                    emptyCode = GetOtherAccountCodeFromParamList(pList, "DehireCode");


                }

            }
            orderHeader.SenderId = !string.IsNullOrEmpty(senderid) ? senderid : _commonXml.IdendtityMatrix.SenderId;
            if (string.IsNullOrEmpty(companycode) || string.IsNullOrEmpty(branchcode))
            {
                ErrorString += "ERROR: Branch Code or Company Code Not Found. Message will be rejected." + Environment.NewLine;
                return null;
            }

            orderHeader.CompanyCode = companycode;
            orderHeader.BranchCode = branchcode;
            orderHeader.ShippingAgent = "TBA";
            if (string.IsNullOrEmpty(deliveryAddressCode))
            {
                var delivery = GetOrgCodeFromCommon(common.Companies, CompanyElementCompanyType.DeliveryAddress);
                if (delivery != null)
                {
                    orderHeader.DeliveryAddress = delivery.CompanyOrgCode;
                    orderHeader.ConsigneeName = delivery.CompanyName;
                }
            }
            orderHeader.ConsigneeCode = deliveryAddressCode;

            switch (common.ContainerMode)
            {
                case "FCL":
                    orderHeader.ShipmentType = RootOrderHeaderShipmentType.FCL;
                    break;
                case "LCL":
                    orderHeader.ShipmentType = RootOrderHeaderShipmentType.LCL;
                    break;
            }

            result.OrderHeader = orderHeader;
            var requiredDate = GetDateFromCommon(common.Dates, DateElementDateType.InStoreDateReq);
            result.Containers = GetContainersFromCommon(common.Containers, requiredDate).ToArray();
            return result;
        }

        private string GetOtherAccountCodeFromParamList(List<string> param, string accountType)
        {
            var pair = (from p in param
                        where p.Contains(accountType)
                        select p).FirstOrDefault();
            if (pair != null)
            {
                string[] code = pair.Split(':');

                return !string.IsNullOrEmpty(code[1]) ? code[1] : null;
            }
            ErrorString += "ERROR: Other Account Code Not Found: " + accountType + Environment.NewLine;
            return null;
        }

        private List<RootContainersContainerDetail> GetContainersFromCommon(ContainerElement[] containers, string requiredDate)
        {
            List<RootContainersContainerDetail> result = new List<RootContainersContainerDetail>();
            int iCount = 1;
            foreach (var cont in containers)
            {
                RootContainersContainerDetail container = new RootContainersContainerDetail();
                container.ContainerNo = cont.ContainerNo;
                container.ContainerSeq = string.Format("{0:000}", iCount);
                container.SealNo = cont.ContainerNo;
                container.TypeSize = cont.ISOCode;
                container.UNCode = cont.UNCode;
                container.GrossWeight = cont.GrossWeight;
                container.GrossWeightSpecified = true;
                container.RequiredDate = requiredDate;
                // Hard Coded for the time being. 
                container.DepotCode = "TBAMT";
                switch (cont.EquipmentNeeded)
                {
                    case ContainerElementEquipmentNeeded.SDL:
                        container.TrailerType = "SL";
                        break;
                    default:
                        container.TrailerType = "SK";
                        break;
                }

                switch (cont.Commodity)
                {
                    case ContainerElementCommodity.GEN:
                        container.CargoType = RootContainersContainerDetailCargoType.GEN;
                        break;
                    case ContainerElementCommodity.HAZ:
                        container.CargoType = RootContainersContainerDetailCargoType.HAZ;
                        break;
                }
                result.Add(container);
                iCount++;
            }
            return result;
        }

        private string GetDateFromCommon(DateElement[] dates, DateElementDateType required)
        {
            var date = (from d in dates
                        where d.DateType == required
                        select d.EstimateDate).FirstOrDefault();
            if (date != null)
            {
                return date;
            }
            ErrorString += "ERROR: Date Value not found: " + required.ToString() + Environment.NewLine;
            return null;
        }

        private CompanyElement GetOrgCodeFromCommon(CompanyElement[] companies, CompanyElementCompanyType compType)
        {
            var comp = (from x in companies
                        where x.CompanyType == compType
                        select x).FirstOrDefault();
            if (comp != null)
            {
                return comp;

            }
            ErrorString += "ERROR: Org Code Not Found: " + compType.ToString() + Environment.NewLine;
            return null;
        }

        private CompanyElement GetOrgNameFromCommon(CompanyElement[] companies, CompanyElementCompanyType compType)
        {
            var comp = (from x in companies
                        where x.CompanyType == compType
                        select x).FirstOrDefault();
            if (comp != null)
            {
                return comp;

            }
            ErrorString += "ERROR: Org Code Not Found: " + compType.ToString() + Environment.NewLine;
            return null;
        }

    }
}

﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using NodeResources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using XUE;

namespace XMLLocker.ContainerChain
{
    public class ContainerChainToCW
    {
        //Custom Error Handler
        public class ConversionException : System.Exception
        {
            public ConversionException()
            {

            }
            public ConversionException(String message)
                : base(message)
            {

            }
            public ConversionException(String message, Exception innerException)
                : base(message, innerException)
            { }


        }
        #region Members
        public string OutputPath { get; set; }
        public ActivityUpdate Activity { get; set; }
        public NodeData.Models.Ivw_CustomerProfile Profile { get; set; }

        public string ConnString { get; set; }
        private string _errString;

        #endregion

        #region Constructors
        public ContainerChainToCW()
        {

        }

        public ContainerChainToCW(string outPath, string connstring, string fileName)
        {
            OutputPath = outPath;
            ConnString = connstring;
            GetActivityFromFile(fileName);
        }
        public ContainerChainToCW(string outPath, string connstring, string fileName, NodeData.Models.Ivw_CustomerProfile profile)
        {
            OutputPath = outPath;
            ConnString = connstring;
            Profile = profile;
            GetActivityFromFile(fileName);
        }
        public ContainerChainToCW(string outPath, ActivityUpdate activity)
        {
            Activity = activity;
            OutputPath = outPath;

        }
        #endregion

        #region Methods

        public void GetActivityFromFile(string fileName)
        {
            ActivityUpdate result = new ActivityUpdate();

            if (File.Exists(fileName))
            {
                XmlSerializer xmlSer = new XmlSerializer(typeof(ActivityUpdate));
                using (StreamReader sr = new StreamReader(fileName))
                {
                    try
                    {
                        var activity = (ActivityUpdate)xmlSer.Deserialize(sr);
                        Activity = activity;
                    }
                    catch (InvalidOperationException ex)
                    {
                        _errString = ex.GetType().Name + " found: " + ex.InnerException;
                        result = null;
                    }

                }

            }

        }

        public bool CreateEventFromActivity()
        {
            if (Activity != null)
            {
                // Loop through the Container Activities in the Activity Object

                foreach (var _activity in Activity.ContainerActivity)
                {
                    Event cwEvent = new Event();
                    // Create the Data Context
                    cwEvent.DataContext = CreateContextCollection(_activity);
                    // If there is no date then ignore everything
                    string eventDate = string.Empty;

                    IEnumDTO enumDTO = new EnumDTO(ConnString);
                    //Find the Cargowise Code from the Container Chain Code.
                    var eventCode = enumDTO.GetEnum("CCACTIVITY", _activity.ActivityCode);
                    switch (eventCode)
                    {
                        case "DHR":
                            eventDate = _activity.DehireDate;
                            break;
                        case "SLC":
                            eventDate = _activity.PortTimeSlotDate;
                            break;
                        default:
                            eventDate = _activity.RequiredDate;
                            break;
                    }
                    if (!string.IsNullOrEmpty(eventDate))
                    {
                        var time = !string.IsNullOrEmpty(_activity.RequiredTime) ? _activity.RequiredTime : "00:00:00";
                        // Format the DateTime for Cargowise XML
                        cwEvent.EventTime = eventDate + "T" + time;
                        cwEvent.IsEstimateSpecified = true;
                        cwEvent.IsEstimate = false;
                    }
                    if (eventCode != null)
                    {
                        cwEvent.EventType = eventCode;
                    }
                    else
                    {
                        _errString = "Operation|Event Type not mapped";
                    }
                    //Add the Context (Container Number) to the Event
                    List<Context> ctColl = new List<Context> {
                         new Context {
                            Type =new ContextType{
                                            Value ="ContainerNumber"},
                            Value = _activity.ContainerNo
                                }
                        };
                    cwEvent.ContextCollection = ctColl.ToArray();
                    //Create the Interchange header and attach the Event
                    var interchange = GetHeaderFromProfile();
                    interchange.Body.BodyField.Event = cwEvent;
                    interchange.Body.BodyField.Event.DataContext.DataProvider = interchange.Header.SenderID;
                    //Build the Universal Event File.
                    if (BuildFile(interchange, _activity.ContainerNo))
                    {
                        return true;
                    }

                }
            }
            return false;
        }

        private bool BuildFile(UniversalInterchange interchange, string containerNO)
        {
            //Create the Unique Name
            String cwXML = Path.Combine(OutputPath, Profile.C_CODE + "-ACT-" + containerNO + DateTime.Now.ToString("yyyyMMddhhmm") + ".xml");
            int iFileCount = 0;
            try
            {
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    //Increment the File Counter until number has not been used. 
                    cwXML = Path.Combine(OutputPath, Profile.C_CODE + "-ACT-" + containerNO + DateTime.Now.ToString("yyyyMMddhhmm") + iFileCount + ".xml");
                }
                using (Stream outputSilk = File.Open(cwXML, FileMode.Create))
                {
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                    xSer.Serialize(outputSilk, interchange);
                    outputSilk.Flush();
                    outputSilk.Close();
                }
            }
            catch (Exception ex)
            {
                _errString = ex.GetType().Name + " Error Found:" + ex.Message;
                return false;
            }

            return true;


        }

        private DataContext CreateContextCollection(ActivityUpdateContainerActivity activity)
        {
            List<DataTarget> dtColl = new List<DataTarget>();
            DataTarget dt = new DataTarget();
            // Build the Data Target from the activity Refernce Number
            if (!string.IsNullOrEmpty(activity.CustomerRef))
            {
                switch (activity.CustomerRef.Substring(0, 1))
                {
                    case "B":
                        dt.Type = "CustomsDeclaration";
                        break;
                    case "S":
                        dt.Type = "ForwardingShipment";
                        break;
                    case "C":
                        dt.Type = "ForwardingConsol";
                        break;
                }
                if (activity.CustomerRef.Contains("/I"))
                {
                    dt.Type = "LocalTransport";
                }

                dt.Key = activity.CustomerRef;
                dtColl.Add(dt);
            }
            DataContext dc = new DataContext { DataTargetCollection = dtColl.ToArray() };
            dc.ActionPurpose = new CodeDescriptionPair { Code = "CTC" };
            
            return dc;
        }

        private UniversalInterchange GetHeaderFromProfile()
        {
            UniversalInterchange xueInt = new UniversalInterchange();
            if (Profile != null)
            {
                //Attach the Sender and Recipient based on the Profile. 
                XUE.UniversalInterchangeHeader header = new UniversalInterchangeHeader
                {
                    RecipientID = Profile.C_CODE,
                    SenderID = Profile.P_SENDERID
                };
                xueInt.Body = new UniversalInterchangeBody();
                xueInt.Body.BodyField = new UniversalEventData();
                xueInt.Header = header;
                xueInt.version = "1.1";
                return xueInt;

            }
            return null;
        }

        #endregion



    }
}

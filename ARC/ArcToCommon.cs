﻿using CTCLogging;
using NodeData.Models;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace XMLLocker.ARC
{
    public class ArcToCommon
    {

        private string logpath;

        public string LogPath
        {
            get { return this.logpath; }
            set { this.logpath = value; }
        }

        public ArcToCommon()
        {


        }
        public ArcToCommon(string path)
        {
            LogPath = path;
        }

        public NodeFile ConvertData(string fileName, Ivw_CustomerProfile profile)
        {
            FileInfo processingFile = new FileInfo(fileName);

            XDocument arcFile = XDocument.Load(fileName);

            var companyList = GetCompanyListFromArc(arcFile);

            NodeFileTrackingOrder _order = new NodeFileTrackingOrder();
            _order.Companies = companyList.ToArray();
            _order.InvoiceNo = GetNodeValue(arcFile, "Documents", "U_PROFORMA");
            _order.OrderNo = GetNodeValue(arcFile, "Documents", "DocNum");
            _order.Dates = GetDatesFromArcOrder(arcFile).ToArray();
            var inco = GetNodeValue(arcFile, "Documents", "U_INCOTERMS");

            _order.IncoTerms = inco == null ? null : inco.Substring(0, 3);
            _order.Currency = GetNodeValue(arcFile, "Documents", "DocCurrency");
            _order.OrderStatus = "INC";
            var cont = GetNodeValue(arcFile, "Documents", "U_CONTAINERTYPE");
            _order.TransportMode = cont == null ? null : cont.Substring(0, 3);
            List<OrderLineElement> ordLines = new List<OrderLineElement>();
            ordLines = GetOrderLinesFromArcOrder(arcFile);
            if (ordLines == null)
            {
                return null;
            }
            else
            {
                _order.OrderLines = ordLines.ToArray();
            }

            NodeFile nodeFile = new NodeFile();
            //NodeFileOperation nodeFileOperation = new NodeFileOperation();
            // nodeFileOperation.TrackingOrders = new List<NodeFileOperationTrackingOrder> { _order }.ToArray();

            NodeFileIdendtityMatrix identityMatrix = new NodeFileIdendtityMatrix();
            identityMatrix.CustomerId = profile.C_CODE;
            identityMatrix.DocumentIdentifier = _order.OrderNo;
            identityMatrix.SenderId = profile.P_SENDERID;
            identityMatrix.DocumentType = "TrackingOrder";
            identityMatrix.FileDateTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            identityMatrix.OriginalFileName = fileName;
            nodeFile.IdendtityMatrix = identityMatrix;

            //added Identity matrix to Tracking order
            _order.IdentityMatrix = identityMatrix;

            nodeFile.TrackingOrders = new List<NodeFileTrackingOrder> { _order }.ToArray();

            return nodeFile;

        }
        private List<OrderLineElement> GetOrderLinesFromArcOrder(XDocument arcFile)
        {
            List<OrderLineElement> lines = new List<OrderLineElement>();
            var ordLines = arcFile.Descendants("Document_Lines").Elements("row");
            var ordNo = GetNodeValue(arcFile, "Documents", "DocNum");
            OrderLineElement ordLine = new OrderLineElement();
            if (ordLines != null)
            {
                foreach (var line in ordLines)
                {
                    try
                    {
                        int i;
                        decimal d;
                        ordLine = new OrderLineElement
                        {
                            LineNo = int.Parse(line.NodeExists("LineNum")) + 1,
                            PackageUnit = "CTN",
                            UnitPriceSpecified = true,
                            OrderQtySpecified = true,
                            LineTotalSpecified = true,
                            Product = new ProductElement
                            {
                                Description = line.NodeExists("ItemDescription"),
                                Code = line.NodeExists("ItemCode")
                            },
                            UnitOfMeasure = line.NodeExists("MeasureUnit"),
                            VolumeSpecified = true,
                            VolumeUnitSpecified = true,
                            WeightSpecified = true,
                            WeightUnitSpecified = true,


                        };
                        decimal.TryParse(line.NodeExists("Quantity", "0"), out d);
                        ordLine.OrderQty = d;
                        decimal.TryParse(line.NodeExists("Price", "0"), out d);
                        ordLine.UnitPrice = d;
                        decimal.TryParse(line.NodeExists("LineTotal", "0"), out d);
                        ordLine.LineTotal = d;
                        int.TryParse(line.NodeExists("PackageQuantity", "0"), out i);
                        ordLine.PackageQty = i;
                        decimal.TryParse(line.NodeExists("Volume", "0"), out d);
                        ordLine.Volume = d;
                        int.TryParse(line.NodeExists("VolumeUnit", "3"), out i);
                        switch (i)
                        {
                            case 1:
                                ordLine.VolumeUnit = OrderLineElementVolumeUnit.FT;
                                break;
                            case 2:
                                ordLine.VolumeUnit = OrderLineElementVolumeUnit.CM;
                                break;
                            case 3:
                                ordLine.VolumeUnit = OrderLineElementVolumeUnit.M3;
                                break;

                        };

                        decimal.TryParse(line.Element("Weight1").Value, out d);
                        ordLine.Weight = d;
                        string wgt = line.NodeExists("Weight1Unit", "3");
                        int.TryParse(wgt, out i);
                        switch (i)
                        {
                            case 1:
                                ordLine.WeightUnit = OrderLineElementWeightUnit.LB;
                                break;
                            case 2:
                                ordLine.WeightUnit = OrderLineElementWeightUnit.KG;
                                break;
                            case 3:
                                ordLine.WeightUnit = OrderLineElementWeightUnit.KG;
                                break;
                        }
                        lines.Add(ordLine);
                    }
                    catch (Exception ex)
                    {
                        var st = new StackTrace();
                        var sf = st.GetFrame(0);

                        var currentMethodName = sf.GetMethod().Name;
                        using (AppLogger log = new AppLogger(logpath, "", "Error Converting Order Lines. Order No:" + ordNo + " Line No:" + line.Element("LineNum").Value + " ",
                            currentMethodName, DateTime.Now, ex.GetType().Name + ":" + ex.Message, string.Empty))
                        {
                            log.AddLog();
                        }
                        return null;
                    }

                }
            }
            return lines;
        }

        private string GetNodeValue(XDocument arcFile, string parent, string nodeToFind)
        {
            try
            {
                var node = arcFile.Descendants(parent).Elements("row").Single();
                var returnValue = node.Element(nodeToFind) == null ? null : node.Element(nodeToFind).Value;
                return returnValue;
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                using (AppLogger log = new AppLogger(logpath, "", "Error returning value: Parent='" + parent + "' NodeToFind='" + nodeToFind + "'",
                    currentMethodName, DateTime.Now, ex.GetType().Name + ":" + ex.Message, string.Empty))
                {
                    log.AddLog();
                }
                return string.Empty;
            }


        }

        private IEnumerable<DateElement> GetDatesFromArcOrder(XDocument arcFile)
        {
            var docHeader = arcFile.Descendants("Documents").Elements("row").Single();
            var addressLines = arcFile.Descendants("AddressExtension").Elements("row");
            List<DateElement> dates = new List<DateElement>();
            DateElement raiseDate = new DateElement();
            DateTime compdate;
            string testDate = string.Empty;
            raiseDate.DateType = DateElementDateType.Raised;
            testDate = docHeader.Element("U_SENTDATE") == null ? string.Empty : docHeader.Element("U_SENTDATE").Value;
            if (DateTime.TryParse(testDate, out compdate))
            {
                raiseDate.ActualDate = compdate.ToString("dd/MM/yyyy hh:mm");
                dates.Add(raiseDate);
            }
            DateElement exfactDate = new DateElement();
            exfactDate.DateType = DateElementDateType.ExFactory;
            testDate = docHeader.Element("U_EXFACTORYDATE") == null ? string.Empty : docHeader.Element("U_EXFACTORYDATE").Value;
            if (DateTime.TryParse(testDate, out compdate))
            {
                exfactDate.ActualDate = compdate.ToString("dd/MM/yyyy hh:mm");
                dates.Add(exfactDate);
            }

            DateElement inStoreDate = new DateElement();
            inStoreDate.DateType = DateElementDateType.InStoreDateReq;
            testDate = docHeader.Element("U_CUSTREQDELDATE") == null ? string.Empty : docHeader.Element("U_CUSTREQDELDATE").Value;
            if (DateTime.TryParse(testDate, out compdate))
            {
                inStoreDate.ActualDate = compdate.ToString("dd/MM/yyyy hh:mm");
                dates.Add(inStoreDate);
            }
            return dates;
        }

        private IEnumerable<CompanyElement> GetCompanyListFromArc(XDocument arcFile)
        {
            var docHeader = arcFile.Descendants("Documents").Elements("row").Single();
            var addressLines = arcFile.Descendants("AddressExtension").Elements("row").Single();
            List<CompanyElement> _companies = new List<CompanyElement>();
            CompanyElement supplier = new CompanyElement();
            supplier.CompanyName = docHeader.Element("CardName").Value;
            supplier.CompanyCode = docHeader.Element("CardCode").Value;
            supplier.CompanyType = CompanyElementCompanyType.Supplier;
            string[] arrAddress = docHeader.Element("Address").Value.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);
            for (int i = 0; i < arrAddress.Length; i++)
            {
                try
                {
                    if (!string.IsNullOrEmpty(arrAddress[i]))
                    {
                        switch (i)
                        {
                            case 0:
                                supplier.Address1 = arrAddress[i];
                                break;
                            case 1:
                                supplier.Address2 = arrAddress[i];
                                break;
                            case 2:
                                supplier.Address3 = arrAddress[i];
                                supplier.Country = arrAddress[i];
                                supplier.CountryCode = GetCompanycode(arrAddress[i]);
                                break;
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            _companies.Add(supplier);
            CompanyElement buyer = new CompanyElement();
            buyer.CompanyOrgCode = docHeader.Element("U_BUYER") == null ? "ARCDISSYD" : docHeader.Element("U_BUYER").Value;
            buyer.CompanyType = CompanyElementCompanyType.Consignee;
            _companies.Add(buyer);
            CompanyElement deliverTo = new CompanyElement();
            deliverTo.CompanyName = docHeader.Element("U_CUSTNAME") == null ? docHeader.Element("ShipToStreet") != null ? docHeader.Element("ShipToStreet").Value : string.Empty : docHeader.Element("U_CUSTNAME").Value;
            deliverTo.CompanyCode = docHeader.Element("U_CUSTCODE") == null ? string.Empty : docHeader.Element("U_CUSTCODE").Value;
            deliverTo.Address1 = addressLines.Element("ShipToStreet") == null ? addressLines.Element("ShipToStreetNo") != null ? addressLines.Element("ShipToStreetNo").Value : string.Empty : addressLines.Element("ShipToStreet").Value;
            deliverTo.Address2 = addressLines.Element("ShipToStreetNo") != null ? addressLines.Element("ShipToStreetNo").Value : addressLines.Element("ShipToBlock") != null ? addressLines.Element("ShipToBlock").Value : string.Empty;
            deliverTo.City = addressLines.Element("ShipToCity") == null ? string.Empty : addressLines.Element("ShipToCity").Value;
            deliverTo.PostCode = addressLines.Element("ShipToZipCode") == null ? string.Empty : addressLines.Element("ShipToZipCode").Value;
            deliverTo.State = addressLines.Element("ShiptoState") == null ? string.Empty : addressLines.Element("ShipToState").Value;
            deliverTo.Country = addressLines.Element("ShipToCountry") == null ? string.Empty : addressLines.Element("ShipToCountry").Value;
            deliverTo.CountryCode = GetCompanycode(addressLines.Element("ShipToCountry") == null ? string.Empty : addressLines.Element("ShipToCountry").Value);
            deliverTo.CompanyType = CompanyElementCompanyType.DeliveryAddress;
            _companies.Add(deliverTo);
            return _companies;

        }

        private string GetCompanycode(string v)
        {
            string code = string.Empty;
            switch (v.ToUpper())
            {
                case "CHINA":
                    code = "CN";
                    break;
                case "JAPAN":
                    code = "JP";
                    break;
                case "INDIA":
                    code = "IN";
                    break;
                case "VIETNAM":
                    code = "VN";
                    break;
                case "AUSTRALIA":
                    code = "AU";
                    break;
            }

            return code;

        }
    }
}

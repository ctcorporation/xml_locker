﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace XMLLocker.ARC
{
    public class CommonToArc
    {
        #region members
        const string arcTimeConst = "yyyy-MM-ddThh:mm:ss.mmm";
        string _fileName;
        #endregion


        #region properties
        public string CWXmlFile
        {
            get
            { return _fileName; }
            set
            { _fileName = value; }
        }
        public string OrderNo { get; private set; }

        #endregion

        #region constructors
        public CommonToArc()
        {

        }
        #endregion

        #region methods



        public ToSend.BOM ConvertData(NodeFile commonXML)
        {
            XMLLocker.ARC.ToSend.BOM arcXML = new ToSend.BOM();
            ToSend.BOMBO arcHeader = new ToSend.BOMBO();
            foreach (var order in commonXML.TrackingOrders)
            {
                arcHeader.Documents = GetHeaderInformationFromCommon(order).ToArray();
            }
            arcXML.BO = new ToSend.BOMBO();
            arcXML.BO = arcHeader;
            return arcXML;
        }

        private List<ToSend.row> GetHeaderInformationFromCommon(NodeFileTrackingOrder order)
        {
            List<ToSend.row> documents = new List<ToSend.row>();
            ToSend.row drow = new ToSend.row();
            if (order.OrderNo.Substring(0, 1) != "P")
            {
                drow.DocNum = "P" + order.OrderNo;
            }
            else
            {
                drow.DocNum = order.OrderNo;
            }

            drow.U_MNDSTATUS = order.OrderStatus;
            drow.U_SENTDATE = GetCommonDate(order.Dates, DateElementDateType.Booked);
            drow.U_EXFACTORYDATE = GetCommonDate(order.Dates, DateElementDateType.ExFactory);
            drow.U_CUSTREQDELDATE = GetCommonDate(order.Dates, DateElementDateType.InStoreDateReq);
            drow.U_INCOTERM = order.IncoTerms;
            drow.U_CONTAINERTYPE = order.TransportMode;
            drow.U_PROFORMA = order.InvoiceNo;
            drow.U_LOADINGDATE = GetCommonDate(order.Dates, DateElementDateType.Loading);
            drow.U_ETD = GetCommonDate(order.Dates, DateElementDateType.Departure);
            drow.U_ETA = GetCommonDate(order.Dates, DateElementDateType.Arrival);
            drow.U_MONDIALEUPDATESTAMP = DateTime.Now.ToString(arcTimeConst);
            documents.Add(drow);
            return documents;

        }

        private object GetContainerTypefromCommon(ContainerElement[] containers)
        {
            string contType = string.Empty;
            if (containers != null)
            {
                contType = (from c in containers
                            where (c.ContainerCode != null)
                            select c.ContainerCode).FirstOrDefault();

            }
            return contType;
        }

        private string GetCommonDate(DateElement[] dates, DateElementDateType dateType)
        {
            var dateElement = (from d in dates
                               where (d.DateType == dateType)
                               select d).FirstOrDefault();
            if (dateElement == null)
            {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(dateElement.EstimateDate))
            {
                if (string.IsNullOrEmpty(dateElement.ActualDate))
                {
                    return string.Empty;
                }
                return dateElement.ActualDate;
            }
            return dateElement.EstimateDate;
        }
        #endregion
        #region helpers
        #endregion
    }
}
